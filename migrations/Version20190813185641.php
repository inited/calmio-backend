<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190813185641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tag__tag (id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE course__tag (course_id INT NOT NULL, tag_id VARCHAR(255) NOT NULL, INDEX IDX_8640737F591CC992 (course_id), INDEX IDX_8640737FBAD26311 (tag_id), PRIMARY KEY(course_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE course__tag ADD CONSTRAINT FK_8640737F591CC992 FOREIGN KEY (course_id) REFERENCES course__course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course__tag ADD CONSTRAINT FK_8640737FBAD26311 FOREIGN KEY (tag_id) REFERENCES tag__tag (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course__tag DROP FOREIGN KEY FK_8640737FBAD26311');
        $this->addSql('DROP TABLE tag__tag');
        $this->addSql('DROP TABLE course__tag');
    }
}
