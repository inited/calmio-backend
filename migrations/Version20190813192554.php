<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190813192554 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lesson__user (id INT AUTO_INCREMENT NOT NULL, lesson_id INT DEFAULT NULL, user_id INT DEFAULT NULL, finished DATETIME NOT NULL, INDEX IDX_9A5072D5CDF80196 (lesson_id), INDEX IDX_9A5072D5A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lesson__user ADD CONSTRAINT FK_9A5072D5CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson__lesson (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lesson__user ADD CONSTRAINT FK_9A5072D5A76ED395 FOREIGN KEY (user_id) REFERENCES user__user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE lesson__user');
    }
}
