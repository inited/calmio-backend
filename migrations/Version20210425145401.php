<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210425145401 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE device__subscription DROP FOREIGN KEY FK_83B91DF94A4C7D4');
        $this->addSql('ALTER TABLE user__device DROP FOREIGN KEY FK_A8114F0794A4C7D4');
        $this->addSql('ALTER TABLE device__subscription DROP FOREIGN KEY FK_83B91DF9A1887DC');
        $this->addSql('ALTER TABLE subscription__transaction DROP FOREIGN KEY FK_42DCABB89A1887DC');
        $this->addSql('DROP TABLE device__device');
        $this->addSql('DROP TABLE device__subscription');
        $this->addSql('DROP TABLE subscription__subscription');
        $this->addSql('DROP TABLE subscription__transaction');
        $this->addSql('DROP TABLE user__device');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE device__device (id VARCHAR(255) NOT NULL, platform VARCHAR(255) DEFAULT NULL, push_token VARCHAR(255) DEFAULT NULL, push_token_valid TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE device__subscription (device_id VARCHAR(255) NOT NULL, subscription_id INT NOT NULL, INDEX IDX_83B91DF94A4C7D4 (device_id), INDEX IDX_83B91DF9A1887DC (subscription_id), PRIMARY KEY(device_id, subscription_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscription__subscription (id INT AUTO_INCREMENT NOT NULL, first_user_id INT DEFAULT NULL, last_error VARCHAR(255) DEFAULT NULL, last_verification DATETIME NOT NULL, platform VARCHAR(255) NOT NULL, product_id VARCHAR(255) DEFAULT NULL, token LONGTEXT NOT NULL, token_hash VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_97985681B4E2BF69 (first_user_id), INDEX IDX_979856813952D0CB (platform), INDEX IDX_97985681B3BC57DA (token_hash), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscription__transaction (id INT AUTO_INCREMENT NOT NULL, subscription_id INT DEFAULT NULL, auto_renewing TINYINT(1) NOT NULL, cancel_reason VARCHAR(255) DEFAULT NULL, order_id VARCHAR(255) DEFAULT NULL, original_transaction_id VARCHAR(255) DEFAULT NULL, payment_state INT DEFAULT NULL, price_amount_micros VARCHAR(255) DEFAULT NULL, price_currency_code VARCHAR(255) DEFAULT NULL, product_id VARCHAR(255) NOT NULL, purchased_at DATETIME NOT NULL, renewals INT DEFAULT NULL, transaction_id VARCHAR(255) DEFAULT NULL, type VARCHAR(255) NOT NULL, upgraded TINYINT(1) NOT NULL, valid_to DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_42DCABB89A1887DC (subscription_id), INDEX IDX_42DCABB88C3213D0 (valid_to), INDEX IDX_42DCABB84584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user__device (id INT AUTO_INCREMENT NOT NULL, device_id VARCHAR(255) DEFAULT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_A8114F0794A4C7D4 (device_id), INDEX IDX_A8114F07A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE device__subscription ADD CONSTRAINT FK_83B91DF94A4C7D4 FOREIGN KEY (device_id) REFERENCES device__device (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE device__subscription ADD CONSTRAINT FK_83B91DF9A1887DC FOREIGN KEY (subscription_id) REFERENCES subscription__subscription (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subscription__subscription ADD CONSTRAINT FK_97985681B4E2BF69 FOREIGN KEY (first_user_id) REFERENCES user__user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subscription__transaction ADD CONSTRAINT FK_42DCABB89A1887DC FOREIGN KEY (subscription_id) REFERENCES subscription__subscription (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user__device ADD CONSTRAINT FK_A8114F0794A4C7D4 FOREIGN KEY (device_id) REFERENCES device__device (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user__device ADD CONSTRAINT FK_A8114F07A76ED395 FOREIGN KEY (user_id) REFERENCES user__user (id) ON DELETE CASCADE');
    }
}
