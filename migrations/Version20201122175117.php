<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201122175117 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user__promo_code_history DROP FOREIGN KEY FK_D1EDB7CA76ED395');
        $this->addSql('ALTER TABLE user__promo_code_history ADD CONSTRAINT FK_D1EDB7CA76ED395 FOREIGN KEY (user_id) REFERENCES user__user (id)');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user__promo_code_history DROP FOREIGN KEY FK_D1EDB7CA76ED395');
        $this->addSql('ALTER TABLE user__promo_code_history ADD CONSTRAINT FK_D1EDB7CA76ED395 FOREIGN KEY (user_id) REFERENCES user__user (id) ON DELETE CASCADE');
    }
}
