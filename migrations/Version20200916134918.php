<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200916134918 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE analytics__analytics (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL, platform VARCHAR(255) NOT NULL, INDEX IDX_B7EA0C66A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE analytics__subscription (id INT AUTO_INCREMENT NOT NULL, analytics_id INT DEFAULT NULL, expiration INT NOT NULL, product_id VARCHAR(255) NOT NULL, receipt LONGTEXT NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_919E2AB8F4297814 (analytics_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE analytics__analytics ADD CONSTRAINT FK_B7EA0C66A76ED395 FOREIGN KEY (user_id) REFERENCES user__user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE analytics__subscription ADD CONSTRAINT FK_919E2AB8F4297814 FOREIGN KEY (analytics_id) REFERENCES analytics__analytics (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course__user_lesson ADD current_session_completed TINYINT(1) NOT NULL, ADD current_session_time INT NOT NULL, ADD lesson_duration_seconds INT NOT NULL, ADD voice VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE analytics__subscription DROP FOREIGN KEY FK_919E2AB8F4297814');
        $this->addSql('DROP TABLE analytics__analytics');
        $this->addSql('DROP TABLE analytics__subscription');
        $this->addSql('ALTER TABLE course__user_lesson DROP current_session_completed, DROP current_session_time, DROP lesson_duration_seconds, DROP voice');
    }
}
