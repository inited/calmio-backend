<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190905135152 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE course__lesson');
        $this->addSql('ALTER TABLE daily_meditation__lesson DROP FOREIGN KEY FK_DEA66465336BE0ED');
        $this->addSql('DROP TABLE daily_meditation__daily_meditation');
        $this->addSql('DROP TABLE daily_meditation__lesson');
        $this->addSql('DROP TABLE file__file');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO `tag__tag` (`id`, `name`) VALUES
            ('BETTER_SLEEP',	''),
            ('CONTACT_WITH_BODY',	''),
            ('FOCUS_AND_PRODUCTIVITY',	''),
            ('LOTS_OF_EXPERIENCE',	''),
            ('MENTAL_HEALTH',	''),
            ('MILD_EXPERIENCE',	''),
            ('NO_EXPERIENCE',	''),
            ('PERSONAL_GROWTH',	''),
            ('STRESS_AND_ANXIETY',	'')"
        );

        $this->addSql('CREATE TABLE course__lesson (course_id INT NOT NULL, lessonentity_id INT NOT NULL, INDEX IDX_984969C591CC992 (course_id), INDEX IDX_984969CE7A27224 (lessonentity_id), PRIMARY KEY(course_id, lessonentity_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE course__lesson ADD CONSTRAINT FK_984969C591CC992 FOREIGN KEY (course_id) REFERENCES course__course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course__lesson ADD CONSTRAINT FK_984969CE7A27224 FOREIGN KEY (lessonentity_id) REFERENCES lesson__lesson (id) ON DELETE CASCADE');
        $this->addSql('CREATE TABLE daily_meditation__daily_meditation (id INT AUTO_INCREMENT NOT NULL, `from` DATE NOT NULL, name VARCHAR(255) NOT NULL, subtitle VARCHAR(255) NOT NULL, thumbnail VARCHAR(255) NOT NULL, `to` DATE NOT NULL, visibility TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE daily_meditation__lesson (dailymeditation_id INT NOT NULL, lessonentity_id INT NOT NULL, INDEX IDX_DEA66465336BE0ED (dailymeditation_id), INDEX IDX_DEA66465E7A27224 (lessonentity_id), PRIMARY KEY(dailymeditation_id, lessonentity_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE daily_meditation__lesson ADD CONSTRAINT FK_DEA66465336BE0ED FOREIGN KEY (dailymeditation_id) REFERENCES daily_meditation__daily_meditation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE daily_meditation__lesson ADD CONSTRAINT FK_DEA66465E7A27224 FOREIGN KEY (lessonentity_id) REFERENCES lesson__lesson (id) ON DELETE CASCADE');
        $this->addSql('CREATE TABLE file__file (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', created DATETIME NOT NULL, name VARCHAR(255) NOT NULL, original_name VARCHAR(255) NOT NULL, size INT NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE daily_meditation__lesson');
        $this->addSql('ALTER TABLE daily_meditation__daily_meditation ADD lesson_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE daily_meditation__daily_meditation ADD CONSTRAINT FK_31FE5328CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson__lesson (id) ON DELETE SET NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_31FE5328CDF80196 ON daily_meditation__daily_meditation (lesson_id)');
    }
}
