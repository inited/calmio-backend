<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210210133027 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX IDX_E76E8DD68CDE5729 ON course__course');
        $this->addSql('ALTER TABLE course__course DROP type');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course__course ADD type VARCHAR(255) NOT NULL');
        $this->addSql('CREATE INDEX IDX_E76E8DD68CDE5729 ON course__course (type)');
        $this->addSql('UPDATE `course__course` SET `type` = \'course\';');
    }
}
