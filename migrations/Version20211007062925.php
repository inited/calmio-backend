<?php
namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211007062925 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE subscription__transaction ADD purchased_at_calculated DATETIME DEFAULT NULL');

        $this->addSql('UPDATE `subscription__transaction` SET `purchased_at_calculated`=DATE_ADD(`valid_to`, INTERVAL -1 MONTH) WHERE `renewals` IS NOT NULL AND `type`=\'MONTHLY\'');
        $this->addSql('UPDATE `subscription__transaction` SET `purchased_at_calculated`=DATE_ADD(`valid_to`, INTERVAL -12 MONTH) WHERE `renewals` IS NOT NULL AND `type`=\'ANNUAL\'');
        $this->addSql('UPDATE `subscription__transaction` SET `purchased_at_calculated`=`purchased_at` WHERE `renewals` IS NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE subscription__transaction DROP purchased_at_calculated');
    }
}
