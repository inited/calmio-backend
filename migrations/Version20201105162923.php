<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201105162923 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user__promo_code_history');
        $this->addSql('ALTER TABLE promo_code__promo_code DROP type');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user__promo_code_history (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, promo_code_active_from DATETIME NOT NULL, promo_code_active_to DATETIME NOT NULL, valid TINYINT(1) NOT NULL, promoCode_id VARCHAR(255) DEFAULT NULL, INDEX IDX_D1EDB7C8D13F45 (promoCode_id), INDEX IDX_D1EDB7CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user__promo_code_history ADD CONSTRAINT FK_D1EDB7C8D13F45 FOREIGN KEY (promoCode_id) REFERENCES promo_code__promo_code (id)');
        $this->addSql('ALTER TABLE user__promo_code_history ADD CONSTRAINT FK_D1EDB7CA76ED395 FOREIGN KEY (user_id) REFERENCES user__user (id)');
        $this->addSql('ALTER TABLE promo_code__promo_code ADD type VARCHAR(255) NOT NULL');
        $this->addSql('UPDATE `promo_code__promo_code` SET `type`=\'promo\' WHERE 1');
    }
}
