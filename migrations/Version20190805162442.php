<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190805162442 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user__user (id INT AUTO_INCREMENT NOT NULL, active TINYINT(1) NOT NULL, created DATETIME NOT NULL, email VARCHAR(255) NOT NULL, facebook_token VARCHAR(255) DEFAULT NULL, name VARCHAR(255) NOT NULL, google_token VARCHAR(255) DEFAULT NULL, last_login DATETIME DEFAULT NULL, surname VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_32745D0AE7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE system__token (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', user_id INT DEFAULT NULL, created DATETIME NOT NULL, invalidation_date DATETIME DEFAULT NULL, valid TINYINT(1) NOT NULL, INDEX IDX_C581F866A76ED395 (user_id), INDEX IDX_C581F8668C0735FF (valid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE system__token ADD CONSTRAINT FK_C581F866A76ED395 FOREIGN KEY (user_id) REFERENCES user__user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE system__token DROP FOREIGN KEY FK_C581F866A76ED395');
        $this->addSql('DROP TABLE user__user');
        $this->addSql('DROP TABLE system__token');
    }
}
