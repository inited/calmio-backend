<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201105071417 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE promo_code__promo_code');
        $this->addSql('ALTER TABLE partner__partner DROP conversions, DROP used_codes');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE promo_code__promo_code (id VARCHAR(255) NOT NULL, partner_id INT DEFAULT NULL, available INT DEFAULT NULL, created_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, used INT NOT NULL, value INT NOT NULL, INDEX IDX_E626D9459393F8FE (partner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE promo_code__promo_code ADD CONSTRAINT FK_E626D9459393F8FE FOREIGN KEY (partner_id) REFERENCES partner__partner (id)');
        $this->addSql('ALTER TABLE partner__partner ADD conversions DOUBLE PRECISION NOT NULL, ADD used_codes INT NOT NULL');
    }
}
