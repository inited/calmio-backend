<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210214133855 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course__course_language MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE course__course_language DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE course__course_language DROP id, DROP daily_meditation, CHANGE course_id course_id INT NOT NULL, CHANGE language_id language_id VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`');
        $this->addSql('ALTER TABLE course__course_language ADD PRIMARY KEY (course_id, language_id)');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course__course_language ADD id INT AUTO_INCREMENT NOT NULL, ADD daily_meditation TINYINT(1) NOT NULL, CHANGE course_id course_id INT DEFAULT NULL, CHANGE language_id language_id VARCHAR(255) DEFAULT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
    }
}
