<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200606084451 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course__user DROP preferred_voice');
        $this->addSql('ALTER TABLE lesson__lesson_source DROP voice');
        $this->addSql('ALTER TABLE user__user DROP preferred_voice');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course__user ADD preferred_voice VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE lesson__lesson_source ADD voice VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user__user ADD preferred_voice VARCHAR(255) NOT NULL');
        $this->addSql("UPDATE course__user SET preferred_voice='man' WHERE 1;");
        $this->addSql("UPDATE lesson__lesson_source SET voice='man' WHERE 1;");
        $this->addSql("UPDATE user__user SET preferred_voice='man' WHERE 1;");
    }
}
