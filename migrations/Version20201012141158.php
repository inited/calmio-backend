<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201012141158 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE analytics__subscription (id INT AUTO_INCREMENT NOT NULL, analytics_id INT DEFAULT NULL, expiration INT NOT NULL, product_id VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, receipt LONGTEXT CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, type VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, INDEX IDX_919E2AB8F4297814 (analytics_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE analytics__subscription ADD CONSTRAINT FK_919E2AB8F4297814 FOREIGN KEY (analytics_id) REFERENCES analytics__analytics (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE analytics__analytics ADD platform VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, DROP iap_products_android, DROP iap_products_ios, DROP timestamp_android, DROP timestamp_ios');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE analytics__subscription');
        $this->addSql('ALTER TABLE analytics__analytics ADD iap_products_android LONGTEXT DEFAULT NULL, ADD iap_products_ios LONGTEXT DEFAULT NULL, ADD timestamp_android DATETIME DEFAULT NULL, ADD timestamp_ios DATETIME DEFAULT NULL, DROP platform');
    }
}
