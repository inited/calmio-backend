<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201113151840 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE partner__partner ADD purchases INT NOT NULL, DROP conversions');
        $this->addSql('ALTER TABLE promo_code__promo_code ADD purchases INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE partner__partner ADD conversions DOUBLE PRECISION NOT NULL, DROP purchases');
        $this->addSql('ALTER TABLE promo_code__promo_code DROP purchases');
    }
}
