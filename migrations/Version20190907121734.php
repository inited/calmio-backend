<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190907121734 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE article__article');
        $this->addSql('ALTER TABLE user__user DROP role');
        $this->addSql('DROP TABLE user__tag');
        $this->addSql('ALTER TABLE user__user DROP FOREIGN KEY FK_32745D0A46E90E27');
        $this->addSql('DROP INDEX IDX_32745D0A46E90E27 ON user__user');
        $this->addSql('ALTER TABLE user__user DROP experience_id, DROP full_version_expiration');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE article__article (id VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql("INSERT INTO `article__article` (`id`, `content`) VALUES 
            ('ABOUT', ''),
            ('TERMS_AND_CONDITIONS', '')");
        $this->addSql("ALTER TABLE user__user ADD role VARCHAR(255) NOT NULL");
        $this->addSql("UPDATE `user__user` SET `role` = 'USER'");
        $this->addSql("CREATE INDEX IDX_32745D0A57698A6A ON user__user (role)");
        $this->addSql('CREATE TABLE user__tag (userentity_id INT NOT NULL, tag_id VARCHAR(255) NOT NULL, INDEX IDX_3463E2B04F68DC3D (userentity_id), INDEX IDX_3463E2B0BAD26311 (tag_id), PRIMARY KEY(userentity_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user__tag ADD CONSTRAINT FK_3463E2B04F68DC3D FOREIGN KEY (userentity_id) REFERENCES user__user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user__tag ADD CONSTRAINT FK_3463E2B0BAD26311 FOREIGN KEY (tag_id) REFERENCES tag__tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user__user ADD experience_id VARCHAR(255) DEFAULT NULL, ADD full_version_expiration DATETIME NOT NULL');
        $this->addSql('ALTER TABLE user__user ADD CONSTRAINT FK_32745D0A46E90E27 FOREIGN KEY (experience_id) REFERENCES tag__tag (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_32745D0A46E90E27 ON user__user (experience_id)');
        $this->addSql("ALTER TABLE `user__user` CHANGE `full_version_expiration` `full_version_expiration` datetime NULL AFTER `experience_id`;");
        $this->addSql("UPDATE `user__user` SET `full_version_expiration` = NULL WHERE 1");
        $this->addSql('ALTER TABLE file__file ADD has_thumbnail TINYINT(1) NOT NULL');
    }
}
