<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190908072917 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE course__enrolled_user (user_id INT NOT NULL, course_id INT NOT NULL, INDEX IDX_6A993B09A76ED395 (user_id), INDEX IDX_6A993B09591CC992 (course_id), PRIMARY KEY(user_id, course_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE course__course_similar ADD CONSTRAINT FK_3E4A58AC591CC992 FOREIGN KEY (course_id) REFERENCES course__course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course__course_similar ADD CONSTRAINT FK_3E4A58AC7447624 FOREIGN KEY (course_similar_id) REFERENCES course__course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course__enrolled_user ADD CONSTRAINT FK_6A993B09591CC992 FOREIGN KEY (course_id) REFERENCES course__course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course__enrolled_user ADD CONSTRAINT FK_6A993B09A76ED395 FOREIGN KEY (user_id) REFERENCES user__user (id)');
        $this->addSql('DROP TABLE course__user');
        $this->addSql('CREATE TABLE lesson__user (id INT AUTO_INCREMENT NOT NULL, lesson_id INT DEFAULT NULL, user_id INT DEFAULT NULL, finished DATETIME NOT NULL, INDEX IDX_9A5072D5A76ED395 (user_id), INDEX IDX_9A5072D5CDF80196 (lesson_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE lesson__user ADD CONSTRAINT FK_9A5072D5A76ED395 FOREIGN KEY (user_id) REFERENCES user__user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE lesson__user ADD CONSTRAINT FK_9A5072D5CDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson__lesson (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE course__user_lesson');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE course__user (id INT AUTO_INCREMENT NOT NULL, course_id INT DEFAULT NULL, user_id INT DEFAULT NULL, INDEX IDX_391DA1BA591CC992 (course_id), INDEX IDX_391DA1BAA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE course__user ADD CONSTRAINT FK_391DA1BA591CC992 FOREIGN KEY (course_id) REFERENCES course__course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course__user ADD CONSTRAINT FK_391DA1BAA76ED395 FOREIGN KEY (user_id) REFERENCES user__user (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE course__enrolled_user');
        $this->addSql('CREATE TABLE course__user_lesson (id INT AUTO_INCREMENT NOT NULL, lesson_id INT DEFAULT NULL, finished DATETIME NOT NULL, time INT NOT NULL, courseUser_id INT DEFAULT NULL, INDEX IDX_4251A5CD3FD8CCC7 (courseUser_id), INDEX IDX_4251A5CDCDF80196 (lesson_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE course__user_lesson ADD CONSTRAINT FK_4251A5CD3FD8CCC7 FOREIGN KEY (courseUser_id) REFERENCES course__user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course__user_lesson ADD CONSTRAINT FK_4251A5CDCDF80196 FOREIGN KEY (lesson_id) REFERENCES lesson__lesson (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE lesson__user');
    }
}
