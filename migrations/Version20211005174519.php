<?php
namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211005174519 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course__user_lesson ADD language_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE course__user_lesson ADD CONSTRAINT FK_4251A5CD82F1BAF4 FOREIGN KEY (language_id) REFERENCES system__language (id)');
        $this->addSql('CREATE INDEX IDX_4251A5CD82F1BAF4 ON course__user_lesson (language_id)');

        $this->addSql('UPDATE `course__user_lesson` INNER JOIN `course__user` ON `course__user`.`id`=`course__user_lesson`.`courseUser_id` INNER JOIN `user__user` ON `user__user`.`id`=`course__user`.`user_id` SET `course__user_lesson`.`language_id`=`user__user`.`language_id` WHERE 1');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course__user_lesson DROP FOREIGN KEY FK_4251A5CD82F1BAF4');
        $this->addSql('DROP INDEX IDX_4251A5CD82F1BAF4 ON course__user_lesson');
        $this->addSql('ALTER TABLE course__user_lesson DROP language_id');
    }
}
