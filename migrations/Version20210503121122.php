<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210503121122 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE subscription__transaction_transaction');
        $this->addSql('ALTER TABLE subscription__transaction ADD subscription_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE subscription__transaction ADD CONSTRAINT FK_42DCABB89A1887DC FOREIGN KEY (subscription_id) REFERENCES subscription__subscription (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_42DCABB89A1887DC ON subscription__transaction (subscription_id)');
        $this->addSql('ALTER TABLE subscription__subscription DROP last_transaction_is_auto_renewing, DROP last_transaction_is_upgraded, DROP last_transaction_payment_state, DROP last_transaction_valid_to');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE subscription__transaction_transaction (subscriptiontransaction_id INT NOT NULL, subscription_id INT NOT NULL, INDEX IDX_397DE17FEE95DBDA (subscriptiontransaction_id), INDEX IDX_397DE17F9A1887DC (subscription_id), PRIMARY KEY(subscriptiontransaction_id, subscription_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE subscription__transaction_transaction ADD CONSTRAINT FK_397DE17FEE95DBDA FOREIGN KEY (subscriptiontransaction_id) REFERENCES subscription__transaction (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subscription__transaction_transaction ADD CONSTRAINT FK_397DE17F9A1887DC FOREIGN KEY (subscription_id) REFERENCES subscription__subscription (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subscription__transaction DROP FOREIGN KEY FK_42DCABB89A1887DC');
        $this->addSql('DROP INDEX IDX_42DCABB89A1887DC ON subscription__transaction');
        $this->addSql('ALTER TABLE subscription__transaction DROP subscription_id');
        $this->addSql('DELETE FROM subscription__transaction WHERE 1');
        $this->addSql('ALTER TABLE subscription__subscription ADD last_transaction_is_auto_renewing TINYINT(1) NOT NULL, ADD last_transaction_is_upgraded TINYINT(1) NOT NULL, ADD last_transaction_payment_state INT DEFAULT NULL, ADD last_transaction_valid_to DATETIME DEFAULT NULL');
    }
}
