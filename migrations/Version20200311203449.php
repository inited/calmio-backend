<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200311203449 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE version (id INT AUTO_INCREMENT NOT NULL, version VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE course__course_language');
        $this->addSql('DROP TABLE daily_meditation__daily_meditation_language');
        $this->addSql('ALTER TABLE article__article DROP FOREIGN KEY FK_239A1B7082F1BAF4');
        $this->addSql('DROP INDEX IDX_239A1B7082F1BAF4 ON article__article');
        $this->addSql('ALTER TABLE article__article DROP language_id');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE course__course_language (course_id INT NOT NULL, language_id VARCHAR(255) NOT NULL, INDEX IDX_85F080EF591CC992 (course_id), INDEX IDX_85F080EF82F1BAF4 (language_id), PRIMARY KEY(course_id, language_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE daily_meditation__daily_meditation_language (dailymeditation_id INT NOT NULL, language_id VARCHAR(255) NOT NULL, INDEX IDX_E3FC75C7336BE0ED (dailymeditation_id), INDEX IDX_E3FC75C782F1BAF4 (language_id), PRIMARY KEY(dailymeditation_id, language_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE course__course_language ADD CONSTRAINT FK_85F080EF591CC992 FOREIGN KEY (course_id) REFERENCES course__course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course__course_language ADD CONSTRAINT FK_85F080EF82F1BAF4 FOREIGN KEY (language_id) REFERENCES system__language (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE daily_meditation__daily_meditation_language ADD CONSTRAINT FK_E3FC75C7336BE0ED FOREIGN KEY (dailymeditation_id) REFERENCES daily_meditation__daily_meditation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE daily_meditation__daily_meditation_language ADD CONSTRAINT FK_E3FC75C782F1BAF4 FOREIGN KEY (language_id) REFERENCES system__language (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE article__article ADD language_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE article__article ADD CONSTRAINT FK_239A1B7082F1BAF4 FOREIGN KEY (language_id) REFERENCES system__language (id)');
        $this->addSql('CREATE INDEX IDX_239A1B7082F1BAF4 ON article__article (language_id)');
        $this->addSql('UPDATE `article__article` SET `language_id` = \'cs\';');
    }
}
