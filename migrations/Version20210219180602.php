<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210219180602 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course__user_lesson DROP FOREIGN KEY FK_4251A5CDBA2D8762');
        $this->addSql('DROP INDEX IDX_4251A5CDBA2D8762 ON course__user_lesson');
        $this->addSql('ALTER TABLE course__user_lesson DROP lecturer_id');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE course__user_lesson ADD lecturer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE course__user_lesson ADD CONSTRAINT FK_4251A5CDBA2D8762 FOREIGN KEY (lecturer_id) REFERENCES lecturer__lecturer (id)');
        $this->addSql('CREATE INDEX IDX_4251A5CDBA2D8762 ON course__user_lesson (lecturer_id)');

        // DATA
        $this->addSql("INSERT INTO `lecturer__lecturer` (`id`, `gender`, `name`, `deleted`) VALUES
            (1,	'man',	'Libor',	0),
            (2,	'woman',	'Victorie',	0);");
        $this->addSql("UPDATE `lesson__lesson_source` SET `lecturer_id` = '1' WHERE `voice` = 'man';");
        $this->addSql("UPDATE `lesson__lesson_source` SET `lecturer_id` = '2' WHERE `voice` = 'woman';");
        $this->addSql("UPDATE `course__user_lesson` SET `lecturer_id` = '1' WHERE `voice` = 'man';");
        $this->addSql("UPDATE `course__user_lesson` SET `lecturer_id` = '2' WHERE `voice` = 'woman';");
    }
}
