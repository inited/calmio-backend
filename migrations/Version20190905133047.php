<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190905133047 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE course__course_similar');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE course__course_similar (course_id INT NOT NULL, course_similar_id INT NOT NULL, INDEX IDX_3E4A58AC7447624 (course_similar_id), INDEX IDX_3E4A58AC591CC992 (course_id), PRIMARY KEY(course_id, course_similar_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE course__course_similar ADD CONSTRAINT FK_3E4A58AC591CC992 FOREIGN KEY (course_id) REFERENCES course__course (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE course__course_similar ADD CONSTRAINT FK_3E4A58AC7447624 FOREIGN KEY (course_similar_id) REFERENCES course__course (id) ON DELETE CASCADE');
    }
}
