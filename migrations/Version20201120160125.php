<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201120160125 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_B7EA0C66A76ED3953952D0CBC84F4277 ON analytics__analytics');
        $this->addSql('ALTER TABLE analytics__analytics DROP active, DROP first_payment, DROP iap_purchase_type, DROP last_payment, DROP platform, DROP receipt');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE analytics__analytics ADD active TINYINT(1) NOT NULL, ADD first_payment DATETIME DEFAULT NULL, ADD iap_purchase_type VARCHAR(255) DEFAULT NULL, ADD last_payment DATETIME DEFAULT NULL, ADD platform VARCHAR(255) DEFAULT NULL, ADD receipt LONGTEXT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B7EA0C66A76ED3953952D0CBC84F4277 ON analytics__analytics (user_id, platform, iap_purchase_type)');
        $this->addSql('CREATE VIEW `statistics_purchases` AS
            SELECT MD5(`user__user`.`email`) md5, `user__user`.`email`, `platform`,`iap_purchase_type`,`analytics__analytics`.`active`,`first_payment`,`last_payment`,`receipt`
            FROM `analytics__analytics`
            INNER JOIN `user__user` ON `user__user`.`id`=`analytics__analytics`.`user_id`
            WHERE 1');
    }
}
