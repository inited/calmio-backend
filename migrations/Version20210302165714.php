<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210302165714 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE OR REPLACE VIEW `statistics_purchases` AS 
            SELECT MD5(`user__user`.`email`) md5, `user__user`.`email`, `platform`,`iap_purchase_type`,`analytics__analytics`.`active`,`first_payment`,`last_payment`,`receipt`
            FROM `analytics__analytics`
            INNER JOIN `user__user` ON `user__user`.`id`=`analytics__analytics`.`user_id`
            WHERE `platform`='ios' OR (`platform`='android' AND `receipt` LIKE '%GPA.%')
        ");
    }

    public function down(Schema $schema): void
    {
    }
}
