<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200311173703 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user__user DROP FOREIGN KEY FK_32745D0A82F1BAF4');
        $this->addSql('CREATE TABLE version (id INT AUTO_INCREMENT NOT NULL, version VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE system__language');
        $this->addSql('DROP INDEX IDX_32745D0A82F1BAF4 ON user__user');
        $this->addSql('ALTER TABLE user__user DROP language_id');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE system__language (id VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user__user ADD language_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user__user ADD CONSTRAINT FK_32745D0A82F1BAF4 FOREIGN KEY (language_id) REFERENCES system__language (id)');
        $this->addSql('CREATE INDEX IDX_32745D0A82F1BAF4 ON user__user (language_id)');
        $this->addSql('INSERT INTO `system__language` (`id`) VALUES
            (\'cs\'),
            (\'hu\'),
            (\'pl\'),
            (\'sk\');');
        $this->addSql('UPDATE `user__user` SET `language_id` = \'cs\';');
    }
}
