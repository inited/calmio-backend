<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210203133909 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_B7EA0C66A76ED3953952D0CBC84F4277 ON analytics__analytics');
        $this->addSql('CREATE INDEX IDX_B7EA0C664B1EFC02 ON analytics__analytics (active)');
        $this->addSql('CREATE INDEX IDX_B7EA0C663952D0CB ON analytics__analytics (platform)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX IDX_B7EA0C664B1EFC02 ON analytics__analytics');
        $this->addSql('DROP INDEX IDX_B7EA0C663952D0CB ON analytics__analytics');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B7EA0C66A76ED3953952D0CBC84F4277 ON analytics__analytics (user_id, platform, iap_purchase_type)');
    }
}
