<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210113083411 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('CREATE INDEX IDX_E626D9458CDE5729 ON promo_code__promo_code (type)');
        $this->addSql('CREATE INDEX IDX_E626D9451D775834 ON promo_code__promo_code (value)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX IDX_E626D9458CDE5729 ON promo_code__promo_code');
        $this->addSql('DROP INDEX IDX_E626D9451D775834 ON promo_code__promo_code');
    }
}
