<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200121192836 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user__role DROP FOREIGN KEY FK_E88E0129D60322AC');
        $this->addSql('DROP TABLE user__role');
        $this->addSql('DROP TABLE system__role');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user__role (userentity_id INT NOT NULL, role_id VARCHAR(255) NOT NULL, INDEX IDX_E88E01294F68DC3D (userentity_id), INDEX IDX_E88E0129D60322AC (role_id), PRIMARY KEY(userentity_id, role_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE system__role (id VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user__role ADD CONSTRAINT FK_E88E01294F68DC3D FOREIGN KEY (userentity_id) REFERENCES user__user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user__role ADD CONSTRAINT FK_E88E0129D60322AC FOREIGN KEY (role_id) REFERENCES system__role (id) ON DELETE CASCADE');
        $this->addSql('INSERT INTO `system__role` (`id`) VALUES (\'ADMIN\'),(\'TEST\'),(\'USER\');');
        $this->addSql('ALTER TABLE course__user_lesson ADD unfinished_time INT NOT NULL, ADD completed TINYINT(1) NOT NULL');
    }
}
