<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210218153428 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lecturer__lecturer CHANGE deleted deleted INT NOT NULL');
        $this->addSql('ALTER TABLE lesson__lesson_source DROP FOREIGN KEY FK_CD9C604DBA2D8762');
        $this->addSql('DROP INDEX IDX_CD9C604DBA2D8762 ON lesson__lesson_source');
        $this->addSql('ALTER TABLE lesson__lesson_source DROP lecturer_id');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `lecturer__lecturer` ADD `deleted` tinyint(1) NOT NULL;');
        $this->addSql('ALTER TABLE lesson__lesson_source ADD lecturer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE lesson__lesson_source ADD CONSTRAINT FK_CD9C604DBA2D8762 FOREIGN KEY (lecturer_id) REFERENCES lecturer__lecturer (id)');
        $this->addSql('CREATE INDEX IDX_CD9C604DBA2D8762 ON lesson__lesson_source (lecturer_id)');
    }
}
