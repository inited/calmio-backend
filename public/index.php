<?php

use App\Handler\ErrorHandler;
use Slim\App;
use Tracy\Debugger;

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

$autoloader = require __DIR__ . '/../vendor/autoload.php';

session_start();

Debugger::enable([
    '217.170.105.135', // T.Pavlik
    '81.91.92.105', // Inited test server
], __DIR__ . '/../logs/', 'info@tomaspavlik.cz');

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';

Sentry\init(['dsn' => $settings['settings']['sentryDsn']]);

if (Debugger::detectDebugMode() && is_file(__DIR__ . '/../src/settings.local.php')) {
    $settingsLocal = require __DIR__ . '/../src/settings.local.php';
    $settings = array_replace_recursive($settings, $settingsLocal);
}

$app = new App($settings);
$container = $app->getContainer();
// v pripade chyby vypis do response stack-trace a chybu zaloguj do logs/
$container['errorHandler'] = $container['phpErrorHandler'] = function () {
    return new ErrorHandler();
};

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();

