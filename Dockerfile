FROM composer as vendor

WORKDIR /tmp/

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install \
    --ignore-platform-reqs \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --prefer-dist

FROM php:7.4-apache

RUN a2enmod rewrite
RUN docker-php-ext-install pdo_mysql mysqli zip

RUN apt-get update &&  apt-get install -y cron

COPY . /var/www/html
COPY ./uploads.ini /usr/local/etc/php/conf.d/uploads.ini
COPY ./calmiocron /etc/cron.d/calmiocron
COPY --from=vendor /tmp/vendor/ /var/www/html/vendor/

RUN chmod 0644 /etc/cron.d/calmiocron

ENTRYPOINT ./entrypoint.sh
EXPOSE 80
