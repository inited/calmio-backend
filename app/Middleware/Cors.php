<?php

namespace App\Middleware;

use Slim\Http\Request;
use Slim\Http\Response;

class Cors
{
    /**
     * @param Request  $request
     * @param Response $response
     * @param mixed    $next
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $next): Response
    {
        $response = $next($request, $response);

        return $response
            ->withHeader('Access-Control-Allow-Origin', $request->getHeader('Origin'))
            ->withHeader('Access-Control-Allow-Headers', 'X-Device-Uuid, X-Requested-With, X-Locale, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }
}
