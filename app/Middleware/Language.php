<?php

namespace App\Middleware;

use App\Model\Entity\Language as LanguageEntity;
use App\Model\Repository\LanguageRepositoryInterface;
use App\Services\Storage\LanguageStorageInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Route;
use Symfony\Component\Translation\Translator;

class Language
{
    /** @var LanguageRepositoryInterface */
    private $languageRepository;

    /** @var LanguageStorageInterface */
    private $languageStorage;

    /** @var Translator */
    private $translator;

    public function __construct(
        LanguageRepositoryInterface $languageRepository,
        LanguageStorageInterface $languageStorage,
        Translator $translator
    ) {
        $this->languageRepository = $languageRepository;
        $this->languageStorage = $languageStorage;
        $this->translator = $translator;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param mixed    $next
     *
     * @return Response
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function __invoke(Request $request, Response $response, $next): Response
    {
        /** @var Route|null $route */
        $route = $request->getAttribute('route');

        if ($route) {
            $locale = strtolower($route->getArgument('language', $request->getHeaderLine('X-Locale')));
        } else {
            $locale = strtolower($request->getHeaderLine('X-Locale'));
        }

        if (!$locale || !in_array($locale, LanguageEntity::getAvailableLanguages())) {
            $locale = LanguageEntity::TYPE_CS;
        }

        /** @var LanguageEntity $language */
        $language = $this->languageRepository->find($locale);
        $this->languageStorage->setLanguage($language);

        $this->translator->setLocale($locale);

        return $next($request, $response);
    }
}
