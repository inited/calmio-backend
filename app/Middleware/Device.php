<?php

namespace App\Middleware;

use App\Model\Factory\DeviceFactory;
use App\Model\Repository\DeviceRepository;
use App\Services\Storage\DeviceStorage;
use Closure;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class Device
{
    const HEADER_NAME = 'X-Device-uuid';

    /** @var DeviceFactory */
    private $deviceFactory;

    /** @var DeviceRepository */
    private $deviceRepository;

    /** @var DeviceStorage */
    private $deviceStorage;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        DeviceFactory $deviceFactory,
        DeviceRepository $deviceRepository,
        DeviceStorage $deviceStorage,
        EntityManagerInterface $em
    ) {
        $this->deviceFactory = $deviceFactory;
        $this->deviceRepository = $deviceRepository;
        $this->deviceStorage = $deviceStorage;
        $this->em = $em;
    }

    public function __invoke(Request $request, Response $response, Closure $next): Response
    {
        if (!$request->hasHeader(self::HEADER_NAME)) {
            return $next($request, $response);
        }

        if (!$uuid = $request->getHeaderLine(self::HEADER_NAME)) {
            return $next($request, $response);
        }

        if (!$device = $this->deviceRepository->find($uuid)) {
            $device = $this->deviceFactory->create($uuid);
            $this->em->persist($device);
            $this->em->getUnitOfWork()->commit($device);
        }

        $this->deviceStorage->setDevice($device);

        return $next($request, $response);
    }
}
