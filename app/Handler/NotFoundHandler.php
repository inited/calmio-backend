<?php

namespace App\Handler;

use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;
use Slim\Views\Twig;

class NotFoundHandler
{
    /** @var Twig */
    private $view;

    public function __construct(Twig $view)
    {
        $this->view = $view;
    }

    public function __invoke(Request $request, Response $response): ResponseInterface
    {
        $response
            ->withHeader('Access-Control-Allow-Origin', $request->getHeader('Origin'))
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
            ->withStatus(StatusCode::HTTP_NOT_FOUND);

        return $this->view->render($response, '404.html.twig');
    }
}
