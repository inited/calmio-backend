<?php
namespace App\Handler;

use App\Exceptions\LocalizationException;
use Sentry;
use Slim\Http\Request;
use Slim\Http\Response;
use Throwable;

class ErrorHandler
{
    public function __invoke(Request $request, Response $response, Throwable $exception): Response
    {
        Sentry\captureException($exception);

        $statusCode = is_integer($exception->getCode()) && 0 !== $exception->getCode() ? $exception->getCode() : 500;
        $localizationKey = null;

        if ($exception instanceof LocalizationException) {
            $localizationKey = $exception->getLocalizationKey();
        }

        return $response
            ->withHeader('Access-Control-Allow-Origin', $request->getHeader('Origin'))
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
            ->withJson([
                'error' => $exception->getMessage(),
                'localizationKey' => $localizationKey,
            ])
            ->withStatus($statusCode);
    }
}
