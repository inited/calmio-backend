<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Event\UserRegisteredEvent;
use App\Model\Entity\UserEntity;
use App\Services\MailService\MailServiceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Translation\Translator;

class UserRegisteredEventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            UserRegisteredEvent::class => [
                ['sendActivationEmail', 200],
            ],
        ];
    }

    /** @var string */
    private $apiUri;

    /** @var MailServiceInterface */
    private $mailService;

    /** @var Translator */
    private $translator;

    public function __construct(
        string $apiUri,
        MailServiceInterface $mailService,
        Translator $translator
    ) {
        $this->apiUri = $apiUri;
        $this->mailService = $mailService;
        $this->translator = $translator;
    }

    public function sendActivationEmail(UserRegisteredEvent $event): void
    {
        /** @var UserEntity $user */
        $user = $event->getUser();

        // Set translator locale by user language
        $this->translator->setLocale($user->getLanguageCode());

        $this->mailService->sendTemplate(
            $user->getEmail(),
            $this->translator->trans('page.account_verification.title'),
            'email/accountActivation.html.twig',
            [
                'uri' => rtrim($this->apiUri, '/')
                    . '/'
                    . $user->getLanguageCode()
                    . '/user/activation/'
                    . $user->getPasswordResetHash(),
            ]
        );
    }
}
