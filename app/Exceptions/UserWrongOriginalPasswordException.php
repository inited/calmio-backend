<?php

declare(strict_types=1);

namespace App\Exceptions;

class UserWrongOriginalPasswordException extends LocalizationException
{
    /** @var string */
    protected $localizationKey = 'user_wrong_original_password';
}
