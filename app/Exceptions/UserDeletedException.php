<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Exceptions;

/**
 * Class UserDeletedException
 * @package App\Exceptions
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
class UserDeletedException extends LocalizationException
{
    /** @var string */
    protected $localizationKey = 'user_deleted_exception';
}
