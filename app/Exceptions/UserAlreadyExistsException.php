<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11.03.2020
 */

declare(strict_types=1);

namespace App\Exceptions;

/**
 * Class UserAlreadyExistsException
 * @package App\Exceptions
 */
class UserAlreadyExistsException extends LocalizationException
{
    /** @var string */
    protected $localizationKey = 'user_already_exists';
}
