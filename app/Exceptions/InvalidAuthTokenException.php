<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Exceptions;

/**
 * Class InvalidAuthTokenException
 * @package App\Exceptions
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
class InvalidAuthTokenException extends LocalizationException
{
    /** @var string */
    protected $localizationKey = 'invalid_auth_token';
}
