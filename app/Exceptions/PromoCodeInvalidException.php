<?php

declare(strict_types=1);

namespace App\Exceptions;

class PromoCodeInvalidException extends LocalizationException
{
    /** @var string */
    protected $localizationKey = 'promo_code_invalid';
}
