<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

abstract class LocalizationException extends Exception
{
    /** @var string */
    protected $localizationKey = '';

    public function getLocalizationKey(): string
    {
        return $this->localizationKey;
    }

}