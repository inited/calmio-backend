<?php

declare(strict_types=1);

namespace App\Exceptions;

class PromoCodeAlreadyUsedException extends LocalizationException
{
    /** @var string */
    protected $localizationKey = 'promo_code_already_used';
}
