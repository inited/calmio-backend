<?php

declare(strict_types=1);

namespace App\Exceptions;

class PromoCodeAnotherIsActiveException extends LocalizationException
{
    /** @var string */
    protected $localizationKey = 'promo_another_code_active';
}
