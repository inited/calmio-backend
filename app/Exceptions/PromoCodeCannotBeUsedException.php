<?php

declare(strict_types=1);

namespace App\Exceptions;

class PromoCodeCannotBeUsedException extends LocalizationException
{
    /** @var string */
    protected $localizationKey = 'promo_code_cannot_be_used';
}
