<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Exceptions;

/**
 * Class LanguageNotExistsException
 * @package App\Exceptions
 */
class LanguageNotExistsException extends LocalizationException
{
    /** @var string */
    protected $localizationKey = 'language_not_exists';

    /** @var string */
    protected $message = 'Zvolený jazyk neexistuje!';
}
