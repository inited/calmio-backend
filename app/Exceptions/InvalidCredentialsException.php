<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Exceptions;

/**
 * Class InvalidCredentialsException
 * @package App\Exceptions
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
class InvalidCredentialsException extends LocalizationException
{
    /** @var int */
    protected $code = 401;

    /** @var string */
    protected $localizationKey = 'invalid_credentials';

    /** @var string */
    protected $message = 'Neplatné uživatelské jméno nebo heslo!';

}
