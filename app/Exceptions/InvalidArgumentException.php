<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 10. 04. 2019
 */

declare(strict_types=1);

namespace App\Exceptions;

/**
 * Class InvalidArgumentException
 * @package App\Exceptions
 */
class InvalidArgumentException extends LocalizationException
{
    /** @var int */
    protected $code = 400; // Bad request

    /** @var string */
    protected $localizationKey = 'invalid_input_argument';
}
