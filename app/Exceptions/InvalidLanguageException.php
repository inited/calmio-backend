<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Exceptions;

/**
 * Class InvalidLanguageException
 * @package App\Exceptions
 */
class InvalidLanguageException extends LocalizationException
{
    /** @var string */
    protected $localizationKey = 'invalid_language';
}
