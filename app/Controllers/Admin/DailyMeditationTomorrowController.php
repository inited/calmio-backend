<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\Course;
use App\Model\Repository\CourseRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class DailyMeditationTomorrowController extends DefaultController
{
    /** @var CourseRepositoryInterface */
    private $courseRepository;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        CourseRepositoryInterface $courseRepository,
        EntityManagerInterface $em
    ) {
        $this->em = $em;
        $this->courseRepository = $courseRepository;
    }

    /**
     * @param array<string,mixed> $args
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        /** @var Course|null $course */
        $course = $this->courseRepository->findOneBy([
            "id" => $args["id"],
            "type" => Course::TYPE_MEDITATION,
        ]);

        if (!$course) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        $this->courseRepository->invalidateTomorrowDailyMeditation();

        $course->useAsTomorrowDailyMeditation();
        $this->em->flush();

        return $response->withStatus(StatusCode::HTTP_NO_CONTENT);
    }
}
