<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\LessonEntity;
use App\Model\Provider\LessonProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class LessonCreateController
 * @package App\Controllers\Admin
 */
final class LessonCreateController extends DefaultController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LessonProviderInterface
     */
    private $lessonsProvider;

    /**
     * LessonCreateController constructor.
     *
     * @param EntityManagerInterface  $em
     * @param LessonProviderInterface $lessonsProvider
     */
    public function __construct(
        EntityManagerInterface $em,
        LessonProviderInterface $lessonsProvider
    ) {
        $this->em = $em;
        $this->lessonsProvider = $lessonsProvider;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        if (!empty($data) && is_array($data)) {
            $lesson = new LessonEntity();
            $this->lessonsProvider->updateLesson($lesson, $data);

            $this->em->persist($lesson);
            $this->em->flush();

            return $response;
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
