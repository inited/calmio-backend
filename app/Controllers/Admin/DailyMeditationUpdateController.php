<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\Course;
use App\Model\Provider\CourseProviderInterface;
use App\Model\Repository\CourseRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class DailyMeditationUpdateController extends DefaultController
{
    /** @var CourseProviderInterface */
    private $courseProvider;

    /** @var CourseRepositoryInterface */
    private $courseRepository;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        CourseProviderInterface $courseProvider,
        CourseRepositoryInterface $courseRepository,
        EntityManagerInterface $em
    ) {
        $this->em = $em;
        $this->courseProvider = $courseProvider;
        $this->courseRepository = $courseRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        $data = $request->getParsedBody();

        if (empty($data) || !isset($args["id"])) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        /** @var Course|null $course */
        $course = $this->courseRepository->findOneBy([
            "id" => $args["id"],
            "type" => Course::TYPE_MEDITATION,
        ]);

        if (!$course) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        $this->courseProvider->updateCourse($course, $data);

        $this->em->persist($course);
        $this->em->flush();

        return $response->withJson($course->__toArray());
    }
}
