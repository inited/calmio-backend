<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 14.06.2020
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\PushNotification;
use App\Model\Repository\PushNotificationRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class PushNotificationListController extends DefaultController
{
    /** @var PushNotificationRepositoryInterface */
    private $pushNotificationRepository;

    public function __construct(PushNotificationRepositoryInterface $pushNotificationRepository)
    {
        $this->pushNotificationRepository = $pushNotificationRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     */
    public function defaultAction(Request $request, Response $response)
    {
        return $response->withJson(collect($this->pushNotificationRepository->findAll())
            ->transform(function (PushNotification $pushNotification) {
                return $pushNotification->__toArray();
            })
            ->toArray());
    }

}
