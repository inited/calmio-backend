<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\Course;
use App\Model\Repository\CourseRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class DailyMeditationListController extends DefaultController
{
    /** @var CourseRepositoryInterface */
    private $courseRepository;

    public function __construct(CourseRepositoryInterface $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    public function defaultAction(Request $request, Response $response): Response
    {
        $courses = $this->courseRepository->findBy([
            "type" => Course::TYPE_MEDITATION,
        ]);

        return $response->withJson(
            collect($courses)
                ->transform(function (Course $course) {
                    return $course->__toArray();
                })
                ->all()
        );
    }
}
