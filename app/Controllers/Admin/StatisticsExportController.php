<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Services\Statistics\StatisticsExport;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Stream;

final class StatisticsExportController extends DefaultController
{
    /** @var StatisticsExport */
    private $statisticsExport;

    public function __construct(StatisticsExport $statisticsExport)
    {
        $this->statisticsExport = $statisticsExport;
    }

    public function __invoke(Request $request, Response $response): void
    {
        $writer = WriterEntityFactory::createXLSXWriter();
        $writer->openToBrowser("statistics.xlsx");
        $this->statisticsExport->generateXlsx($writer);
        $writer->close();
    }
}
