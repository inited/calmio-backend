<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\Course;
use App\Model\Repository\CourseRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class CourseDeleteController
 * @package App\Controllers\Admin
 */
final class CourseDeleteController extends DefaultController
{
    /**
     * @var CourseRepositoryInterface
     */
    private $courseRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CourseDeleteController constructor.
     *
     * @param CourseRepositoryInterface $courseRepository
     * @param EntityManagerInterface    $em
     */
    public function __construct(
        CourseRepositoryInterface $courseRepository,
        EntityManagerInterface $em
    ) {
        $this->em = $em;
        $this->courseRepository = $courseRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (!isset($args["id"])) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        /** @var Course|null $course */
        $course = $this->courseRepository->findOneBy([
            "id" => $args["id"],
            "type" => Course::TYPE_COURSE,
        ]);

        if (!$course) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        $this->em->remove($course);
        $this->em->flush();

        return $response;
    }
}
