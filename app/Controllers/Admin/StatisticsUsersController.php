<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\InvalidArgumentException;
use App\Model\Entity\UserEntity;
use App\Model\Pagination\Paginator;
use App\Model\Repository\UserRepository;
use App\Services\Statistics\UserStatistics;
use Slim\Http\Request;
use Slim\Http\Response;

final class StatisticsUsersController extends DefaultController
{
    /** @var UserRepository */
    private $userRepository;

    /** @var UserStatistics */
    private $userStatistics;

    public function __construct(
        UserRepository $userRepository,
        UserStatistics $userStatistics
    ) {
        $this->userRepository = $userRepository;
        $this->userStatistics = $userStatistics;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function __invoke(Request $request, Response $response): Response
    {
        $limit = $this->validateRequestLimit($request->getParam('limit'));
        $offset = $this->validateRequestOffset($request->getParam('offset'));

        $users = $this->userRepository->findAllByPaginated($limit, $offset);
        $usersTotalCount = $users->count();

        $statistics = collect($users)
            ->transform(function (UserEntity $userEntity) {
                return $this->userStatistics->getStatistics($userEntity);
            })
            ->all();

        return $response->withJson(
            (new Paginator($statistics, $usersTotalCount, $limit, $offset))->transform()
        );
    }
}
