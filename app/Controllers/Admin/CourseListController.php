<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\Course;
use App\Model\Repository\CourseRepositoryInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class CourseListController
 * @package App\Controllers\Admin
 */
final class CourseListController extends DefaultController
{
    /**
     * @var CourseRepositoryInterface
     */
    private $courseRepository;

    /**
     * CourseListController constructor.
     *
     * @param CourseRepositoryInterface $courseRepository
     */
    public function __construct(CourseRepositoryInterface $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $courses = $this->courseRepository->findBy([
            "type" => Course::TYPE_COURSE,
        ]);

        return $response->withJson(
            collect($courses)
                ->transform(function (Course $course) {
                    return $course->__toArray();
                })
                ->all()
        );
    }
}
