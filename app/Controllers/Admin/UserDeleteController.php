<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\UserRepositoryInterface;
use App\Services\TokenStorage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class UserDeleteController
 * @package App\Controllers\Admin
 */
final class UserDeleteController extends DefaultController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserDeleteController constructor.
     *
     * @param EntityManagerInterface  $em
     * @param TokenStorageInterface   $tokenStorage
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        UserRepositoryInterface $userRepository
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (isset($data["email"])) {
            if (null !== $token = $this->tokenStorage->getToken()) {
                try {
                    $user = $this->userRepository->findByEmail($data["email"]);

                    if ($user !== $token->getUser()) {
                        $this->em->remove($user);
                        $this->em->flush();

                        return $response;
                    }
                } catch (EntityNotFoundException $e) {
                    return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
                }
            }
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
