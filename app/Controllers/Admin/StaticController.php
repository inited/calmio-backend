<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\LanguageNotExistsException;
use App\Model\Entity\Article;
use App\Model\Entity\Language;
use App\Model\Repository\ArticleRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class StaticController
 * @package App\Controllers\Admin
 */
final class StaticController extends DefaultController
{
    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * StaticController constructor.
     *
     * @param ArticleRepositoryInterface $articleRepository
     */
    public function __construct(ArticleRepositoryInterface $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws LanguageNotExistsException
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        $id = $request->getParam("id", ($data["id"] ?? null));
        $language = $request->getParam("language", ($data["language"] ?? Language::TYPE_CS));

        if (!in_array($language, Language::getAvailableLanguages())) {
            throw new LanguageNotExistsException();
        }

        if (isset($id, $language)) {
            try {
                /** @var Article $article */
                $article = $this->articleRepository->findByKeyAndLanguage($id, $language);

                return $response->withJson($article->__toArray());
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
            }
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
