<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\LessonEntity;
use App\Model\Repository\LessonRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class LessonDeleteController
 * @package App\Controllers\Admin
 */
final class LessonDeleteController extends DefaultController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LessonRepositoryInterface
     */
    private $lessonRepository;

    /**
     * LessonDeleteController constructor.
     *
     * @param EntityManagerInterface    $em
     * @param LessonRepositoryInterface $lessonRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        LessonRepositoryInterface $lessonRepository
    ) {
        $this->em = $em;
        $this->lessonRepository = $lessonRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (isset($args["id"])) {
            try {
                /** @var LessonEntity $lesson */
                $lesson = $this->lessonRepository->find($args["id"]);

                $this->em->remove($lesson);
                $this->em->flush();

                return $response;
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
            }
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
