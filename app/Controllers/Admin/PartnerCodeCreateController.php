<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InvalidArgumentException;
use App\Model\Entity\PromoCode;
use App\Model\Repository\PartnerRepositoryInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class PartnerCodeCreateController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var PartnerRepositoryInterface */
    private $partnerRepository;

    public function __construct(
        EntityManagerInterface $em,
        PartnerRepositoryInterface $partnerRepository
    ) {
        $this->em = $em;
        $this->partnerRepository = $partnerRepository;
    }

    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $data = $request->getParsedBody();

        if (empty($data) || !is_array($data) || !isset($args["id"])) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        if (empty($data["name"])) {
            throw new InvalidArgumentException("Parameter \"name\" is missing!");
        }

        if (empty($data["value"])) {
            throw new InvalidArgumentException("Parameter \"value\" is missing!");
        }

        $value = (int) $data["value"];
        if (0 >= $value) {
            throw new InvalidArgumentException("Parameter \"value\" is invalid!");
        }

        try {
            $partner = $this->partnerRepository->find($args["id"]);
        } catch (EntityNotFoundException $e) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        // Invalidate all previous valid promo codes
        foreach ($partner->getPromoCodes() as $promoCode) {
            if (PromoCode::TYPE_PROMO === $promoCode->getType()) {
                $promoCode->invalidate();
            }
        }

        // Create new promo code
        $promoCode = new PromoCode($data["name"]);
        $promoCode->setPartner($partner);
        $promoCode->setValue($value);

        try {
            $this->em->persist($promoCode);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new InvalidArgumentException("Code name already exists!");
        }

        return $response->withJson($promoCode->__toArray());
    }

}
