<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 14.06.2020
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\InvalidArgumentException;
use App\Model\Entity\PushNotification;
use App\Services\FirebaseMessageService\FirebaseMessageService;
use App\Services\TokenStorage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use sngrl\PhpFirebaseCloudMessaging\Notification;

final class PushNotificationController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var FirebaseMessageService */
    private $firebaseMessageService;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    public function __construct(
        EntityManagerInterface $em,
        FirebaseMessageService $firebaseMessageService,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->firebaseMessageService = $firebaseMessageService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws InvalidArgumentException
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (empty($data['message'])) {
            throw new InvalidArgumentException("Není zadán popisek push notifikace!");
        }

        $pushNotification = new PushNotification();
        $pushNotification->setMessage($data['message']);
        $pushNotification->setTest(isset($data['test']) && $data['test']);
        $pushNotification->setTitle($data['title'] ?? '');
        $pushNotification->setUser($this->tokenStorage->getToken()->getUser());

        $this->em->persist($pushNotification);
        $this->em->flush();

        // FCM NOTIFICATION
        $topicName = $pushNotification->isTest() ? 'news-test' : 'news';

        $notification = new Notification($pushNotification->getTitle(), $pushNotification->getMessage());
        $notification->setSound('default');
        $notification->setClickAction('FCM_PLUGIN_ACTIVITY');
        $notification->setIcon('fcm_push_icon');

        $this->firebaseMessageService->sendNotificationToTopic($notification, $topicName);

        return $response->withJson($pushNotification->__toArray());
    }

}
