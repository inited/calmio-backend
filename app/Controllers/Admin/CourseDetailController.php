<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\Course;
use App\Model\Repository\CourseRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class CourseDetailController
 * @package App\Controllers\Admin
 */
final class CourseDetailController extends DefaultController
{
    /**
     * @var CourseRepositoryInterface
     */
    private $courseRepository;

    /**
     * CourseDetailController constructor.
     *
     * @param CourseRepositoryInterface $courseRepository
     */
    public function __construct(CourseRepositoryInterface $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (!isset($args["id"])) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        /** @var Course|null $course */
        $course = $this->courseRepository->findOneBy([
            "id" => $args["id"],
            "type" => Course::TYPE_COURSE,
        ]);

        if (!$course) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        return $response->withJson($course->__toArray());
    }
}
