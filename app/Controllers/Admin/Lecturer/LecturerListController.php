<?php

declare(strict_types=1);

namespace App\Controllers\Admin\Lecturer;

use App\Controllers\DefaultController;
use App\Model\Entity\Lecturer;
use App\Model\Repository\LecturerRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class LecturerListController extends DefaultController
{
    /** @var LecturerRepositoryInterface */
    private $lecturerRepository;

    public function __construct(LecturerRepositoryInterface $lecturerRepository)
    {
        parent::__construct();

        $this->lecturerRepository = $lecturerRepository;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $lecturers = $this->lecturerRepository->findBy([
            'deleted' => false,
        ]);

        return $response->withJson(
            collect($lecturers)
                ->transform(function (Lecturer $lecturer) {
                    return $lecturer->__toArray();
                })
                ->all()
        );
    }
}
