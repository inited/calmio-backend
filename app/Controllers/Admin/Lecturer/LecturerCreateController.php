<?php

declare(strict_types=1);

namespace App\Controllers\Admin\Lecturer;

use App\Controllers\DefaultController;
use App\Model\Factory\LecturerFactory;
use App\Model\Provider\LecturerProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class LecturerCreateController extends DefaultController
{
    /** @var LecturerFactory */
    private $lecturerFactory;

    /** @var LecturerProviderInterface */
    private $lecturerProvider;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        EntityManagerInterface $em,
        LecturerFactory $lecturerFactory,
        LecturerProviderInterface $lecturerProvider
    ) {
        parent::__construct();
        
        $this->em = $em;
        $this->lecturerFactory = $lecturerFactory;
        $this->lecturerProvider = $lecturerProvider;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $data = $request->getParsedBody();

        $lecturer = $this->lecturerFactory->create();

        $this->lecturerProvider->updateLecturer($lecturer, $data);

        $this->em->persist($lecturer);
        $this->em->flush();

        return $response->withJson($lecturer->__toArray());
    }
}
