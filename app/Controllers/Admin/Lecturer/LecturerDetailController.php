<?php

declare(strict_types=1);

namespace App\Controllers\Admin\Lecturer;

use App\Controllers\DefaultController;
use App\Model\Repository\LecturerRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class LecturerDetailController extends DefaultController
{
    /** @var LecturerRepositoryInterface */
    private $lecturerRepository;

    public function __construct(LecturerRepositoryInterface $lecturerRepository)
    {
        parent::__construct();

        $this->lecturerRepository = $lecturerRepository;
    }

    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if (!isset($args['id'])) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        $lecturer = $this->lecturerRepository->findOneBy([
            'id' => $args['id'],
            'deleted' => false,
        ]);

        if (!$lecturer) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        return $response->withJson($lecturer->__toArray());
    }
}
