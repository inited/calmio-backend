<?php

declare(strict_types=1);

namespace App\Controllers\Admin\Lecturer;

use App\Controllers\DefaultController;
use App\Model\Provider\LecturerProviderInterface;
use App\Model\Repository\LecturerRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class LecturerUpdateController extends DefaultController
{
    /** @var LecturerProviderInterface */
    private $lecturerProvider;

    /** @var LecturerRepositoryInterface */
    private $lecturerRepository;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        EntityManagerInterface $em,
        LecturerProviderInterface $lecturerProvider,
        LecturerRepositoryInterface $lecturerRepository
    ) {
        parent::__construct();

        $this->em = $em;
        $this->lecturerProvider = $lecturerProvider;
        $this->lecturerRepository = $lecturerRepository;
    }

    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $data = $request->getParsedBody();

        if (!isset($args['id'])) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        $lecturer = $this->lecturerRepository->findOneBy([
            'id' => $args['id'],
            'deleted' => false,
        ]);

        if (!$lecturer) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        $this->lecturerProvider->updateLecturer($lecturer, $data);

        $this->em->flush();

        return $response->withJson($lecturer->__toArray());
    }
}
