<?php

declare(strict_types=1);

namespace App\Controllers\Admin\Lecturer;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\LecturerRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class LecturerDeleteController extends DefaultController
{
    /** @var LecturerRepositoryInterface */
    private $lecturerRepository;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        EntityManagerInterface $em,
        LecturerRepositoryInterface $lecturerRepository
    ) {
        parent::__construct();

        $this->em = $em;
        $this->lecturerRepository = $lecturerRepository;
    }

    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if (!isset($args['id'])) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        try {
            $lecturer = $this->lecturerRepository->find($args['id']);
        } catch (EntityNotFoundException $e) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        $lecturer->setDeleted(true);
        $this->em->flush();

        return $response;
    }
}
