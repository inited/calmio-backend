<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\Partner;
use App\Model\Repository\PartnerRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class PartnerListController extends DefaultController
{
    /** @var PartnerRepositoryInterface */
    private $partnerRepository;

    public function __construct(PartnerRepositoryInterface $partnerRepository)
    {
        $this->partnerRepository = $partnerRepository;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $partners = collect($this->partnerRepository->findAll())
            ->transform(function (Partner $partner) {
                return $partner->__toArray();
            })
            ->all();

        return $response->withJson($partners);
    }
}
