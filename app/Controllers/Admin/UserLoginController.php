<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InvalidCredentialsException;
use App\Model\Entity\Role;
use App\Model\Entity\TokenEntity;
use App\Model\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class UserLoginController
 * @package App\Controllers\Admin
 */
final class UserLoginController extends DefaultController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserLoginController constructor.
     *
     * @param EntityManagerInterface  $em
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        UserRepositoryInterface $userRepository
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (isset($data["email"], $data["password"])) {
            try {
                /** @var Role $role */
                $role = $this->em->find(Role::class, Role::TYPE_ADMIN);
                $user = $this->userRepository->findByEmail($data["email"]);
                if ($user->isActive() && password_verify($data["password"], $user->getPassword()) && $user->hasRole($role)) {
                    $token = new TokenEntity();
                    $user->addToken($token);

                    $this->em->flush();

                    return $response->withJson([
                        "token" => $token->getId(),
                    ]);
                }
            } catch (EntityNotFoundException $e) {
                // Do nothing
            }

            throw new InvalidCredentialsException();
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
