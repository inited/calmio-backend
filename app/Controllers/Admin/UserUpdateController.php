<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\LanguageNotExistsException;
use App\Model\Entity\Language;
use App\Model\Entity\Tag;
use App\Model\Repository\RoleRepositoryInterface;
use App\Model\Repository\TagRepositoryInterface;
use App\Model\Repository\UserRepositoryInterface;
use App\Model\Transformer\UserTransformer;
use DateTime;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class UserUpdateController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var RoleRepositoryInterface */
    private $roleRepository;

    /** @var TagRepositoryInterface */
    private $tagRepository;

    /** @var UserRepositoryInterface */
    private $userRepository;

    /** @var UserTransformer */
    private $userTransformer;

    public function __construct(
        EntityManagerInterface $em,
        RoleRepositoryInterface $roleRepository,
        TagRepositoryInterface $tagRepository,
        UserRepositoryInterface $userRepository,
        UserTransformer $userTransformer
    ) {
        parent::__construct();
        $this->em = $em;
        $this->roleRepository = $roleRepository;
        $this->tagRepository = $tagRepository;
        $this->userRepository = $userRepository;
        $this->userTransformer = $userTransformer;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $data = $request->getParsedBody();

        if (!isset($data["email"])) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        try {
            $user = $this->userRepository->findByEmail($data["email"]);

            if (!empty($data["email"]) && is_string($data["email"])) {
                $user->setEmail($data["email"]);
            }

            if (isset($data["experience"])) {
                /** @var null|Tag $experience */
                $experience = empty($data["experience"]) ? null : $this->tagRepository->find($data["experience"]);
                $user->setExperience($experience);
            }

            if (isset($data["fullVersionExpiration"])) {
                try {
                    $fullVersionExpiration = intval($data["fullVersionExpiration"]);
                    $fullVersionExpiration = 0 === $fullVersionExpiration ?
                        null : new DateTime(date("Y-m-d H:i:s", $fullVersionExpiration));
                    $user->setFullVersionExpiration($fullVersionExpiration);
                } catch (Exception $e) {
                    $user->setFullVersionExpiration(null);
                }
            }

            if (isset($data["installReason"])) {
                $installReason = [];
                if (is_array($data["installReason"]) && 0 < count($data["installReason"])) {
                    /** @var Tag[] $installReason */
                    $installReason = $this->tagRepository->findBy([
                        "id" => $data["installReason"],
                    ]);
                }
                $user->setInstallReason($installReason);
            }

            if (isset($data["language"])) {
                if (!in_array(strtolower($data["language"]), Language::getAvailableLanguages())) {
                    throw new LanguageNotExistsException();
                }

                $user->setLanguage($this->em->find(Language::class, strtolower($data['language'])));
            }

            if (isset($data["name"]) && is_string($data["name"])) {
                $user->setName($data["name"]);
            }

            if (isset($data["pushNotification"])) {
                $user->setPushNotification((bool) $data["pushNotification"]);
            }

            if (isset($data["roles"])) {
                $roles = empty($data["roles"]) ? [] : $this->roleRepository->findBy([
                    "id" => $data["roles"],
                ]);
                $user->setRoles($roles);
            }

            if (isset($data["surname"]) && is_string($data["surname"])) {
                $user->setSurname($data["surname"]);
            }

            $this->em->flush();

            return $response->withJson(
                $this->transformItem($user, $this->userTransformer)
            );
        } catch (EntityNotFoundException $e) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        } catch (UniqueConstraintViolationException $e) {
            return $response
                ->withJson([
                    "error" => sprintf("Email %s je již registrován jiným uživatelem!", $data["email"]),
                ])
                ->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }
    }
}
