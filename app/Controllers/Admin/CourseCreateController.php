<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\Course;
use App\Model\Provider\CourseProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class CourseCreateController
 * @package App\Controllers\Admin
 */
final class CourseCreateController extends DefaultController
{
    /**
     * @var CourseProviderInterface
     */
    private $courseProvider;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CourseCreateController constructor.
     *
     * @param CourseProviderInterface $courseProvider
     * @param EntityManagerInterface  $em
     */
    public function __construct(
        CourseProviderInterface $courseProvider,
        EntityManagerInterface $em
    ) {
        $this->em = $em;
        $this->courseProvider = $courseProvider;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();
        if (!empty($data) && is_array($data)) {
            $course = new Course();
            $this->courseProvider->updateCourse($course, $data);

            $this->em->persist($course);
            $this->em->flush();

            return $response->withJson($course->__toArray());
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
