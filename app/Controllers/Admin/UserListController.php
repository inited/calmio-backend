<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\InvalidArgumentException;
use App\Model\Entity\UserEntity;
use App\Model\Pagination\Paginator;
use App\Model\Repository\UserRepositoryInterface;
use App\Model\Transformer\UserTransformer;
use Slim\Http\Request;
use Slim\Http\Response;

final class UserListController extends DefaultController
{
    /** @var UserRepositoryInterface */
    private $userRepository;

    /** @var UserTransformer */
    private $userTransformer;

    public function __construct(
        UserRepositoryInterface $userRepository,
        UserTransformer $userTransformer
    ) {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->userTransformer = $userTransformer;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function __invoke(Request $request, Response $response): Response
    {
        $limit = $this->validateRequestLimit($request->getParam('limit'));
        $offset = $this->validateRequestOffset($request->getParam('offset'));

        $users = $this->userRepository->findAllByPaginated($limit, $offset);

        $data = collect($users)
            ->transform(function (UserEntity $user) {
                return $this->transformItem($user, $this->userTransformer);
            })
            ->all();

        return $response->withJson(
            (new Paginator($data, $users->count(), $limit, $offset))->transform()
        );
    }
}
