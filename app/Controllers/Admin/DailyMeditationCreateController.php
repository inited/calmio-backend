<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\Course;
use App\Model\Provider\CourseProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class DailyMeditationCreateController extends DefaultController
{
    /** @var CourseProviderInterface */
    private $courseProvider;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        CourseProviderInterface $courseProvider,
        EntityManagerInterface $em
    ) {
        $this->em = $em;
        $this->courseProvider = $courseProvider;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (empty($data) || !is_array($data)) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        $course = new Course();
        $course->setType(Course::TYPE_MEDITATION);
        $this->courseProvider->updateCourse($course, $data);

        $this->em->persist($course);
        $this->em->flush();

        return $response->withJson($course->__toArray());
    }
}
