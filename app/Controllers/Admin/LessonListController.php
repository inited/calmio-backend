<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\LessonEntity;
use App\Model\Repository\LessonRepositoryInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class LessonListController
 * @package App\Controllers\Admin
 */
final class LessonListController extends DefaultController
{
    /**
     * @var LessonRepositoryInterface
     */
    private $lessonRepository;

    /**
     * LessonListController constructor.
     *
     * @param LessonRepositoryInterface $lessonRepository
     */
    public function __construct(LessonRepositoryInterface $lessonRepository)
    {
        $this->lessonRepository = $lessonRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $lessonList = [];

        /** @var LessonEntity $lesson */
        foreach ($this->lessonRepository->findAll() as $lesson) {
            $lessonList[] = $lesson->__toArray();
        }

        return $response->withJson($lessonList);
    }

}
