<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\PartnerRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class PartnerCodeUpdateController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var PartnerRepositoryInterface */
    private $partnerRepository;

    public function __construct(
        EntityManagerInterface $em,
        PartnerRepositoryInterface $partnerRepository
    ) {
        $this->em = $em;
        $this->partnerRepository = $partnerRepository;
    }

    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $data = $request->getParsedBody();

        if (!isset($args["id"], $args["code"]) || empty($data["value"])) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        try {
            $partner = $this->partnerRepository->find($args["id"]);
        } catch (EntityNotFoundException $e) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        if (!$promoCode = $partner->getPromoCodeByCode($args["code"])) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        $promoCode->setValue((int) $data["value"]);
        $this->em->flush();

        return $response->withJson($promoCode->__toArray());
    }
}
