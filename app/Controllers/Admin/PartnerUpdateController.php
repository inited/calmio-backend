<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Model\Provider\PartnerProviderInterface;
use App\Model\Repository\PartnerRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class PartnerUpdateController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var PartnerProviderInterface */
    private $partnerProvider;

    /** @var PartnerRepositoryInterface */
    private $partnerRepository;

    public function __construct(
        EntityManagerInterface $em,
        PartnerProviderInterface $partnerProvider,
        PartnerRepositoryInterface $partnerRepository
    ) {
        $this->em = $em;
        $this->partnerProvider = $partnerProvider;
        $this->partnerRepository = $partnerRepository;
    }

    /**
     * @param array<string, mixed> $args
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $data = $request->getParsedBody();

        if (empty($data) || !is_array($data) || !isset($args["id"])) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        try {
            $partner = $this->partnerRepository->find($args["id"]);
        } catch (EntityNotFoundException $e) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        $this->partnerProvider->updatePartner($partner, $data);
        $this->em->flush();

        return $response->withJson($partner->__toArray());
    }
}
