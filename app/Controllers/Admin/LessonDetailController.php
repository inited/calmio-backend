<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\LessonEntity;
use App\Model\Repository\LessonRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class LessonDetailController
 * @package App\Controllers\Admin
 */
final class LessonDetailController extends DefaultController
{
    /**
     * @var LessonRepositoryInterface
     */
    private $lessonRepository;

    /**
     * LessonDetailController constructor.
     *
     * @param LessonRepositoryInterface $lessonRepository
     */
    public function __construct(LessonRepositoryInterface $lessonRepository)
    {
        $this->lessonRepository = $lessonRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (isset($args["id"])) {
            try {
                /** @var LessonEntity $lesson */
                $lesson = $this->lessonRepository->find($args["id"]);

                return $response->withJson($lesson->__toArray());
            } catch (EntityNotFoundException $e) {
                // Do nothing
            }
        }

        return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
    }

}
