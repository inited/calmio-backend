<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11.03.2020
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Version;
use App\Model\Repository\VersionRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class CourseCreateController
 * @package App\Controllers\Admin
 */
final class AppVersionUpdateController extends DefaultController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var VersionRepositoryInterface
     */
    private $versionRepository;

    /**
     * AppVersionUpdateController constructor.
     *
     * @param EntityManagerInterface     $em
     * @param VersionRepositoryInterface $versionRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        VersionRepositoryInterface $versionRepository
    ) {
        $this->em = $em;
        $this->versionRepository = $versionRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws EntityNotFoundException
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (empty($data['latestVersion'])) {
            throw new InvalidArgumentException('Parameter "latestVersion" is missing!');
        }

        /** @var Version $version */
        $version = $this->versionRepository->find(1);
        $version->setForceUpdateVersion($data['forceUpdateVersion'] ?? null);
        $version->setVersion($data['latestVersion']);

        $this->em->flush();

        return $response;
    }

}
