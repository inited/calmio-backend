<?php

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Model\Entity\Partner;
use App\Model\Provider\PartnerProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class PartnerCreateController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var PartnerProviderInterface */
    private $partnerProvider;

    public function __construct(
        EntityManagerInterface $em,
        PartnerProviderInterface $partnerProvider
    ) {
        $this->em = $em;
        $this->partnerProvider = $partnerProvider;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $data = $request->getParsedBody();

        if (empty($data) || !is_array($data)) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        $partner = new Partner();
        $this->partnerProvider->updatePartner($partner, $data);

        $this->em->persist($partner);
        $this->em->flush();

        return $response->withJson($partner->__toArray());
    }

}
