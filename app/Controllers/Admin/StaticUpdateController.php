<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\LanguageNotExistsException;
use App\Model\Entity\Article;
use App\Model\Entity\Language;
use App\Model\Repository\ArticleRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class StaticUpdateController
 * @package App\Controllers\Admin
 */
final class StaticUpdateController extends DefaultController
{
    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * StaticUpdateController constructor.
     *
     * @param ArticleRepositoryInterface $articleRepository
     * @param EntityManagerInterface     $em
     */
    public function __construct(
        ArticleRepositoryInterface $articleRepository,
        EntityManagerInterface $em
    ) {
        $this->articleRepository = $articleRepository;
        $this->em = $em;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws LanguageNotExistsException
     */
    public function defaultAction(Request $request, Response $response)
    {
        $body = $request->getParsedBody();

        if (isset($body["id"], $body["language"])) {
            if (!in_array($body["language"], Language::getAvailableLanguages())) {
                throw new LanguageNotExistsException();
            }

            try {
                /** @var Article $article */
                $article = $this->articleRepository->findByKeyAndLanguage($body["id"], $body["language"]);
            } catch (EntityNotFoundException $e) {
                $article = new Article();
                $article->setKey($body["id"]);
                $article->setLanguage($this->em->find(Language::class, strtolower($body["language"])));
            }

            if (isset($body["html"])) {
                $article->setContent((string) $body["html"]);
            }

            $this->em->persist($article);
            $this->em->flush();

            return $response->withJson($article->__toArray());
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
