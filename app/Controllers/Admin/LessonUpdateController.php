<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers\Admin;

use App\Controllers\DefaultController;
use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\LessonEntity;
use App\Model\Provider\LessonProviderInterface;
use App\Model\Repository\LessonRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class LessonUpdateController
 * @package App\Controllers\Admin
 */
final class LessonUpdateController extends DefaultController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LessonProviderInterface
     */
    private $lessonsProvider;

    /**
     * @var LessonRepositoryInterface
     */
    private $lessonRepository;

    /**
     * LessonUpdateController constructor.
     *
     * @param EntityManagerInterface    $em
     * @param LessonProviderInterface   $lessonsProvider
     * @param LessonRepositoryInterface $lessonRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        LessonProviderInterface $lessonsProvider,
        LessonRepositoryInterface $lessonRepository
    ) {
        $this->em = $em;
        $this->lessonsProvider = $lessonsProvider;
        $this->lessonRepository = $lessonRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        $data = $request->getParsedBody();
        if (!empty($data) && is_array($data) && isset($args["id"])) {
            try {
                /** @var LessonEntity $lesson */
                $lesson = $this->lessonRepository->find($args["id"]);
                $this->lessonsProvider->updateLesson($lesson, $data);

                $this->em->persist($lesson);
                $this->em->flush();

                return $response;
            } catch (EntityNotFoundException $e) {
                return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
            }
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
