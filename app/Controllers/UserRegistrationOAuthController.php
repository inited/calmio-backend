<?php

/**
 * @author: Tomas Pavlik <pavlik@sovanet.cz>
 * created: 05. 08. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InvalidArgumentException;
use App\Exceptions\LanguageNotExistsException;
use App\Model\Entity\Language;
use App\Model\Entity\Role;
use App\Model\Entity\TokenEntity;
use App\Model\Entity\UserEntity;
use App\Model\Repository\UserRepositoryInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class UserRegistrationOAuthController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var UserRepositoryInterface */
    private $userRepository;

    public function __construct(
        EntityManagerInterface $em,
        UserRepositoryInterface $userRepository
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (isset($data["email"], $data["name"], $data["oauth_id"], $data["surname"], $data["type"])) {
            if (false === in_array($data["type"], UserEntity::getTypes())) {
                throw new InvalidArgumentException(
                    sprintf("Chybný 'type' %s!", $data["type"])
                );
            }

            try {
                switch ($data["type"]) {
                    case UserEntity::TYPE_APPLE:
                        $user = $this->userRepository->findByAppleToken($data["oauth_id"]);
                        break;
                    case UserEntity::TYPE_FACEBOOK:
                        $user = $this->userRepository->findByFacebookToken($data["oauth_id"]);
                        break;
                    case UserEntity::TYPE_GOOGLE:
                        $user = $this->userRepository->findByGoogleToken($data["oauth_id"]);
                        break;
                    default:
                        throw new EntityNotFoundException();
                }
            } catch (EntityNotFoundException $e) {
                $user = new UserEntity();
                $user->setActive(true);
                $user->setEmail($data["email"]);
                /** @var Role $role */
                $role = $this->em->find(Role::class, Role::TYPE_USER);
                $user->setRoles([$role]);
            }

            $user->setLastLogin(new DateTime());
            $user->setName($data["name"] ?? "");
            $user->setSurname($data["surname"] ?? "");
            $user->setVerified(true);
            $user->setVerificationUntil(null);
            $user->setPasswordResetHash(null);

            $language = Language::TYPE_CS; // Default language
            if (isset($data["language"])) {
                if (!in_array(strtolower($data["language"]), Language::getAvailableLanguages())) {
                    throw new LanguageNotExistsException();
                }
                $language = strtolower($data["language"]);
            }
            $user->setLanguage($this->em->find(Language::class, $language));

            switch ($data["type"]) {
                case UserEntity::TYPE_APPLE:
                    $user->setAppleToken($data["oauth_id"]);
                    break;
                case UserEntity::TYPE_FACEBOOK:
                    $user->setFacebookToken($data["oauth_id"]);
                    break;
                case UserEntity::TYPE_GOOGLE:
                    $user->setGoogleToken($data["oauth_id"]);
                    break;
            }

            $token = new TokenEntity();
            $user->addToken($token);

            $this->em->persist($user);
            $this->em->flush();

            return $response->withJson([
                "token" => $token->getId(),
            ]);
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
