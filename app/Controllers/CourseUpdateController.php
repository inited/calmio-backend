<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 06.06.2020
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InvalidArgumentException;
use App\Model\Entity\Voice;
use App\Model\Repository\CourseRepositoryInterface;
use App\Services\TokenStorage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class CourseDetailController
 * @package App\Controllers
 */
final class CourseUpdateController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var CourseRepositoryInterface */
    private $courseRepository;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * CourseUpdateController constructor.
     *
     * @param EntityManagerInterface    $em
     * @param CourseRepositoryInterface $courseRepository
     * @param TokenStorageInterface     $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $em,
        CourseRepositoryInterface $courseRepository,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->courseRepository = $courseRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     * @throws InvalidArgumentException
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (empty($args["id"])) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        $body = $request->getParsedBody();
        if (!(isset($body["preferredVoice"]) && in_array($body["preferredVoice"], [Voice::MAN, Voice::WOMAN]))) {
            throw new InvalidArgumentException("Chybný parameter preferredVoice!");
        }

        if (null === $token = $this->tokenStorage->getToken()) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        try {
            $course = $this->courseRepository->find($args["id"]);
        } catch (EntityNotFoundException $e) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        $user = $token->getUser();

        if (null === $courseUser = $user->getEnrolledCourseByCourse($course)) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        $courseUser->setPreferredVoice(strtolower($body["preferredVoice"]));
        $this->em->flush();

        return $response;
    }

}
