<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Event\UserRegisteredEvent;
use App\Exceptions\InvalidArgumentException;
use App\Services\TokenStorage\TokenStorageInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Slim\Http\Request;
use Slim\Http\Response;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class UserResendVerificationEmailController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    public function __construct(
        EntityManagerInterface $em,
        EventDispatcherInterface $eventDispatcher,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
        $this->tokenStorage = $tokenStorage;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $user = $this->tokenStorage->getToken()->getUser();

        if ($user->isVerified()) {
            throw new InvalidArgumentException('User already verified!');
        }

        $user->setPasswordResetHash(Uuid::uuid4()->toString());
        $user->setVerificationUntil(new DateTime('+3days'));
        $this->em->flush();

        $this->eventDispatcher->dispatch(new UserRegisteredEvent($user));

        return $response;
    }

}
