<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\Course;
use App\Model\Entity\Version;
use App\Model\Repository\CourseRepositoryInterface;
use App\Services\Storage\LanguageStorageInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class CourseDailyMeditationListController extends DefaultController
{
    /** @var CourseRepositoryInterface */
    private $courseRepository;

    /** @var LanguageStorageInterface */
    private $languageStorage;

    public function __construct(
        CourseRepositoryInterface $courseRepository,
        LanguageStorageInterface $languageStorage
    ) {
        $this->courseRepository = $courseRepository;
        $this->languageStorage = $languageStorage;
    }

    public function defaultAction(Request $request, Response $response): Response
    {
        $coursesCriteria = [
            "type" => Course::TYPE_COURSE,
        ];

        $dailyMeditations = [];

        $courses = $this->courseRepository->findBy($coursesCriteria);

        $language = $this->languageStorage->getLanguage();
        
        if ($dailyMeditation = $this->courseRepository->findDailyMeditationByLanguage($language)) {
            $dailyMeditations = [$dailyMeditation];
        }

        return $response->withJson([
            "coursesAll" => collect($courses)
                ->transform(function (Course $course) {
                    return $course->__toArray();
                })
                ->all(),
            "dailyMeditations" => collect($dailyMeditations)
                ->transform(function (Course $dailyMeditation) {
                    return $dailyMeditation->__toArray();
                })
                ->all(),
        ]);
    }
}
