<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11. 08. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\UserRepositoryInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\NotFoundException;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

final class UserSetPasswordController extends DefaultController
{
    /** @var UserRepositoryInterface */
    private $userRepository;

    /** @var Twig */
    private $view;

    public function __construct(
        Twig $view,
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
        $this->view = $view;
    }

    public function defaultAction(Request $request, Response $response, array $args): ResponseInterface
    {
        if (empty($args['hash'])) {
            throw new NotFoundException($request, $response);
        }

        $hash = $args['hash'];

        try {
            $this->userRepository->findByPasswordResetHash($hash);
        } catch (EntityNotFoundException $e) {
            throw new NotFoundException($request, $response);
        }

        return $this->view->render($response, 'setPassword.html.twig', [
            'hash' => $hash,
        ]);
    }
}
