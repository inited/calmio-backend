<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\UserRepositoryInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\NotFoundException;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

final class UserAccountActivationController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var UserRepositoryInterface */
    private $userRepository;

    /** @var Twig */
    private $view;

    public function __construct(
        EntityManagerInterface $em,
        Twig $view,
        UserRepositoryInterface $userRepository
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->view = $view;
    }

    public function __invoke(Request $request, Response $response, array $args): ResponseInterface
    {
        if (empty($args['hash'])) {
            throw new NotFoundException($request, $response);
        }

        try {
            $user = $this->userRepository->findByPasswordResetHash($args['hash']);
        } catch (EntityNotFoundException $e) {
            throw new NotFoundException($request, $response);
        }

        $now = new DateTime();

        if ($now->getTimestamp() <= $user->getVerificationUntil()->getTimestamp()) {
            $user->setPasswordResetHash(null);
            $user->setVerified(true);
            $user->setVerificationUntil(null);
            $this->em->flush();

            $template = 'accountVerified.html.twig';
        } else {
            $template = 'accountVerificationFailed.html.twig';
        }

        return $this->view->render($response, $template);
    }
}
