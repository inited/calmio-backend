<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11.03.2020
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\PromoCode;
use App\Model\Repository\PromoCodeRepository;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class PartnerCodeController extends DefaultController
{
    /** @var PromoCodeRepository */
    private $promoCodeRepository;

    public function __construct(PromoCodeRepository $promoCodeRepository)
    {
        $this->promoCodeRepository = $promoCodeRepository;
    }

    public function __invoke(Request $request, Response $response, array $args): Response
    {
        /** @var PromoCode|null $promoCode */
        $promoCode = $this->promoCodeRepository->findOneBy([
            "id" => $args["id"],
            "type" => PromoCode::TYPE_PROMO,
        ]);

        if (!$promoCode) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        return $response->withJson([
            "code" => $promoCode->getId(),
            "partnerName" => $promoCode->getPartnerName(),
        ]);
    }
}
