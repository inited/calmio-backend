<?php

/**
 * @author: Tomas Pavlik <pavlik@sovanet.cz>
 * created: 05. 08. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InvalidCredentialsException;
use App\Model\Entity\TokenEntity;
use App\Model\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class UserLoginPasswordController
 * @package App\Controllers
 */
final class UserLoginPasswordController extends DefaultController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UserLoginPasswordController constructor.
     *
     * @param EntityManagerInterface  $em
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        UserRepositoryInterface $userRepository
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (isset($data["email"], $data["password"])) {
            try {
                $user = $this->userRepository->findByEmail($data["email"]);

                $passwordVerified = password_verify($data["password"], $user->getPassword());

                if ($user->isActive() && null !== $user->getPassword() && $passwordVerified) {
                    $token = new TokenEntity();
                    $user->addToken($token);

                    $this->em->flush();

                    return $response->withJson([
                        "token" => $token->getId(),
                    ]);
                }

                throw new InvalidCredentialsException();
            } catch (EntityNotFoundException $e) {
                throw new InvalidCredentialsException();
            }
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
