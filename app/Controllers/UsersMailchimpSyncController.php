<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 01.04.2020
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\UserEntity;
use App\Model\Repository\UserRepositoryInterface;
use App\Services\MailchimpService\MailchimpServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Tracy\Debugger;

/**
 * Class UsersMailchimpSyncController
 * @package App\Controllers\Admin
 */
final class UsersMailchimpSyncController extends DefaultController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var MailchimpServiceInterface
     */
    private $mailchimpService;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * UsersMailchimpSyncController constructor.
     *
     * @param EntityManagerInterface    $em
     * @param MailchimpServiceInterface $mailchimpService
     * @param UserRepositoryInterface   $userRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        MailchimpServiceInterface $mailchimpService,
        UserRepositoryInterface $userRepository
    ) {
        $this->em = $em;
        $this->mailchimpService = $mailchimpService;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function defaultAction(Request $request, Response $response)
    {

        // Volani Mailchimpu vypnuto, nepouziva se
        return $response->withJson('Finished.');

        /* if (!Debugger::$productionMode) {
            return $response->withJson('Finished.');
        } */

        // /** @var UserEntity[] $users */
        /* $users = $this->userRepository->findAll();

        foreach ($users as $user) {
            try {
                if (null === $user->getMailchimpId()) {
                    $user->setMailchimpId($this->mailchimpService->createMember($user));
                    $this->em->getUnitOfWork()->commit($user);
                } else {
                    $this->mailchimpService->updateMember($user);
                }
            } catch (Exception $e) {
                continue;
            }
        }

        return $response->withJson('Finished.'); */
    }

}
