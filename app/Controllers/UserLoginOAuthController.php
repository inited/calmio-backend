<?php

/**
 * @author: Tomas Pavlik <pavlik@sovanet.cz>
 * created: 05. 08. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InvalidArgumentException;
use App\Exceptions\InvalidCredentialsException;
use App\Exceptions\LanguageNotExistsException;
use App\Model\Entity\Language;
use App\Model\Entity\Role;
use App\Model\Entity\TokenEntity;
use App\Model\Entity\UserEntity;
use App\Model\Repository\UserRepositoryInterface;
use App\Services\FacebookService\FacebookAuthMissingEmailException;
use App\Services\FacebookService\FacebookServiceException;
use App\Services\FacebookService\FacebookServiceInterface;
use App\Services\GoogleService\GoogleServiceException;
use App\Services\GoogleService\GoogleServiceInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Ramsey\Uuid\Uuid;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class UserLoginOAuthController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var FacebookServiceInterface */
    private $facebookService;

    /** @var GoogleServiceInterface */
    private $googleService;

    /** @var UserRepositoryInterface */
    private $userRepository;

    public function __construct(
        EntityManagerInterface $em,
        FacebookServiceInterface $facebookService,
        GoogleServiceInterface $googleService,
        UserRepositoryInterface $userRepository
    ) {
        $this->em = $em;
        $this->facebookService = $facebookService;
        $this->googleService = $googleService;
        $this->userRepository = $userRepository;
    }

    /**
     * @param UserEntity $user
     * @param array      $data
     *
     * @throws Exception
     */
    private function setLanguage(UserEntity $user, array $data): void
    {
        $language = Language::TYPE_CS; // Default language

        if (isset($data["language"])) {
            if (!in_array(strtolower($data["language"]), Language::getAvailableLanguages())) {
                throw new LanguageNotExistsException();
            }
            $language = strtolower($data['language']);
        }

        $user->setLanguage($this->em->find(Language::class, $language));
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     * @throws FacebookAuthMissingEmailException
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (isset($data["type"], $data["oauth_id"])) {
            if (false === in_array($data["type"], UserEntity::getTypes())) {
                throw new InvalidArgumentException(
                    sprintf("Chybný 'type' %s!", $data["type"])
                );
            }

            switch ($data["type"]) {
                case UserEntity::TYPE_APPLE:
                    try {
                        $user = $this->userRepository->findByAppleToken($data["oauth_id"]);
                    } catch (EntityNotFoundException $e) {
                    }

                    if (!isset($user) && !empty($data["apple_auth_details"]["email"])) {
                        try {
                            $user = $this->userRepository->findByEmail($data["apple_auth_details"]["email"]);
                        } catch (EntityNotFoundException $e) {
                        }
                    }

                    if (!isset($user)) {
                        $user = new UserEntity();
                        $user->setActive(true);
                        $user->setEmail($data["apple_auth_details"]["email"] ?? (Uuid::uuid4()->toString() . '@calmio.cz'));
                        $user->setName($data["apple_auth_details"]["fullName"]["givenName"] ?? "");
                        $user->setSurname($data["apple_auth_details"]["fullName"]["familyName"] ?? "");
                        /** @var Role $role */
                        $role = $this->em->find(Role::class, Role::TYPE_USER);
                        $user->setRoles([$role]);
                        $this->setLanguage($user, $data);
                    }

                    $user->setAppleToken($data["oauth_id"]);
                    break;
                case UserEntity::TYPE_FACEBOOK:
                    try {
                        $facebookUser = $this->facebookService->getUserInfo($data["oauth_id"]);
                    } catch (FacebookServiceException $e) {
                        throw new InvalidArgumentException("Facebook> Chyba ověření uživatele!");
                    }

                    try {
                        $user = $this->userRepository->findByFacebookToken($facebookUser->getId());
                    } catch (EntityNotFoundException $e) {
                        // Do nothing
                    }

                    if (!isset($user)) {
                        try {
                            $user = $this->userRepository->findByEmail($facebookUser->getEmail());
                        } catch (EntityNotFoundException $e) {
                            $user = new UserEntity();
                            $user->setActive(true);
                            $user->setEmail($facebookUser->getEmail());
                            $user->setName($facebookUser->getFirstName());
                            $user->setSurname($facebookUser->getLastName());
                            /** @var Role $role */
                            $role = $this->em->find(Role::class, Role::TYPE_USER);
                            $user->setRoles([$role]);
                            $this->setLanguage($user, $data);
                        }
                    }
                    $user->setFacebookToken($facebookUser->getId());
                    break;
                case UserEntity::TYPE_GOOGLE:
                    try {
                        $googleUser = $this->googleService->getUserInfo($data["oauth_id"]);
                    } catch (GoogleServiceException $e) {
                        throw new InvalidArgumentException("Google> Chyba ověření uživatele!");
                    }

                    try {
                        $user = $this->userRepository->findByGoogleToken($googleUser->getId());
                    } catch (EntityNotFoundException $e) {
                        // Do nothing
                    }
                    if (!isset($user)) {
                        try {
                            $user = $this->userRepository->findByEmail($googleUser->getEmail());
                        } catch (EntityNotFoundException $e) {
                            $user = new UserEntity();
                            $user->setActive(true);
                            $user->setEmail($googleUser->getEmail());
                            $user->setName($googleUser->getFirstName());
                            $user->setSurname($googleUser->getLastName());
                            /** @var Role $role */
                            $role = $this->em->find(Role::class, Role::TYPE_USER);
                            $user->setRoles([$role]);
                            $this->setLanguage($user, $data);
                        }
                    }
                    $user->setGoogleToken($googleUser->getId());
                    break;
                default:
                    throw new InvalidCredentialsException();
            }

            if ($user->isActive()) {
                $token = new TokenEntity();
                $user->addToken($token);
                $user->setLastLogin(new DateTime());
                $user->setVerified(true);
                $user->setVerificationUntil(null);
                $user->setPasswordResetHash(null);

                $this->em->persist($user);
                $this->em->flush();

                return $response->withJson([
                    "token" => $token->getId(),
                ]);
            }

            throw new InvalidCredentialsException();
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
