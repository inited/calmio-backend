<?php

namespace App\Controllers;

use App\Exceptions\InvalidArgumentException;
use App\Services\Storage\DeviceStorage;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class UserAddPushTokenController extends DefaultController
{
    /** @var DeviceStorage */
    private $deviceStorage;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(
        DeviceStorage $deviceStorage,
        EntityManagerInterface $em
    ) {
        parent::__construct();
        $this->deviceStorage = $deviceStorage;
        $this->em = $em;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        if (!$platform = $request->getParam('platform')) {
            throw new InvalidArgumentException("Platforma nezadána!");
        }

        if (!$token = $request->getParam('token')) {
            throw new InvalidArgumentException("Token nezadán!");
        }

        if (!$device = $this->deviceStorage->getDevice()) {
            return $response;
        }

        $device->setPlatform($platform);
        $device->setPushToken($token);

        $this->em->flush();

        return $response;
    }
}
