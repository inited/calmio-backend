<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11.03.2020
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Version;
use App\Model\Repository\VersionRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AppVersionController
 * @package App\Controllers
 */
final class AppVersionController extends DefaultController
{
    /**
     * @var VersionRepositoryInterface
     */
    private $versionRepository;

    /**
     * AppVersionController constructor.
     *
     * @param VersionRepositoryInterface $versionRepository
     */
    public function __construct(VersionRepositoryInterface $versionRepository)
    {
        $this->versionRepository = $versionRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws EntityNotFoundException
     */
    public function defaultAction(Request $request, Response $response)
    {
        /** @var Version $version */
        $version = $this->versionRepository->find(1);

        return $response->withJson([
            'forceUpdateVersion' => $version->getForceUpdateVersion(),
            'latestVersion' => $version->getVersion(),
        ]);
    }

}
