<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 23. 01. 2020
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Services\TokenStorage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class UserPingController
 * @package App\Controllers
 */
final class UserPingController extends DefaultController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * UserPingController constructor.
     *
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface  $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        if (null !== $token = $this->tokenStorage->getToken()) {
            $user = $token->getUser();
            $user->setLastLogin(new \DateTime());

            $this->em->flush();
        }

        return $response->withStatus(StatusCode::HTTP_OK);
    }

}
