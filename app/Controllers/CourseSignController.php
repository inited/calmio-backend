<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11. 08. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Course;
use App\Model\Repository\CourseRepositoryInterface;
use App\Services\TokenStorage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class CourseDetailController
 * @package App\Controllers
 */
final class CourseSignController extends DefaultController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var CourseRepositoryInterface
     */
    private $courseRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * CourseSignController constructor.
     *
     * @param EntityManagerInterface    $em
     * @param CourseRepositoryInterface $courseRepository
     * @param TokenStorageInterface     $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $em,
        CourseRepositoryInterface $courseRepository,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->courseRepository = $courseRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (isset($args["id"])) {
            if (null !== $token = $this->tokenStorage->getToken()) {
                try {
                    $user = $token->getUser();
                    /** @var Course $course */
                    $course = $this->courseRepository->find($args["id"]);
                    $user->signToCourse($course);

                    $this->em->flush();

                    return $response;
                } catch (EntityNotFoundException $e) {
                    return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
                }
            }
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
