<?php

namespace App\Controllers;

use App\Model\Entity\Analytics;
use App\Model\Entity\Device;
use App\Model\Entity\Subscription;
use App\Model\Entity\SubscriptionLog;
use App\Services\PurchaseSubscriptionValidation\PurchaseValidationService;
use App\Services\Storage\DeviceStorage;
use App\Services\Validation\ValidationFactory;
use DateTime;
use Doctrine\ORM\EntityManager;
use Slim\Http\Request;
use Slim\Http\Response;

final class PurchaseSubscriptionVerificationController extends DefaultController
{
    /** @var DeviceStorage */
    private $deviceStorage;

    /** @var EntityManager */
    private $em;

    /** @var PurchaseValidationService */
    private $purchaseValidationService;

    public function __construct(
        DeviceStorage $deviceStorage,
        EntityManager $em,
        PurchaseValidationService $purchaseValidationService,
        ValidationFactory $validationFactory
    ) {
        parent::__construct();
        $this->deviceStorage = $deviceStorage;
        $this->em = $em;
        $this->purchaseValidationService = $purchaseValidationService;
        $this->validationFactory = $validationFactory;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $errors = $this->validateRequest((array) $request->getParsedBody(), [
            "lazy" => "filled|boolean",
            "platform" => "required|string|in:" . implode(',', Device::AVAILABLE_PLATFORMS),
            "receipts" => "required|array",
            "receipts.*.receipt" => "required|string",
            "receipts.*.productId" => "required_if:platform," . Device::PLATFORM_ANDROID . "|string|in:"
                . implode(',', Analytics::AVAILABLE_SUBSCRIPTION_TYPES),
        ]);

        if ($errors) {
            return $this->respondWithErrors($response, $errors);
        }

        $device = $this->deviceStorage->getDevice();
        $platform = $request->getParam("platform");
        $receipts = $request->getParam("receipts");
        $lazy = (bool) $request->getParam("lazy", false);

        if ($device) {
            collect($this->purchaseValidationService->validateSubscriptions($platform, $receipts, $lazy))
                ->each(function (Subscription $subscription) use ($device) {
                    $device->addSubscription($subscription);
                });

            // TODO temporarily
            $this->logSubscriptions($request, $device);

            $this->em->flush();
        }

        return $this->respondWithNoContent($response);
    }

    private function logSubscriptions(Request $request, Device $device): void
    {
        $appCurrentSubscriptions = $request->getParam("subscriptions", []);
        $beCurrentSubscriptions = collect($device->getSubscriptions())
            ->filter(function (Subscription $subscription) {
                $now = new DateTime();
                $transaction = $subscription->getLastValidTransaction();
                return $transaction && $now->getTimestamp() < $transaction->getValidTo()->getTimestamp();
            })
            ->map(function (Subscription $subscription) {
                $transaction = $subscription->getLastValidTransaction();

                return [
                    'id' => $transaction->getProductId(),
                    'type' => $transaction->getType(),
                    'expirationTimestamp' => $transaction->getValidTo()->getTimestamp(),
                    'receipt' => [
                        'orderId' => $transaction->getOrderId(),
                        'productId' => $transaction->getProductId(),
                        'purchaseTime' => $transaction->getPurchasedAt()->getTimestamp(),
                        'purchaseState' => $transaction->getPaymentState(),
                        'purchaseToken' => $subscription->getToken(),
                        'autoRenewing' => $transaction->isAutoRenewing(),
                    ],
                ];
            })
            ->values();

        $trouble = false;
        collect($appCurrentSubscriptions)
            ->each(function ($subscription) use (&$trouble, $beCurrentSubscriptions) {
                $productId = $subscription['id'];
                $type = $subscription['type'];

                $cnt = collect($beCurrentSubscriptions)
                    ->filter(function ($subscription) use ($productId, $type) {
                        return $subscription['id'] === $productId && $subscription['type'] === $type;
                    })
                    ->count();

                if (!$cnt) {
                    $trouble = true;
                }
            });

        $subscriptionLog = new SubscriptionLog(
            $device,
            json_encode($appCurrentSubscriptions),
            json_encode($beCurrentSubscriptions),
            $trouble
        );

        $this->em->persist($subscriptionLog);
    }
}
