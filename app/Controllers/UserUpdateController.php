<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 08. 09. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\InvalidArgumentException;
use App\Exceptions\LanguageNotExistsException;
use App\Exceptions\UserWrongOriginalPasswordException;
use App\Model\Entity\Language;
use App\Model\Entity\Tag;
use App\Model\Entity\Voice;
use App\Model\Repository\TagRepositoryInterface;
use App\Model\Transformer\UserTransformer;
use App\Services\TokenStorage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class UserUpdateController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var TagRepositoryInterface */
    private $tagRepository;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var UserTransformer */
    private $userTransformer;

    public function __construct(
        EntityManagerInterface $em,
        TagRepositoryInterface $tagRepository,
        TokenStorageInterface $tokenStorage,
        UserTransformer $userTransformer
    ) {
        parent::__construct();
        $this->em = $em;
        $this->tagRepository = $tagRepository;
        $this->tokenStorage = $tokenStorage;
        $this->userTransformer = $userTransformer;
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $body = $request->getParsedBody();

        if (!$token = $this->tokenStorage->getToken()) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        $user = $token->getUser();

        if (isset($body["experience"])) {
            /** @var null|Tag $experience */
            $experience = empty($body["experience"]) ? null : $this->tagRepository->find($body["experience"]);
            $user->setExperience($experience);
        }

        if (isset($body["installReason"])) {
            $installReason = [];
            if (is_array($body["installReason"]) && 0 < count($body["installReason"])) {
                /** @var Tag[] $installReason */
                $installReason = $this->tagRepository->findBy([
                    "id" => $body["installReason"],
                ]);
            }
            $user->setInstallReason($installReason);
        }

        if (isset($body["name"])) {
            $user->setName((string) $body["name"]);
        }

        if (isset($body["language"])) {
            if (!in_array(strtolower($body["language"]), Language::getAvailableLanguages())) {
                throw new LanguageNotExistsException();
            }
            $user->setLanguage($this->em->find(Language::class, strtolower($body['language'])));
        }

        if (isset($body["pushNotification"])) {
            $user->setPushNotification((bool) $body["pushNotification"]);
        }

        if (isset($body["passwordOld"], $body["passwordNew"])) {
            if (false === password_verify($body["passwordOld"], $user->getPassword())) {
                throw new UserWrongOriginalPasswordException("Nesouhlasí původní heslo!");
            }

            $password = password_hash($body["passwordNew"], PASSWORD_DEFAULT);
            if (false !== $password) {
                $user->setPassword($password);
            }
        }

        if (array_key_exists("preferredVoice", $body)) {
            $preferredVoice = strtolower($body["preferredVoice"]);
            if (!in_array($body["preferredVoice"], [Voice::MAN, Voice::WOMAN])) {
                throw new InvalidArgumentException("Chybný parametr preferredVoice!");
            }
            $user->setPreferredVoice($preferredVoice);
        }

        if (isset($body["surname"])) {
            $user->setSurname((string) $body["surname"]);
        }

        $this->em->flush();

        $userData = $this->transformItem($token->getUser(), $this->userTransformer);

        if (!$request->getParam("version")) {
            $userData["coursesOwned"] = collect($userData["coursesOwned"])
                ->where("free", true)
                ->all();
        }

        return $response->withJson($userData);
    }
}
