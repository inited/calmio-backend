<?php

/**
 * @author: Tomas Pavlik <pavlik@sovanet.cz>
 * created: 07. 08. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InvalidEmailException;
use App\Model\Repository\UserRepositoryInterface;
use App\Services\MailService\MailServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;
use Symfony\Component\Translation\Translator;

final class UserResetPasswordController extends DefaultController
{
    /** @var string */
    private $apiUri;

    /** @var EntityManagerInterface */
    private $em;

    /** @var MailServiceInterface */
    private $mailService;

    /** @var Translator */
    private $translator;

    /** @var UserRepositoryInterface */
    private $userRepository;

    public function __construct(
        string $apiUri,
        EntityManagerInterface $em,
        MailServiceInterface $mailService,
        Translator $translator,
        UserRepositoryInterface $userRepository
    ) {
        $this->apiUri = $apiUri;
        $this->em = $em;
        $this->mailService = $mailService;
        $this->translator = $translator;
        $this->userRepository = $userRepository;
    }

    public function defaultAction(Request $request, Response $response): Response
    {
        $data = $request->getParsedBody();

        if (empty($data['email'])) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        $email = $data['email'];

        if (false === filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmailException(
                sprintf('Zadaný email %s není validní!', $email)
            );
        }

        try {
            $user = $this->userRepository->findByEmail($email);
            $user->setPasswordResetHash(Uuid::uuid4()->toString());
        } catch (EntityNotFoundException $e) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        // Set translator locale by user language
        $this->translator->setLocale($user->getLanguageCode());

        $this->mailService->sendTemplate(
            $user->getEmail(),
            $this->translator->trans('page.forgotten_password.title'),
            'resetPassword.html.twig',
            [
                'uri' => rtrim($this->apiUri, '/')
                    . '/'
                    . $user->getLanguageCode()
                    . '/user/set-password/'
                    . $user->getPasswordResetHash(),
            ]
        );

        $this->em->flush();

        return $response;
    }
}
