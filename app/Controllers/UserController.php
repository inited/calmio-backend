<?php
namespace App\Controllers;

use App\Model\Transformer\UserTransformer;
use App\Services\TokenStorage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Illuminate\Support\Str;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

final class UserController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var UserTransformer */
    private $userTransformer;

    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        UserTransformer $userTransformer
    ) {
        parent::__construct();
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->userTransformer = $userTransformer;
    }

    public function show(Request $request, Response $response): Response
    {
        if (!$token = $this->tokenStorage->getToken()) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        $userData = $this->transformItem($token->getUser(), $this->userTransformer);

        if (!$request->getParam("version")) {
            $userData["coursesOwned"] = collect($userData["coursesOwned"])
                ->where("free", true)
                ->all();
        }

        return $response->withJson($userData);
    }

    public function destroy(Request $request, Response $response): Response
    {
        if (!$token = $this->tokenStorage->getToken()) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        $token->setValid(false);
        $user = $token->getUser();
        $email = $user->getEmail();

        $user->setEmail(str_replace(strstr($email, '@', true), Str::uuid()->toString(), $email))
            ->setFacebookToken(null)
            ->setGoogleToken(null)
            ->setAppleToken(null)
            ->setName('')
            ->setSurname('');

        $this->em->flush();

        return $response->withStatus(StatusCode::HTTP_NO_CONTENT);
    }
}
