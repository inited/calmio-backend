<?php
namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Course;
use App\Model\Entity\CourseUserLesson;
use App\Model\Entity\Voice;
use App\Model\Repository\CourseRepositoryInterface;
use App\Model\Repository\LecturerRepositoryInterface;
use App\Services\Storage\LanguageStorageInterface;
use App\Services\TokenStorage\TokenStorageInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;
use Tracy\Debugger;

final class LessonFinishController extends DefaultController
{
    /** @var CourseRepositoryInterface */
    private $courseRepository;

    /** @var EntityManagerInterface */
    private $em;

    /** @var LanguageStorageInterface */
    private $languageStorage;

    /** @var LecturerRepositoryInterface */
    private $lecturerRepository;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    public function __construct(
        CourseRepositoryInterface $courseRepository,
        EntityManagerInterface $em,
        LanguageStorageInterface $languageStorage,
        LecturerRepositoryInterface $lecturerRepository,
        TokenStorageInterface $tokenStorage
    )
    {
        parent::__construct();
        $this->courseRepository = $courseRepository;
        $this->em = $em;
        $this->languageStorage = $languageStorage;
        $this->lecturerRepository = $lecturerRepository;
        $this->tokenStorage = $tokenStorage;
    }

    public function defaultAction(Request $request, Response $response): Response
    {
        $body = $request->getParsedBody();

        if (!isset($body["id"], $body["courseId"], $body["time"], $body["unfinishedTime"], $body["completed"], $body["timestamp"])) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        if (!$token = $this->tokenStorage->getToken()) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        try {
            /** @var Course $course */
            $course = $this->courseRepository->find((int) $body["courseId"]);
        } catch (EntityNotFoundException $e) {
            return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
        }

        if (!$lessonEntity = $course->getLessonById((int) $body["id"])) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        $user = $token->getUser();

        if (isset($body['continuousDays'])) {
            $user->setContinuousDays((int) $body['continuousDays']);
        }

        if (!$courseUser = $user->getEnrolledCourseByCourse($course)) {
            return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
        }

        $completed = (bool) $body['completed'];
        $time = (int) $body["time"];
        $timestamp = new DateTime(date("Y-m-d H:i:s", (int) $body["timestamp"]));
        $unfinishedTime = (int) $body['unfinishedTime'];

        if (!$courseUserLesson = $courseUser->getLesson($lessonEntity)) {
            $courseUserLesson = new CourseUserLesson();
            $courseUserLesson->setCourseUser($courseUser);
            $courseUserLesson->setLesson($lessonEntity);
            $courseUser->addCourseUserLesson($courseUserLesson);
        }

        $courseUserLesson->setCompleted($completed);
        $courseUserLesson->setCurrentSessionCompleted((bool) ($body["currentSessionCompleted"] ?? false));
        $courseUserLesson->setCurrentSessionTime((int) ($body["currentSessionTime"] ?? false));
        $courseUserLesson->setFinished($timestamp);
        $courseUserLesson->setLanguage($this->languageStorage->getLanguage());
        $courseUserLesson->setLessonDurationSeconds((int) ($body["lessonDurationSeconds"] ?? 0));
        $courseUserLesson->setTime($time);
        $courseUserLesson->setUnfinishedTime($unfinishedTime);
        $courseUserLesson->setVoice((string) ($body["voice"] ?? Voice::MAN));

        if (isset($body["lecturerId"])) {
            try {
                $courseUserLesson->setLecturer($this->lecturerRepository->find($body["lecturerId"]));
                $courseUserLesson->setVoice($courseUserLesson->getLecturer()->getGender());
            } catch (EntityNotFoundException $e) {
                Debugger::log($e, Debugger::EXCEPTION);
            }
        }

        $this->em->flush();

        return $response;
    }
}
