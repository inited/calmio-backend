<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11. 08. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Exception\NotFoundException;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

final class UserSavePasswordController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var UserRepositoryInterface */
    private $userRepository;

    /** @var Twig */
    private $view;

    public function __construct(
        EntityManagerInterface $em,
        Twig $view,
        UserRepositoryInterface $userRepository
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->view = $view;
    }

    public function defaultAction(Request $request, Response $response): ResponseInterface
    {
        $body = $request->getParsedBody();

        if (!isset($body['hash'], $body['password'])) {
            throw new NotFoundException($request, $response);
        }

        if (!$password = password_hash($body['password'], PASSWORD_DEFAULT)) {
            throw new NotFoundException($request, $response);
        }

        try {
            $user = $this->userRepository->findByPasswordResetHash($body['hash']);
        } catch (EntityNotFoundException $e) {
            throw new NotFoundException($request, $response);
        }

        $user->setPassword($password);
        $user->setPasswordResetHash(null);

        $this->em->flush();

        return $this->view->render($response, 'savePassword.html.twig');
    }

}
