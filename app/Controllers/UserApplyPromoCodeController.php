<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\PromoCodeAlreadyUsedException;
use App\Exceptions\PromoCodeAnotherIsActiveException;
use App\Exceptions\PromoCodeInvalidException;
use App\Model\Entity\PromoCode;
use App\Model\Entity\UserEntity;
use App\Model\Entity\UserPromoCodeHistoryEntity;
use App\Model\Repository\PromoCodeRepositoryInterface;
use App\Model\Transformer\UserTransformer;
use App\Services\TokenStorage\TokenStorageInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class UserApplyPromoCodeController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var PromoCodeRepositoryInterface */
    private $promoCodeRepository;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var UserTransformer */
    private $userTransformer;

    public function __construct(
        EntityManagerInterface $em,
        PromoCodeRepositoryInterface $promoCodeRepository,
        TokenStorageInterface $tokenStorage,
        UserTransformer $userTransformer
    ) {
        parent::__construct();
        $this->em = $em;
        $this->promoCodeRepository = $promoCodeRepository;
        $this->tokenStorage = $tokenStorage;
        $this->userTransformer = $userTransformer;
    }

    /**
     * @throws PromoCodeInvalidException
     */
    private function findPromoCode(string $code): PromoCode
    {
        try {
            return $this->promoCodeRepository->find($code);
        } catch (EntityNotFoundException $e) {
            throw new PromoCodeInvalidException("Promo code not found!", 400);
        }
    }

    /**
     * @throws PromoCodeAlreadyUsedException
     * @throws PromoCodeAnotherIsActiveException
     */
    private function validateIsPossibleToUsePromoCode(PromoCode $promoCode, UserEntity $user): void
    {
        if ($user->hasActivePromoCode()) {
            throw new PromoCodeAnotherIsActiveException("Another promo code is still active!", 400);
        }

        if ($user->hasPromoCodeEverBeenUsed($promoCode)) {
            throw new PromoCodeAlreadyUsedException("Promo code has been already used!", 400);
        }
    }

    /**
     * @throws PromoCodeInvalidException
     */
    private function validatePromoCode(string $type, PromoCode $promoCode): void
    {
        $partner = $promoCode->getPartner();

        if (!($promoCode->isValid() && ($type === $promoCode->getType()) && (!$partner || !$partner->isDeleted()))) {
            throw new PromoCodeInvalidException("Promo code is not valid!", 400);
        }
    }

    /**
     * @throws PromoCodeInvalidException
     */
    private function validateRequestData(Request $request): void
    {
        $body = $request->getParsedBody();

        if (empty($body["name"])) {
            throw new PromoCodeInvalidException("Promo code is empty!", 400);
        }
    }

    /**
     * @throws PromoCodeAlreadyUsedException
     * @throws PromoCodeAnotherIsActiveException
     * @throws PromoCodeInvalidException
     */
    public function giftAction(Request $request, Response $response): Response
    {
        $this->validateRequestData($request);

        $promoCode = $this->findPromoCode($request->getParsedBody()["name"]);
        $user = $this->tokenStorage->getToken()->getUser();

        $this->validatePromoCode(PromoCode::TYPE_GIFT, $promoCode);
        $this->validateIsPossibleToUsePromoCode($promoCode, $user);

        $user->usePromoCode($promoCode);
        $this->em->flush();

        return $response->withJson(
            $this->transformItem($user, $this->userTransformer)
        );
    }

    /**
     * @throws PromoCodeAlreadyUsedException
     * @throws PromoCodeAnotherIsActiveException
     * @throws PromoCodeInvalidException
     */
    public function promoAction(Request $request, Response $response): Response
    {
        $this->validateRequestData($request);

        $promoCode = $this->findPromoCode($request->getParsedBody()["name"]);
        $user = $this->tokenStorage->getToken()->getUser();

        $this->validatePromoCode(PromoCode::TYPE_PROMO, $promoCode);

        if ($user->hasActivePromoCode()) {
            throw new PromoCodeAnotherIsActiveException("Another promo code is still active!", 400);
        }

        $dateYearAgo = new DateTime('-1year');
        $countOfUsedPromoCodesLessThenYearAgo = collect($user->getPromoCodes())
            ->filter(function (UserPromoCodeHistoryEntity $entity) use ($dateYearAgo) {
                if (PromoCode::TYPE_PROMO !== $entity->getPromoCode()->getType()) {
                    return false;
                }

                if ($dateYearAgo > $entity->getPromoCodeActiveFrom()) {
                    return false;
                }

                return true;
            })
            ->count();

        if ($countOfUsedPromoCodesLessThenYearAgo) {
            throw new PromoCodeAlreadyUsedException("Promo code has been already used!", 400);
        }

        $user->usePromoCode($promoCode);
        $this->em->flush();

        return $response->withJson(
            $this->transformItem($user, $this->userTransformer)
        );
    }
}
