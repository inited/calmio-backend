<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 22. 05. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Event\UserRegisteredEvent;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InvalidArgumentException;
use App\Exceptions\InvalidEmailException;
use App\Exceptions\LanguageNotExistsException;
use App\Exceptions\UserAlreadyExistsException;
use App\Model\Entity\Language;
use App\Model\Entity\Role;
use App\Model\Entity\TokenEntity;
use App\Model\Entity\UserEntity;
use App\Model\Repository\UserRepositoryInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class UserRegistrationPasswordController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /** @var UserRepositoryInterface */
    private $userRepository;

    public function __construct(
        EntityManagerInterface $em,
        EventDispatcherInterface $eventDispatcher,
        UserRepositoryInterface $userRepository
    ) {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function __invoke(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (isset($data["email"], $data["name"], $data["surname"], $data["password"])) {
            if (false === filter_var($data["email"], FILTER_VALIDATE_EMAIL)) {
                throw new InvalidEmailException(
                    sprintf("Zadaný email %s není validní!", $data["email"])
                );
            }

            if (empty($data["password"])) {
                throw new InvalidArgumentException("Heslo nemůže být prázdné!");
            }

            try {
                $this->userRepository->findByEmail($data["email"]);

                throw new UserAlreadyExistsException(
                    sprintf("Uživatel %s je již registrován!", $data["email"])
                );
            } catch (EntityNotFoundException $e) {
                // User with email not exists
            }

            $password = password_hash($data["password"], PASSWORD_DEFAULT);

            if (false !== $password) {
                /** @var Role $role */
                $role = $this->em->find(Role::class, Role::TYPE_USER);

                $user = new UserEntity();
                $user->setActive(true);
                $user->setEmail($data["email"]);
                $user->setLastLogin(new DateTime());
                $user->setName($data["name"] ?? "");
                $user->setPassword($password);
                $user->setRoles([$role]);
                $user->setSurname($data["surname"] ?? "");

                // $user->setVerified(true); // TODO Temporarily
                $user->setVerificationUntil(new DateTime('+2days')); // TODO Temporarily

                $language = Language::TYPE_CS;
                if (isset($data["language"])) {
                    if (!in_array(strtolower($data["language"]), Language::getAvailableLanguages())) {
                        throw new LanguageNotExistsException();
                    }
                    $language = strtolower($data['language']);
                }
                $user->setLanguage($this->em->find(Language::class, $language));

                $token = new TokenEntity();
                $user->addToken($token);

                $this->em->persist($user);
                $this->em->flush();

                // Send activation email
                $this->eventDispatcher->dispatch(new UserRegisteredEvent($user));

                return $response->withJson([
                    "token" => $token->getId(),
                ]);
            }
        }

        return $response->withStatus(StatusCode::HTTP_BAD_REQUEST);
    }

}
