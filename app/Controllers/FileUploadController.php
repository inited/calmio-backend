<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Model\Entity\FileEntity;
use App\Services\FileService\FileServiceException;
use App\Services\FileService\FileServiceInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

/**
 * Class FileUploadController
 * @package App\Controllers
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
final class FileUploadController extends DefaultController
{

    /**
     * @var FileServiceInterface $fileService
     */
    private $fileService;

    /**
     * @var string $serverFileUrl
     */
    private $serverFileUrl;

    /**
     * FileUploadController constructor.
     *
     * @param string               $serverFileUrl
     * @param FileServiceInterface $fileService
     */
    public function __construct(
        $serverFileUrl,
        FileServiceInterface $fileService
    ) {
        $this->fileService = $fileService;
        $this->serverFileUrl = $serverFileUrl;
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return mixed
     */
    public function defaultAction(Request $request, Response $response)
    {
        $file = $request->getUploadedFiles();
        if (isset($file['file']) && $file['file'] instanceof UploadedFile) {
            try {
                /** @var FileEntity $fileEntity */
                $fileEntity = $this->fileService->uploadFile($file['file']);
                $data['url'] = $this->serverFileUrl . $fileEntity->getName();

                if ($fileEntity->isImage() && $fileEntity->hasThumbnail()) {
                    $data['thumbnailUrl'] = $this->serverFileUrl . 'thumbnails/' . $fileEntity->getName();
                }

                return $response->withJson($data);
            } catch (FileServiceException $e) {
                return $response->withJson($e->getMessage(), 400);
            }
        }

        return $response->withStatus(400);
    }

}
