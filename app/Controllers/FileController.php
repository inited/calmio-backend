<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\FileEntity;
use App\Model\Repository\FileRepositoryInterface;
use App\Services\FileService\FileServiceInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class FileUploadController
 * @package App\Controllers
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
final class FileController extends DefaultController
{

    /**
     * @var string
     */
    private $downloadUrl;

    /**
     * @var FileRepositoryInterface $fileRepository
     */
    private $fileRepository;

    /**
     * @var FileServiceInterface $fileService
     */
    private $fileService;

    /**
     * FileController constructor.
     *
     * @param FileRepositoryInterface $fileRepository
     * @param FileServiceInterface    $fileService
     * @param string                  $downloadUrl
     */
    public function __construct(
        FileRepositoryInterface $fileRepository,
        FileServiceInterface $fileService,
        string $downloadUrl
    ) {
        $this->downloadUrl = $downloadUrl;
        $this->fileRepository = $fileRepository;
        $this->fileService = $fileService;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (is_array($args) && isset($args['id'])) {
            try {
                /** @var FileEntity $fileEntity */
                $fileEntity = $this->fileRepository->find($args['id']);
                return $response->withRedirect($this->downloadUrl . $fileEntity->getName());
            } catch (EntityNotFoundException $e) {
                // Do nothing - status 404
            }
        }
        return $response->withStatus(404);
    }

}
