<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\InvalidArgumentException;
use App\Model\Entity\Feedback;
use App\Services\MailService\MailServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class FeedbackController extends DefaultController
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var string */
    private $feedbackEmail;

    /** @var MailServiceInterface */
    private $mailService;

    public function __construct(
        string $feedbackEmail,
        EntityManagerInterface $em,
        MailServiceInterface $mailService
    ) {
        $this->em = $em;
        $this->feedbackEmail = $feedbackEmail;
        $this->mailService = $mailService;
    }

    private function validateRequestData(Request $request): void
    {
        $body = $request->getParsedBody();

        if (empty($body["contactEmail"])) {
            throw new InvalidArgumentException("Contact email cannot be empty!", 400);
        }

        if (empty($body["message"])) {
            throw new InvalidArgumentException("Message cannot be empty!", 400);
        }
    }

    public function __invoke(Request $request, Response $response): Response
    {
        $this->validateRequestData($request);

        $body = $request->getParsedBody();

        $feedback = new Feedback();
        $feedback->setName($body["name"] ?? null);
        $feedback->setEmail($body["email"] ?? null);
        $feedback->setContactEmail($body["contactEmail"]);
        $feedback->setMessage(preg_replace('/[\x{10000}-\x{10FFFF}]/u', "", $body["message"]));
        $feedback->setMetadata(json_encode($body["metadata"] ?? ""));

        $this->em->persist($feedback);
        $this->em->flush();

        $this->mailService->sendTemplate($this->feedbackEmail, "Feedback", "feedback.html.twig", [
            "name" => $feedback->getName(),
            "email" => $feedback->getEmail(),
            "contactEmail" => $feedback->getContactEmail(),
            "metadata" => $feedback->getMetadata(),
            "message" => $feedback->getMessage(),
        ], $feedback->getContactEmail());

        return $response;
    }
}
