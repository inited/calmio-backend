<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\InvalidArgumentException;
use App\Model\Entity\Language;
use App\Model\Pagination\Paginator;
use App\Model\Transformer\CustomDataSerializer;
use App\Services\Validation\ValidationFactory;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class DefaultController
 * @package App\Controllers
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
abstract class DefaultController
{
    /** @var Manager */
    protected $fractal;

    /** @var ValidationFactory|null */
    protected $validationFactory;

    public function __construct()
    {
        $this->fractal = new Manager();
        $this->fractal->setSerializer(new CustomDataSerializer());
    }

    protected function respondWithErrors(Response $response, array $messages): Response
    {
        return $response->withJson([
            "error" => [
                "messages" => $messages,
            ],
        ], StatusCode::HTTP_BAD_REQUEST);
    }

    protected function respondWithNoContent(Response $response): Response
    {
        return $response->withStatus(StatusCode::HTTP_NO_CONTENT);
    }

    /**
     * @param mixed $data
     *
     * @return mixed[]
     */
    protected function transformItem($data, TransformerAbstract $transformer): array
    {
        return $this->fractal->createData(new Item($data, $transformer))->toArray();
    }

    protected function validateRequest(array $data, array $rules, array $messages = []): ?array
    {
        if (!$this->validationFactory) {
            return null;
        }

        $validator = $this->validationFactory->make($data, $rules, $messages);
        $validator->getTranslator()->setLocale(Language::TYPE_EN);

        if ($validator->fails()) {
            return $validator->errors()->getMessages();
        }

        return null;
    }

    /**
     * @param mixed $limit
     *
     * @return int
     * @throws InvalidArgumentException
     */
    protected function validateRequestLimit($limit): int
    {
        if (0 >= $limit = (int) ($limit ?? Paginator::LIMIT)) {
            throw new InvalidArgumentException('Limit is invalid!');
        }

        return $limit;
    }

    /**
     * @param mixed $offset
     *
     * @return int
     * @throws InvalidArgumentException
     */
    protected function validateRequestOffset($offset): int
    {
        if (0 > $offset = (int) ($offset ?? Paginator::OFFSET)) {
            throw new InvalidArgumentException('Offset is invalid!');
        }

        return $offset;
    }
}
