<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11. 08. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Course;
use App\Model\Repository\CourseRepositoryInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class CourseDetailController
 * @package App\Controllers
 */
final class CourseDetailController extends DefaultController
{
    /**
     * @var CourseRepositoryInterface
     */
    private $courseRepository;

    /**
     * CourseDetailController constructor.
     *
     * @param CourseRepositoryInterface $courseRepository
     */
    public function __construct(CourseRepositoryInterface $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     *
     * @return Response
     */
    public function defaultAction(Request $request, Response $response, array $args)
    {
        if (isset($args["id"])) {
            try {
                /** @var Course $course */
                $course = $this->courseRepository->find($args["id"]);

                return $response->withJson($course->__toArray());
            } catch (EntityNotFoundException $e) {
                // Do nothing
            }
        }

        return $response->withStatus(StatusCode::HTTP_NOT_FOUND);
    }

}
