<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 22. 05. 2019
 */

declare(strict_types=1);

namespace App\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InvalidArgumentException;
use App\Model\Entity\Analytics;
use App\Model\Entity\Device;
use App\Model\Entity\UserEntity;
use App\Model\Repository\AnalyticsRepositoryInterface;
use App\Model\Repository\UserRepositoryInterface;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;

final class UserPurchaseAnalyticsController extends DefaultController
{
    /** @var AnalyticsRepositoryInterface */
    private $analyticsRepository;

    /** @var EntityManagerInterface */
    private $em;

    /** @var UserRepositoryInterface */
    private $userRepository;

    public function __construct(
        AnalyticsRepositoryInterface $analyticsRepository,
        UserRepositoryInterface $userRepository,
        EntityManagerInterface $em
    ) {
        $this->analyticsRepository = $analyticsRepository;
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    private function convertUnixTimestampMillisecondsToDateTime(int $milliseconds): DateTime
    {
        $dateTime = new DateTime();
        $dateTime->setTimezone(new DateTimeZone('Europe/Prague'));
        $dateTime->setTimestamp((int) ceil($milliseconds / 1000));

        return $dateTime;
    }

    private function tryToAddPurchaseToPartnerAndPromoCode(UserEntity $user): void
    {
        if (!$promoCodeHistory = $user->getPartnerPromoCodeHistory()) {
            return;
        }

        $timeLimit = new DateTime('-3 months');
        $promoCodeHistoryActiveTo = $promoCodeHistory->getPromoCodeActiveTo();

        if ($promoCodeHistoryActiveTo->getTimestamp() < $timeLimit->getTimestamp()) {
            return;
        }

        $promoCode = $promoCodeHistory->getPromoCode();
        $promoCode->addPurchases();
    }

    /**
     * @param Request  $request
     * @param Response $response
     *
     * @return Response
     * @throws Exception
     */
    public function defaultAction(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        if (empty($data['email'])) {
            throw new InvalidArgumentException('Email nezadán!!');
        }

        if (empty($data['platform'])) {
            throw new InvalidArgumentException('Platforma nezadána!!');
        }

        if (!in_array($data['platform'], Device::AVAILABLE_PLATFORMS)) {
            throw new InvalidArgumentException('Neznámá platforma!!');
        }

        if (empty($data['timestamp'])) {
            throw new InvalidArgumentException('Timestamp nezadán!!');
        }

        try {
            $user = $this->userRepository->findByEmail($data['email']);
        } catch (EntityNotFoundException $e) {
            return $response;
        }

        $platform = strtolower($data['platform']);
        $subscriptions = (array) $data['subscriptions'] ?? [];

        $analytics = collect($this->analyticsRepository->findActiveByUserAndPlatform($user, $platform))
            ->mapWithKeys(function (Analytics $analytics) {
                $analytics->setActive(false);
                return [$analytics->getIapPurchaseType() => $analytics];
            })
            ->all();

        foreach ($subscriptions as $subscription) {
            if (empty($subscription['type'])) {
                continue;
            }

            $iapPurchaseType = strtoupper($subscription['type']);

            if (!array_key_exists($iapPurchaseType, $analytics)) {
                $entity = new Analytics();
                $entity->setIapPurchaseType($iapPurchaseType);
                $entity->setPlatform($data['platform']);
                $entity->setUser($user);

                $this->em->persist($entity);

                if ($user->hasPartnerPromoCode()) {
                    $this->tryToAddPurchaseToPartnerAndPromoCode($user);
                }
            } else {
                /** @var Analytics $entity */
                $entity = $analytics[$iapPurchaseType];
                $entity->setActive(true);
            }

            $entity->setReceipt(json_encode($subscriptions));

            if (Device::PLATFORM_ANDROID === $platform) {
                $entity->setFirstPayment($this->convertUnixTimestampMillisecondsToDateTime((int) $subscription['receipt']['purchaseTime']));
            }

            if (Device::PLATFORM_IOS === $platform) {
                $entity->setLastPayment($this->convertUnixTimestampMillisecondsToDateTime((int) $subscription['receipt']['purchase_date_ms']));
            }
        }

        $this->em->flush();

        return $response;
    }
}
