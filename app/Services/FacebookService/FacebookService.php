<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 25. 09. 2019
 */

declare(strict_types=1);

namespace App\Services\FacebookService;

use App\Model\ValueObject\FacebookSettings;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;

/**
 * Class FacebookService
 * @package App\Services\FacebookService
 */
class FacebookService implements FacebookServiceInterface
{
    /**
     * @var FacebookSettings
     */
    private $facebookSettings;

    /**
     * FacebookService constructor.
     *
     * @param FacebookSettings $facebookSettings
     */
    public function __construct(FacebookSettings $facebookSettings)
    {
        $this->facebookSettings = $facebookSettings;
    }

    /**
     * {@inheritDoc}
     */
    public function getUserInfo(string $token): FacebookUser
    {
        try {
            $fb = new Facebook([
                'app_id' => $this->facebookSettings->getAppId(),
                'app_secret' => $this->facebookSettings->getAppSecret(),
                'default_graph_version' => 'v2.4',
                'default_access_token' => $token,
            ]);

            $response = $fb->get('/me?fields=email,first_name,last_name');
            $user = $response->getGraphUser();
        } catch (FacebookSDKException $e) {
            throw new FacebookServiceException('Facebook service failed!');
        }

        if (null === $user->getEmail()) {
            throw new FacebookAuthMissingEmailException('Empty facebook user email!');
        }

        return new FacebookUser(
            $user->getId(),
            $user->getFirstName(),
            $user->getLastName(),
            $user->getEmail()
        );
    }
}
