<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 25. 09. 2019
 */

declare(strict_types=1);

namespace App\Services\FacebookService;

/**
 * Class FacebookUser
 * @package App\Services\FacebookService
 */
class FacebookUser
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * FacebookUser constructor.
     *
     * @param string $id
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     */
    public function __construct(string $id, string $firstName, string $lastName, string $email)
    {
        $this->id = $id;
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

}
