<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 10.08.2020
 */

declare(strict_types=1);

namespace App\Services\FacebookService;

use App\Exceptions\LocalizationException;

class FacebookAuthMissingEmailException extends LocalizationException
{
    protected $localizationKey = 'facebook_auth_missing_email';
}
