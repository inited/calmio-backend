<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 25. 09. 2019
 */

declare(strict_types=1);

namespace App\Services\FacebookService;

/**
 * Interface FacebookServiceInterface
 * @package App\Services\FacebookService
 */
interface FacebookServiceInterface
{
    /**
     * @param string $token
     *
     * @return FacebookUser
     * @throws FacebookAuthMissingEmailException
     * @throws FacebookServiceException
     */
    public function getUserInfo(string $token): FacebookUser;
}
