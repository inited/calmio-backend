<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 25. 09. 2019
 */

declare(strict_types=1);

namespace App\Services\FacebookService;

use Exception;

/**
 * Class FacebookServiceException
 * @package App\Services\FacebookService
 */
class FacebookServiceException extends Exception
{

}
