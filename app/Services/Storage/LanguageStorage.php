<?php

namespace App\Services\Storage;

use App\Model\Entity\Language;

class LanguageStorage implements LanguageStorageInterface
{
    /** @var Language */
    private $language;

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function setLanguage(Language $language): void
    {
        $this->language = $language;
    }
}
