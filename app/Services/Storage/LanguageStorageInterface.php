<?php

namespace App\Services\Storage;

use App\Model\Entity\Language;

interface LanguageStorageInterface
{
    public function getLanguage(): Language;

    public function setLanguage(Language $language): void;
}
