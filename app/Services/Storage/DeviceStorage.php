<?php

namespace App\Services\Storage;

use App\Model\Entity\Device;

class DeviceStorage
{
    /** @var Device|null */
    private $device;

    public function getDevice(): ?Device
    {
        return $this->device;
    }

    public function setDevice(Device $device): DeviceStorage
    {
        $this->device = $device;
        return $this;
    }
}
