<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 25. 09. 2019
 */

declare(strict_types=1);

namespace App\Services\GoogleService;

use App\Services\FacebookService\FacebookServiceException;

/**
 * Interface GoogleServiceInterface
 * @package App\Services\GoogleService
 */
interface GoogleServiceInterface
{
    /**
     * @param string $token
     *
     * @return GoogleUser
     * @throws GoogleServiceException
     */
    public function getUserInfo(string $token): GoogleUser;
}
