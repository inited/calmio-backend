<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 25. 09. 2019
 */

declare(strict_types=1);

namespace App\Services\GoogleService;

use Exception;

/**
 * Class GoogleServiceException
 * @package App\Services\GoogleService
 */
class GoogleServiceException extends Exception
{

}
