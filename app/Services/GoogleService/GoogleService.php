<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 25. 09. 2019
 */

declare(strict_types=1);

namespace App\Services\GoogleService;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Uri;

/**
 * Class GoogleService
 * @package App\Services\GoogleService
 */
class GoogleService implements GoogleServiceInterface
{
    /**
     * {@inheritDoc}
     */
    public function getUserInfo(string $token): GoogleUser
    {
        try {
            $uri = new Uri('https://www.googleapis.com/oauth2/v3/userinfo?alt=json&access_token=' . $token);
            $client = new Client();
            $data = json_decode($client->get($uri)->getBody()->getContents());

            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new GoogleServiceException('Google error: Invalid JSON!');
            }

            if(!isset ($data->family_name)){
                $data->family_name = '';
            }

            if (isset($data->sub, $data->given_name, $data->family_name, $data->email)) {
                return new GoogleUser(
                    $data->sub,
                    $data->given_name,
                    $data->family_name,
                    $data->email
                );
            }

            throw new GoogleServiceException('Google error: Missing required data!');
        } catch (ClientException $e) {
            $body = json_decode($e->getResponse()->getBody()->getContents());
            if (json_last_error() === JSON_ERROR_NONE && isset($body->error_description)) {
                throw new GoogleServiceException('Google error: ' . $body->error_description . '!');
            }

            throw new GoogleServiceException('Google service failed!');
        }
    }
}
