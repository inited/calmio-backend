<?php

namespace App\Services\Validation;

use App\Services\Translation\Translator as ValidationTranslator;
use Illuminate\Validation\Factory;
use Illuminate\Validation\Validator;

class ValidationFactory
{
    /** @var Factory $illuminateValidationFactory */
    private $illuminateValidationFactory;

    public function __construct(ValidationTranslator $translator)
    {
        $this->illuminateValidationFactory = new Factory($translator);
    }

    /**
     * @param array<string|string> $data
     * @param array<string|string> $rules
     * @param array<string|string> $messages
     */
    public function make(array $data, array $rules, array $messages = []): Validator
    {
        return $this->illuminateValidationFactory->make($data, $rules, $messages);
    }
}
