<?php

namespace App\Services\Statistics;

use App\Model\Entity\Tag;
use App\Model\Repository\CourseRepositoryInterface;
use App\Model\Repository\UserRepositoryInterface;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\XLSX\Writer;

class StatisticsExport
{
    /** @var string[] */
    private $experienceMapper = [
        Tag::NO_EXPERIENCE => 'Nemám',
        Tag::MILD_EXPERIENCE => 'Jen trochu',
        Tag::LOTS_OF_EXPERIENCE => 'Spoustu',
    ];

    /** @var CourseRepositoryInterface */
    private $courseRepository;

    /** @var UserRepositoryInterface */
    private $userRepository;

    /** @var UserStatistics */
    private $userStatistics;

    public function __construct(
        CourseRepositoryInterface $courseRepository,
        UserRepositoryInterface $userRepository,
        UserStatistics $userStatistics
    ) {
        $this->courseRepository = $courseRepository;
        $this->userRepository = $userRepository;
        $this->userStatistics = $userStatistics;
    }

    private function generateCoursesSheet(Writer $writer): void
    {
        
        $invalidCharacters = ['\\', '/', '?', '*', ':', '[', ']'];

        $courses = $this->courseRepository->findAll();
        foreach ($courses as $course) {
            $courseUserData = $this->courseRepository->getCourseUserStatistics($course);

            $writer->addNewSheetAndMakeItCurrent();
            $sheetName = str_replace($invalidCharacters, '', mb_substr($course->getName(), 0, 31, 'UTF-8'));
            $writer->getCurrentSheet()->setName($sheetName);

            $row1 = ['Uživatel'];
            $contentRows = [];

            foreach ($course->getLessons() as $lessonKey => $lesson) {
                array_push($row1, $lesson->getOrder());
                $i = 0;
                foreach ($courseUserData as $email => $courseUser) {
                    $row = [];
                    if (0 === $lessonKey) {
                        $contentRows[$i] = [$email];
                    }
                    $contentRows[$i][$lessonKey + 1] = "";
                    if (array_key_exists($lesson->getOrder(), $courseUser)) {
                        $contentRows[$i][$lessonKey + 1] = gmdate('H:i:s', $courseUser[$lesson->getOrder()]);
                    }
                    $i++;
                }
            }

            $writer->addRow(WriterEntityFactory::createRowFromArray($row1));
            foreach ($contentRows as $row) {
                $writer->addRow(WriterEntityFactory::createRowFromArray($row));
            }

            $this->courseRepository->clear();
        }
    }

    private function generateQuestionnaireSheet(Writer $writer): void
    {
        $writer->addNewSheetAndMakeItCurrent();

        $writer->getCurrentSheet()->setName('Dotazník');

        $row1 = ['Uživatel', 'Zkušenost', 'Kontakt s tělem', 'Soustředění a produktivita' , 'Duševní zdraví', 'Osobní rozvoj', 'Stres a úzkosti', 'Lepší spánek'];
        
        $writer->addRow(WriterEntityFactory::createRowFromArray($row1));

        foreach ($this->userRepository->getQuestionnaireData() as $k => $user) {
            $experience = $user['experience'] ? $this->experienceMapper[$user['experience']] : '';
            $k = (int) $k;

            $row = [
                $user['email'],
                $experience,
                $user['contactWithBody'],
                $user['focusAndProductivity'],
                $user['mentalHealth'],
                $user['personalGrowth'],
                $user['stressAndAnxiety'],
                $user['betterSleep'],
            ];
            $writer->addRow(WriterEntityFactory::createRowFromArray($row));
            $this->userRepository->clear();
        }
    }

    private function generateUsersSheet(Writer $writer): void
    {
        $writer->getCurrentSheet()->setName('Uživatelé');

        $row1 = ['', '', '', '', 'Kurzy', 'Lekce'];
        $row2 = ['Testovací uživatel', 'Uživatel', 'Datum registrace', 'Poslední aktivita', 'Dokončené' , 'Dokončené', 'Předčasně uk.', 'Promeditováno'];
        
        $writer->addRow(WriterEntityFactory::createRowFromArray($row1));
        $writer->addRow(WriterEntityFactory::createRowFromArray($row2));
        
        $limit = 500;
        $totalGeneratedCount = 0;
        $totalUsersCount = $this->userRepository->count();
        
        while ($totalGeneratedCount < $totalUsersCount) {
            $users = $this->userRepository->findAllByPaginated($limit, $totalGeneratedCount);
            
            foreach ($users as $user) {
                $user = $this->userStatistics->getStatistics($user);
                
                $row = [
                    $user['test'] ? 'X' : '',
                    $user['email'],
                    $user['createdAt'] ? $user['createdAt']->format('d.m.Y H:i:s') : '',
                    $user['lastActivity'] ? $user['lastActivity']->format('d.m.Y H:i:s') : '',
                    $user['coursesFinished'] . ' / ' . $user['coursesSignedIn'],
                    $user['lessonsFinished'] . ' / ' . $user['lessonsSignedIn'],
                    $user['lessonsUnfinished'],
                    gmdate('H:i:s', $user['totalMeditationTime']),
                ];
                $writer->addRow(WriterEntityFactory::createRowFromArray($row));
            }

            $totalGeneratedCount += $limit;
            $this->userRepository->clear();
        }
    }

    public function generateXlsx(Writer $writer): void
    {
        $this->generateUsersSheet($writer);
        $this->generateCoursesSheet($writer);
        $this->generateQuestionnaireSheet($writer);
    }
}
