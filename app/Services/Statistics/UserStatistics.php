<?php

namespace App\Services\Statistics;

use App\Model\Entity\CourseUserLesson;
use App\Model\Entity\UserEntity;

class UserStatistics
{
    public function getStatistics(UserEntity $userEntity): array
    {
        $statistics = [
            'id' => $userEntity->getId(),
            'email' => $userEntity->getEmail(),
            'test' => $userEntity->isTestUser(),
            'createdAt' => $userEntity->getCreated(),
            'lastActivity' => $userEntity->getLastActivity(),
            'coursesSignedIn' => count($userEntity->getEnrolledCourses()),
            'coursesFinished' => 0,
            'lessonsSignedIn' => 0,
            'lessonsFinished' => 0,
            'lessonsUnfinished' => 0,
            'totalMeditationTime' => 0,
            'courses' => [],
        ];

        foreach ($userEntity->getEnrolledCourses() as $signedInCourse) {
            $courseLessonsCount = $signedInCourse->getCourse()->getLessonsCount();
            $lessonsFinishedCount = $signedInCourse->getLessonsFinishedCount();
            $statistics['coursesFinished'] += $courseLessonsCount === $lessonsFinishedCount ? 1 : 0;
            $statistics['lessonsSignedIn'] += $signedInCourse->getCourse()->getLessonsCount();
            $statistics['lessonsFinished'] += $lessonsFinishedCount;
            $statistics['lessonsUnfinished'] += $signedInCourse->getLessonsCount() - $lessonsFinishedCount;
            $statistics['totalMeditationTime'] += $signedInCourse->getLessonsSpendTotalTime();

            $statistics['courses'][] = [
                'id' => $signedInCourse->getCourse()->getId(),
                'name' => $signedInCourse->getCourse()->getName(),
                'lessons' => collect($signedInCourse->getLessons())
                    ->transform(function (CourseUserLesson $courseUserLesson) {
                        return [
                            'id' => $courseUserLesson->getLesson()->getId(),
                            'order' => $courseUserLesson->getLesson()->getOrder(),
                            'completed' => $courseUserLesson->isCompleted(),
                            'lessonTime' => $courseUserLesson->getTime(),
                            'lessonDurationSeconds' => $courseUserLesson->getLessonDurationSeconds(),
                        ];
                    })
                    ->all(),
            ];
        }

        return $statistics;
    }
}