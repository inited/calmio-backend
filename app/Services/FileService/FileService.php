<?php
namespace App\Services\FileService;

use App\Model\Entity\FileEntity;
use App\Model\Repository\FileRepositoryInterface;
use Exception;
use Gumlet\ImageResize;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Slim\Http\UploadedFile;
use Tracy\Debugger;

class FileService implements FileServiceInterface
{
    /** @var FileRepositoryInterface */
    private $fileRepository;

    /** @var string $uploadDirectory */
    private $uploadDirectory;

    public function __construct(
        string $uploadDirectory,
        FileRepositoryInterface $fileRepository
    ) {
        $this->fileRepository = $fileRepository;
        $this->uploadDirectory = $uploadDirectory;
    }

    public function downloadFile(FileEntity $fileEntity)
    {
        if (!is_file($this->uploadDirectory . $fileEntity->getName())) {
            throw new FileNotFoundException();
        }

        header('Content-Description: File Transfer');
        header('Content-Type: ' . $fileEntity->getType());
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . $fileEntity->getSize());
        readfile($this->uploadDirectory . $fileEntity->getName());
        exit;
    }

    /**
     * @throws FileServiceException
     */
    public function uploadFile(UploadedFile $file): FileEntity
    {
        if ($file->getError() !== UPLOAD_ERR_OK) {
            throw new FileServiceException('Upload file failed!');
        }

        $name = pathinfo($file->getClientFilename(), PATHINFO_FILENAME);
        $extension = strtolower(pathinfo($file->getClientFilename(), PATHINFO_EXTENSION));
        $newFileName = Str::slug($name) . '-' . Uuid::uuid4()->toString() . '.' . $extension;

        try {
            $fileFinalPath = $this->uploadDirectory . $newFileName;
            $file->moveTo($this->uploadDirectory . $newFileName);

            $fileEntity = new FileEntity(
                $newFileName,
                $file->getClientFilename(),
                $file->getSize(),
                $file->getClientMediaType()
            );

            if ($fileEntity->isImage()) {
                try {
                    $image = new ImageResize($fileFinalPath);
                    $image->resizeToLongSide(200);
                    $image->save($this->uploadDirectory . 'thumbnails/' . $newFileName);

                    $fileEntity->setHasThumbnail(true);
                } catch (Exception $e) {
                    Debugger::log($e, Debugger::EXCEPTION);
                }
            }

            return $this->fileRepository->saveEntity($fileEntity);
        } catch (Exception $e) {
            Debugger::log($e, Debugger::EXCEPTION);
            throw new FileServiceException($e->getMessage());
        }

        throw new FileServiceException('Upload file failed!');
    }
}
