<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Services\AuthorizationService;

use App\Model\Entity\Role;
use App\Services\TokenStorage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class AuthorizationService
 * @package App\Services\AuthorizationService
 */
class AuthorizationService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * AuthorizationService constructor.
     *
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface  $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param mixed    $next
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $next)
    {
        if (null !== $token = $this->tokenStorage->getToken()) {
            /** @var Role $role */
            $role = $this->em->find(Role::class, Role::TYPE_ADMIN);
            if ($token->getUser()->hasRole($role)) {
                return $next($request, $response);
            }
        }

        return $response->withStatus(403);
    }
}
