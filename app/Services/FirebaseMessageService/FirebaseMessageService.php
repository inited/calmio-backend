<?php

namespace App\Services\FirebaseMessageService;

use Exception;
use sngrl\PhpFirebaseCloudMessaging\Client as FirebaseClient;
use sngrl\PhpFirebaseCloudMessaging\Message;
use sngrl\PhpFirebaseCloudMessaging\Notification;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Device;
use sngrl\PhpFirebaseCloudMessaging\Recipient\Topic;
use Tracy\Debugger;
use Tracy\ILogger;

class FirebaseMessageService
{
    /** @var FirebaseClient */
    private $firebaseClient;

    public function __construct(FirebaseClient $firebaseClient)
    {
        $this->firebaseClient = $firebaseClient;
    }

    private function parseInvalidTokens(string $response, array $tokens): array
    {
        $invalidTokens = [];
        if (is_string($response)) {
            $response = json_decode($response, true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                return $invalidTokens; // Invalid response data
            }

            if (isset($response["failure"], $response["results"]) && $response["failure"] && is_array($response["results"])) {
                foreach ($response["results"] as $key => $result) {
                    if ("error" === array_key_first($result) && isset($tokens[$key])) {
                        $invalidTokens[] = $tokens[$key];
                    }
                }
            }
        }

        return $invalidTokens;
    }

    private function sendMessage(Message $message): string
    {
        try {
            $response = $this->firebaseClient->send($message);
            return $response->getBody()->getContents();
        } catch (Exception $e) {
            Debugger::log($e, ILogger::EXCEPTION);
            return "";
        }
    }

    public function sendNotificationToDevices(Notification $notification, array $tokens): array
    {
        $message = new Message();
        $message->setPriority("high");
        $message->setNotification($notification);

        foreach ($tokens as $token) {
            $message->addRecipient(new Device($token));
        }

        $response = $this->sendMessage($message);
        return $this->parseInvalidTokens($response, $tokens);
    }

    public function sendNotificationToTopic(Notification $notification, string $topic): void
    {
        $message = new Message();
        $message->setPriority("high");
        $message->addRecipient(new Topic($topic));
        $message->setNotification($notification);

        $this->sendMessage($message);
    }

}