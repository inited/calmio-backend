<?php

namespace App\Services\Translation;

use Symfony\Component\Translation\Translator as SymfonyTranslator;

class Translator implements \Illuminate\Contracts\Translation\Translator
{
    /** @var SymfonyTranslator */
    private $translator;

    public function __construct(SymfonyTranslator $translator)
    {
        $this->translator = $translator;
    }

    public function choice($key, $number, array $replace = [], $locale = null)
    {
        return $key;
    }

    public function get($key, array $replace = [], $locale = null)
    {
        return $this->translator->trans($key, $replace, null, $locale);
    }

    public function getLocale(): string
    {
        return $this->translator->getLocale();
    }

    public function setLocale($locale)
    {
        $this->translator->setLocale($locale);
    }
}
