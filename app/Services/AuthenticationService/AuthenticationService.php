<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\AuthenticationService;

use App\Exceptions\InvalidAuthTokenException;
use App\Model\Entity\UserEntity;
use App\Model\Factory\UserDeviceFactory;
use App\Model\Repository\TokenRepositoryInterface;
use App\Services\Storage\DeviceStorage;
use App\Services\TokenStorage\TokenStorageInterface;
use Closure;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Route;

/**
 * Class AuthenticationService
 * @package App\Services\AuthenticationService
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
class AuthenticationService
{
    /** @var DeviceStorage */
    private $deviceStorage;

    /** @var EntityManagerInterface */
    private $em;

    /** @var TokenRepositoryInterface */
    private $tokenRepository;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var UserDeviceFactory */
    private $userDeviceFactory;

    public function __construct(
        DeviceStorage $deviceStorage,
        EntityManagerInterface $em,
        TokenRepositoryInterface $tokenRepository,
        TokenStorageInterface $tokenStorage,
        UserDeviceFactory $userDeviceFactory
    ) {
        $this->deviceStorage = $deviceStorage;
        $this->em = $em;
        $this->tokenRepository = $tokenRepository;
        $this->tokenStorage = $tokenStorage;
        $this->userDeviceFactory = $userDeviceFactory;
    }

    private function getBearerToken(Request $request): ?string
    {
        $pattern = '/Bearer\s(\S+)/';

        if ($headers = $request->getHeaderLine('Authorization')) {
            if (preg_match($pattern, $headers, $matches)) {
                return $matches[1];
            }
        }

        if ($headers = $request->getHeaderLine('authorization')) {
            if (preg_match($pattern, $headers, $matches)) {
                return $matches[1];
            }
        }

        if ($headers = $request->getHeaderLine('x-authorization')) {
            if (preg_match($pattern, $headers, $matches)) {
                return $matches[1];
            }
        }

        return null;
    }

    private function processUserDevice(UserEntity $user): void
    {
        if (!$device = $this->deviceStorage->getDevice()) {
            return;
        }

        if (!$user->hasDevice($device)) {
            $userDevice = $this->userDeviceFactory->create($device, $user);
            $user->addDevice($userDevice);
            $this->em->getUnitOfWork()->commit($user);
        }
    }

    /**
     * @param Route|Closure $next
     *
     * @return ResponseInterface
     * @throws Exception
     */
    public function __invoke(Request $request, Response $response, $next): ResponseInterface
    {
        if (!$token = $this->getBearerToken($request)) {
            return $response->withStatus(401);
        }

        try {
            $tokenEntity = $this->tokenRepository->verifyToken($token);
        } catch (InvalidAuthTokenException $e) {
            return $response->withStatus(401);
        }

        $this->tokenStorage->setToken($tokenEntity);

        $this->processUserDevice($tokenEntity->getUser());

        return $next($request, $response);
    }

}
