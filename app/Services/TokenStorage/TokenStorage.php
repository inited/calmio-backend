<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\TokenStorage;

use App\Model\Entity\TokenEntity;

/**
 * Class LanguageStorage
 * @package App\Services\LanguageStorage
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
class TokenStorage implements TokenStorageInterface
{

    /**
     * @var null|TokenEntity $token
     */
    private $token;

    /**
     * @return TokenEntity|null
     */
    public function getToken(): ?TokenEntity
    {
        return $this->token;
    }

    /**
     * @param TokenEntity $token
     *
     * @return TokenStorage
     */
    public function setToken(TokenEntity $token): TokenStorage
    {
        $this->token = $token;
        return $this;
    }

}
