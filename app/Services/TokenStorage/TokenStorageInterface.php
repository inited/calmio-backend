<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\TokenStorage;

use App\Model\Entity\TokenEntity;

/**
 * Interface LanguageStorageInterface
 * @package App\Services\LanguageStorage
 */
interface TokenStorageInterface
{

    /**
     * @param TokenEntity $token
     *
     * @return mixed
     */
    public function setToken(TokenEntity $token);

    /**
     * @return TokenEntity|null
     */
    public function getToken();
}
