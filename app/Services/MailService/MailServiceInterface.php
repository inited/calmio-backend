<?php

declare(strict_types=1);

namespace App\Services\MailService;

interface MailServiceInterface
{
    public function sendTemplate(
        string $address,
        string $subject,
        string $template,
        array $data = [],
        string $replyTo = null
    ): bool;
}
