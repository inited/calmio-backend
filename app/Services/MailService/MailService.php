<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Services\MailService;

use Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Slim\Views\Twig;
use Tracy\Debugger;
use Tracy\ILogger;

class MailService extends PHPMailer implements MailServiceInterface
{
    /** @var Twig */
    private $view;

    public function __construct(
        array $settings,
        Twig $view
    ) {
        parent::__construct();
        $this->view = $view;

        if (!empty($settings['smtp'])) {
            $this->isSMTP();
            $this->CharSet = self::CHARSET_UTF8;
            $this->Host = $settings['host'];
            $this->Port = $settings['port'];

            if (isset($settings['username'], $settings['password'])) {
                $this->SMTPAuth = true;
                $this->Username = $settings['username'];
                $this->Password = $settings['password'];
            }
            if (isset($settings['secure'])) {
                $this->SMTPSecure = $settings['secure'];
            }
        }

        if (isset($settings['from'])) {
            $this->From = $settings['from'];
        }

        if (isset($settings['fromName'])) {
            $this->FromName = $settings['fromName'];
        }
    }

    /**
     * @param mixed[] $data
     */
    private function setBodyFromTemplate(string $template, array $data): string
    {
        $this->isHTML();
        return $this->view->fetch($template, $data);
    }

    /**
     * @param mixed[] $data
     */
    public function sendTemplate(
        string $address,
        string $subject,
        string $template,
        array $data = [],
        string $replyTo = null
    ): bool {
        if (!Debugger::$productionMode) {
            return true;
        }

        try {
            $this->addAddress($address);
            $this->Subject = $subject;
            $this->Body = $this->setBodyFromTemplate($template, $data);

            if ($replyTo) {
                $this->addReplyTo($replyTo);
            }

            return $this->send();
        } catch (Exception $e) {
            Debugger::log($e, ILogger::EXCEPTION);
            return false;
        }
    }

}
