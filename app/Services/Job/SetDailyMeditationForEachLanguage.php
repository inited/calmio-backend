<?php

namespace App\Services\Job;

use App\Model\Entity\CourseLanguage;
use App\Model\Repository\CourseRepositoryInterface;
use App\Model\Repository\LanguageRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class SetDailyMeditationForEachLanguage
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var CourseRepositoryInterface */
    private $courseRepository;

    /** @var LanguageRepositoryInterface */
    private $languageRepository;

    public function __construct(
        EntityManagerInterface $em,
        CourseRepositoryInterface $courseRepository,
        LanguageRepositoryInterface $languageRepository
    ) {
        $this->em = $em;
        $this->courseRepository = $courseRepository;
        $this->languageRepository = $languageRepository;
    }

    public function run(): void
    {
        $languages = $this->languageRepository->findAll();

        $preferredMeditation = $this->courseRepository->findTomorrowPreferredDailyMeditation();

        foreach ($languages as $language) {
            $courseLanguageEntities = $this->courseRepository->getCourseLanguageEntitiesByLanguageForDailyMeditations(
                $language
            );

            $courseLanguageEntities = collect($courseLanguageEntities)
                ->mapWithKeys(function (CourseLanguage $courseLanguage) {
                    return [$courseLanguage->getId() => $courseLanguage];
                });

            if (!$courseLanguageEntities->count()) {
                continue;
            }

            if ($preferredMeditation && ($courseLanguage = $preferredMeditation->getCourseLanguageByLanguage($language))) {
                $courseLanguageEntities->each(function (CourseLanguage $courseLanguage) {
                    $courseLanguage->setDailyMeditation(false);
                });

                $courseLanguage->setDailyMeditation(true);
                continue;
            }

            /** @var CourseLanguage|null $actualCourseLanguage */
            $actualCourseLanguage = $courseLanguageEntities->filter(function (CourseLanguage $courseLanguage) {
                return $courseLanguage->isDailyMeditation();
            })->first();

            if (!$actualCourseLanguage) {
                /** @var CourseLanguage $courseLanguage */
                $courseLanguage = $courseLanguageEntities->first();
                $courseLanguage->setDailyMeditation(true);
                continue;
            }

            $actualCourseLanguageKey = $courseLanguageEntities->keys()->search($actualCourseLanguage->getId());
            $courseLanguageEntities->each(function (CourseLanguage $courseLanguage) {
                $courseLanguage->setDailyMeditation(false);
            });

            if (false === $actualCourseLanguageKey
                || $actualCourseLanguageKey === ($courseLanguageEntities->keys()->count() - 1)) {
                /** @var CourseLanguage $courseLanguage */
                $courseLanguage = $courseLanguageEntities->first();
                $courseLanguage->setDailyMeditation(true);
                continue;
            }

            $newCourseLanguageKey = $actualCourseLanguageKey + 1;
            /** @var CourseLanguage $newCourseLanguage */
            $newCourseLanguage = $courseLanguageEntities->get(
                $courseLanguageEntities->keys()->get($newCourseLanguageKey)
            );
            $newCourseLanguage->setDailyMeditation(true);
        }

        $this->em->flush();

        $this->courseRepository->invalidateTomorrowDailyMeditation();
    }
}
