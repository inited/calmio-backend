<?php

namespace App\Services\Job;

use App\Model\Repository\PromoCodeRepositoryInterface;
use Tracy\Debugger;
use Tracy\ILogger;

class CheckAvailableGiftVoucherCodes
{
    /** @var PromoCodeRepositoryInterface */
    private $promoCodeRepository;

    public function __construct(PromoCodeRepositoryInterface $promoCodeRepository)
    {
        $this->promoCodeRepository = $promoCodeRepository;
    }

    public function run(): void
    {
        $values = $this->promoCodeRepository->getAvailableGiftPromoCodeValues();

        foreach ($values as $value) {
            $available = $this->promoCodeRepository->getNumberOfAvailableGiftPromoCodeByValue($value);

            if ($available < 50) {
                Debugger::log(
                    sprintf("Only %s gift codes with a value of %s are available!", $available, $value),
                    ILogger::EXCEPTION
                );
            }
        }
    }
}
