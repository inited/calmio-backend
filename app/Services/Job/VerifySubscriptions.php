<?php

namespace App\Services\Job;

use App\Model\Entity\Subscription;
use App\Model\Repository\SubscriptionRepository;
use App\Services\PurchaseSubscriptionValidation\PurchaseValidationService;

class VerifySubscriptions
{
    /** @var PurchaseValidationService */
    private $purchaseValidationService;

    /** @var SubscriptionRepository */
    private $subscriptionRepository;

    public function __construct(
        PurchaseValidationService $purchaseValidationService,
        SubscriptionRepository $subscriptionRepository
    ) {
        $this->purchaseValidationService = $purchaseValidationService;
        $this->subscriptionRepository = $subscriptionRepository;
    }

    public function run(): void
    {
        collect($this->subscriptionRepository->getAllUsersSubscriptionsToVerify())
            ->each(function (Subscription $subscription) {
                $this->purchaseValidationService->validateSubscriptions(
                    $subscription->getPlatform(),
                    [
                        [
                            'receipt' => $subscription->getToken(),
                            'productId' => $subscription->getProductId(),
                        ],
                    ]
                );
            });
    }
}
