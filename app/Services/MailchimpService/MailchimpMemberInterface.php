<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 01.04.2020
 */

declare(strict_types=1);

namespace App\Services\MailchimpService;

use DateTime;

/**
 * Interface MailchimpMemberInterface
 * @package App\Services\MailchimpService
 */
interface MailchimpMemberInterface
{

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return DateTime
     */
    public function getLastActivity(): DateTime;

    /**
     * @return string|null
     */
    public function getMailchimpId(): ?string;

    /**
     * @return string[]
     */
    public function getMailchimpTags(): array;

    /**
     * @return DateTime
     */
    public function getRegistrationDate(): DateTime;

}