<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 01.04.2020
 */

declare(strict_types=1);

namespace App\Services\MailchimpService;

use App\Model\Entity\Tag;
use DrewM\MailChimp\MailChimp;
use Exception;

/**
 * Class MailchimpService
 * @package App\Services\MailchimpService
 */
class MailchimpService implements MailchimpServiceInterface
{
    /** @var MailChimp */
    private $mailchimp;

    /** @var mixed[] */
    private $mailchimpSettings;

    /** @var string[] */
    private $tags = [
        Tag::BETTER_SLEEP => 'Better sleep',
        Tag::CONTACT_WITH_BODY => 'Contact with body',
        Tag::FOCUS_AND_PRODUCTIVITY => 'Focus and productivity',
        Tag::MENTAL_HEALTH => 'Mental health',
        Tag::PERSONAL_GROWTH => 'Personal development',
        Tag::STRESS_AND_ANXIETY => 'Stress and anxiety',
    ];

    /**
     * MailchimpService constructor.
     *
     * @param mixed[] $mailchimpSettings
     *
     * @throws Exception
     */
    public function __construct(array $mailchimpSettings)
    {
        // Volani Mailchimpu vypnuto, nepouziva se

        // $this->mailchimp = new MailChimp($mailchimpSettings['apiKey']);
        // $this->mailchimpSettings = $mailchimpSettings;
    }

    /**
     * @param MailchimpMemberInterface $member
     *
     * @return string[]
     */
    private function mapMailchimpTags(MailchimpMemberInterface $member): array
    {
        $tags = [];

        foreach ($member->getMailchimpTags() as $tag) {
            if (array_key_exists($tag, $this->tags)) {
                $tags[] = $this->tags[$tag];
            }
        }

        return $tags;
    }

    /**
     * {@inheritDoc}
     */
    public function createMember(MailchimpMemberInterface $member): string
    {
        // Volani Mailchimpu vypnuto, nepouziva se
        return "";

        /* $result = $this->mailchimp->post('lists/' . $this->mailchimpSettings['listId'] . '/members', [
            'email_address' => $member->getEmail(),
            'status' => 'subscribed',
            'tags' => $this->mapMailchimpTags($member),
            'merge_fields' => [
                $this->mailchimpSettings['mergeFields']['LAST_ACTIVITY'] => $member->getLastActivity()->format('Y-m-d'),
                $this->mailchimpSettings['mergeFields']['REG_DATE'] => $member->getRegistrationDate()->format('Y-m-d'),
            ],
        ]);

        if ($this->mailchimp->success()) {
            return $result['id'];
        }

        throw new Exception($this->mailchimp->getLastError()); */
    }

    /**
     * {@inheritDoc}
     */
    public function updateMember(MailchimpMemberInterface $member): void
    {

        // Volani Mailchimpu vypnuto, nepouziva se
        return;

        /* $subscriberHash = MailChimp::subscriberHash($member->getEmail());

        $this->mailchimp->put('lists/' . $this->mailchimpSettings['listId'] . '/members/' . $subscriberHash, [
            'email_address' => $member->getEmail(),
            'merge_fields' => [
                $this->mailchimpSettings['mergeFields']['LAST_ACTIVITY'] => $member->getLastActivity()->format('Y-m-d'),
                $this->mailchimpSettings['mergeFields']['REG_DATE'] => $member->getRegistrationDate()->format('Y-m-d'),
            ],
        ]);

        if (!$this->mailchimp->success()) {
            throw new Exception($this->mailchimp->getLastError());
        } */
    }

}