<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 01.04.2020
 */

declare(strict_types=1);

namespace App\Services\MailchimpService;

use Exception;

/**
 * Interface MailchimpServiceInterface
 * @package App\Services\MailchimpService
 */
interface MailchimpServiceInterface
{
    /**
     * @param MailchimpMemberInterface $member
     *
     * @return string
     * @throws Exception
     */
    public function createMember(MailchimpMemberInterface $member): string;

    /**
     * @param MailchimpMemberInterface $member
     *
     * @throws Exception
     */
    public function updateMember(MailchimpMemberInterface $member): void;
}