<?php
namespace App\Services\PurchaseSubscriptionValidation;

use App\Model\Entity\Subscription;
use DateTime;

class PurchaseSubscriptionData
{
    const SUBSCRIPTION_TYPE_1 = 'subscription1';
    const SUBSCRIPTION_TYPE_2 = 'subscription_monthly';
    const SUBSCRIPTION_TYPE_3 = 'subscription_annual';
    const SUBSCRIPTION_TYPE_4 = 'subscription_monthly_11_2021';
    const SUBSCRIPTION_TYPE_LIFETIME = 'lifetime_full_version';

    /** @var bool */
    private $autoRenewing = false;

    /** @var string|null */
    private $cancelReason;

    /** @var string|null */
    private $environment;

    /** @var string|null */
    private $error;

    /** @var string|null */
    private $orderId;

    /** @var string|null */
    private $originalTransactionId;

    /** @var integer|null */
    private $paymentState;

    /** @var string */
    private $platform;

    /** @var int */
    private $priceAmountMicros = 0;

    /** @var string|null */
    private $priceCurrencyCode;

    /** @var string */
    private $productId;

    /** @var DateTime */
    private $purchasedAt;

    /** @var string|null */
    private $purchaseType;

    /** @var string */
    private $receipt;

    /** @var integer|null */
    private $renewals;

    /** @var string|null */
    private $transactionId;

    /** @var bool */
    private $upgraded = false;

    /** @var DateTime */
    private $validTo;

    public function __construct(string $platform)
    {
        $this->platform = $platform;
    }

    public function getCancelReason(): ?string
    {
        return $this->cancelReason;
    }

    public function setCancelReason(?string $cancelReason): PurchaseSubscriptionData
    {
        $this->cancelReason = $cancelReason;
        return $this;
    }

    public function getEnvironment(): ?string
    {
        return $this->environment;
    }

    public function setEnvironment(?string $environment): PurchaseSubscriptionData
    {
        $this->environment = $environment;
        return $this;
    }

    public function getError(): ?string
    {
        return $this->error;
    }

    public function setError(?string $error): PurchaseSubscriptionData
    {
        $this->error = $error;
        return $this;
    }

    public function getHash(): string
    {
        return md5($this->receipt);
    }

    public function getOrderId(): ?string
    {
        return $this->orderId;
    }

    public function setOrderId(?string $orderId): PurchaseSubscriptionData
    {
        $this->orderId = $orderId;
        return $this;
    }

    public function getOriginalTransactionId(): ?string
    {
        return $this->originalTransactionId;
    }

    public function setOriginalTransactionId(?string $originalTransactionId): PurchaseSubscriptionData
    {
        $this->originalTransactionId = $originalTransactionId;
        return $this;
    }

    public function getPaymentState(): ?int
    {
        return $this->paymentState;
    }

    public function setPaymentState(?int $paymentState): PurchaseSubscriptionData
    {
        $this->paymentState = $paymentState;
        return $this;
    }

    public function getPlatform(): string
    {
        return $this->platform;
    }

    public function getPriceAmountMicros(): int
    {
        return $this->priceAmountMicros;
    }

    public function setPriceAmountMicros(int $priceAmountMicros): PurchaseSubscriptionData
    {
        $this->priceAmountMicros = $priceAmountMicros;
        return $this;
    }

    public function getPriceCurrencyCode(): ?string
    {
        return $this->priceCurrencyCode;
    }

    public function setPriceCurrencyCode(?string $priceCurrencyCode): PurchaseSubscriptionData
    {
        $this->priceCurrencyCode = $priceCurrencyCode;
        return $this;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function setProductId(string $productId): PurchaseSubscriptionData
    {
        $this->productId = $productId;
        return $this;
    }

    public function getPurchaseType(): ?string
    {
        return $this->purchaseType;
    }

    public function setPurchaseType(?string $purchaseType): PurchaseSubscriptionData
    {
        $this->purchaseType = $purchaseType;
        return $this;
    }

    public function getPurchasedAt(): DateTime
    {
        return $this->purchasedAt;
    }

    public function setPurchasedAt(DateTime $purchasedAt): PurchaseSubscriptionData
    {
        $this->purchasedAt = $purchasedAt;
        return $this;
    }

    public function getReceipt(): string
    {
        return $this->receipt;
    }

    public function setReceipt(string $receipt): PurchaseSubscriptionData
    {
        $this->receipt = $receipt;
        return $this;
    }

    public function getRenewals(): ?int
    {
        return $this->renewals;
    }

    public function setRenewals(?int $renewals): PurchaseSubscriptionData
    {
        $this->renewals = $renewals;
        return $this;
    }

    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    public function setTransactionId(?string $transactionId): PurchaseSubscriptionData
    {
        $this->transactionId = $transactionId;
        return $this;
    }

    public function getType(): string
    {
        switch ($this->productId) {
            case self::SUBSCRIPTION_TYPE_1:
            case self::SUBSCRIPTION_TYPE_2:
            case self::SUBSCRIPTION_TYPE_4:
                return Subscription::TYPE_MONTHLY;
            case self::SUBSCRIPTION_TYPE_3:
                return Subscription::TYPE_ANNUAL;
            case self::SUBSCRIPTION_TYPE_LIFETIME:
                return Subscription::TYPE_LIFETIME;
            default:
                return '';
        }
    }

    public function getValidTo(): DateTime
    {
        return $this->validTo;
    }

    public function setValidTo(DateTime $validTo): PurchaseSubscriptionData
    {
        $this->validTo = $validTo;
        return $this;
    }

    public function hasError(): bool
    {
        return !is_null($this->error);
    }

    public function isAutoRenewing(): bool
    {
        return $this->autoRenewing;
    }

    public function setAutoRenewing(bool $autoRenewing): PurchaseSubscriptionData
    {
        $this->autoRenewing = $autoRenewing;
        return $this;
    }

    public function isUpgraded(): bool
    {
        return $this->upgraded;
    }

    public function setUpgraded(bool $upgraded): PurchaseSubscriptionData
    {
        $this->upgraded = $upgraded;
        return $this;
    }
}
