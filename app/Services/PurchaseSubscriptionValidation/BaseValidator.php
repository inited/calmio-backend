<?php

namespace App\Services\PurchaseSubscriptionValidation;

use DateTime;
use DateTimeZone;

abstract class BaseValidator
{
    protected function convertUnixTimestampToDatetime(int $timestamp): DateTime
    {
        $dateTime = new DateTime();
        $dateTime->setTimezone(new DateTimeZone('UTC'));
        $dateTime->setTimestamp((int) ceil($timestamp / 1000));

        return $dateTime;
    }
}
