<?php

namespace App\Services\PurchaseSubscriptionValidation;

use App\Model\Entity\Analytics;
use App\Model\Entity\Device;
use Exception;
use ReceiptValidator\iTunes\PurchaseItem;
use ReceiptValidator\iTunes\Validator as iTunesValidator;

class IosValidator extends BaseValidator
{
    private const SHARED_SECRET = 'e30a1c3aaa4246eabfd2fd92db0a1237';

    /** @var iTunesValidator */
    private $validator;

    public function __construct()
    {
        $this->validator = (new iTunesValidator(iTunesValidator::ENDPOINT_PRODUCTION))
            ->setSharedSecret(self::SHARED_SECRET);
    }

    private function processPurchaseItem(PurchaseItem $purchaseItem): PurchaseSubscriptionData
    {
        $purchaseTimestamp = $purchaseItem->getPurchaseDate()->getTimestamp();
        $purchaseDate = $this->convertUnixTimestampToDatetime($purchaseTimestamp * 1000);

        if (Analytics::SUBSCRIPTION_TYPE_LIFETIME === $purchaseItem->getProductId()) {
            $validTo = (clone $purchaseDate)->modify('+100years');
        } else {
            $expires = $purchaseItem->getExpiresDate()->getTimestamp();
            $validTo = $this->convertUnixTimestampToDatetime($expires * 1000);
        }

        return (new PurchaseSubscriptionData(Device::PLATFORM_IOS))
            ->setPurchasedAt($this->convertUnixTimestampToDatetime($purchaseTimestamp * 1000))
            ->setOriginalTransactionId($purchaseItem->getOriginalTransactionId())
            ->setProductId($purchaseItem->getProductId())
            ->setTransactionId($purchaseItem->getTransactionId())
            ->setUpgraded((bool) ($purchaseItem->getRawResponse()['is_upgraded'] ?? false))
            ->setValidTo($validTo);
    }

    /**
     * @param array<int, array> $receipts
     *
     * @return PurchaseSubscriptionData[]
     */
    public function validate(array $receipts): array
    {
        return collect($receipts)
            ->map(function ($data) {
                $receipt = $data['receipt'];

                try {
                    $response = $this->validator
                        ->setReceiptData($receipt)
                        ->validate();
                } catch (Exception $e) {
                    return (new PurchaseSubscriptionData(Device::PLATFORM_IOS))
                        ->setReceipt($receipt)
                        ->setError($e->getMessage());
                }

                if (!$response->isValid()) {
                    return (new PurchaseSubscriptionData(Device::PLATFORM_IOS))
                        ->setReceipt($receipt)
                        ->setError('Receipt is not valid!');
                }

                $rawData = $response->getRawData();
                $environment = $rawData['environment'] ?? null;
                
                return collect($response->getLatestReceiptInfo())
                    ->map(function (PurchaseItem $purchaseItem) use ($environment, $receipt) {
                        $purchaseSubscriptionData = $this->processPurchaseItem($purchaseItem);
                        $purchaseSubscriptionData->setEnvironment($environment);
                        $purchaseSubscriptionData->setReceipt($receipt);

                        return $purchaseSubscriptionData;
                    })
                    ->toArray();
            })
            ->first();
    }
}
