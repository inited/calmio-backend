<?php
namespace App\Services\PurchaseSubscriptionValidation;

use App\Model\Entity\Device;
use App\Model\Entity\Subscription;
use App\Model\Factory\SubscriptionFactory;
use App\Model\Factory\SubscriptionTransactionFactory;
use App\Model\Repository\SubscriptionRepository;
use App\Services\TokenStorage\TokenStorageInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;

class PurchaseValidationService
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var AndroidValidator */
    private $androidValidator;

    /** @var IosValidator */
    private $iosValidator;

    /** @var SubscriptionFactory */
    private $subscriptionFactory;

    /** @var SubscriptionTransactionFactory */
    private $subscriptionTransactionFactory;

    /** @var SubscriptionRepository */
    private $subscriptionRepository;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    public function __construct(
        EntityManagerInterface $em,
        AndroidValidator $androidValidator,
        IosValidator $iosValidator,
        SubscriptionFactory $subscriptionFactory,
        SubscriptionTransactionFactory $subscriptionTransactionFactory,
        SubscriptionRepository $subscriptionRepository,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->androidValidator = $androidValidator;
        $this->iosValidator = $iosValidator;
        $this->subscriptionFactory = $subscriptionFactory;
        $this->subscriptionTransactionFactory = $subscriptionTransactionFactory;
        $this->subscriptionRepository = $subscriptionRepository;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @return Subscription[]
     */
    private function validateLazy(string $platform, array $receipts): array
    {
        $subscriptionEntities = collect([]);

        foreach ($receipts as $receipt) {
            $receiptToken = $receipt['receipt'];

            if (!$subscription = $this->findSubscriptionByPlatformAndReceipt($platform, $receiptToken)) {
                if ($subscriptions = $this->validateSubscriptions($platform, [$receipt])) {
                    collect($subscriptions)->each(function (Subscription $subscription) use ($subscriptionEntities) {
                        $subscriptionEntities->add($subscription);
                    });
                }
                continue;
            }

            $now = new DateTime();
            $lastTransaction = $subscription->getLastValidTransaction();

            if (!$lastTransaction
                || ($lastTransaction->getValidTo()->getTimestamp() <= $now->getTimestamp())
                || (PurchaseSubscriptionData::SUBSCRIPTION_TYPE_LIFETIME === $lastTransaction->getProductId())) {
                if ($subscriptions = $this->validateSubscriptions($platform, [$receipt])) {
                    collect($subscriptions)->each(function (Subscription $subscription) use ($subscriptionEntities) {
                        $subscriptionEntities->add($subscription);
                    });
                }
                continue;
            }

            $subscriptionEntities->add($subscription);
        }

        return $subscriptionEntities->all();
    }

    /**
     * @return Subscription[]
     */
    public function validateSubscriptions(string $platform, array $receipts, bool $lazy = false): array
    {
        if ($lazy) {
            if ($subscriptions = $this->validateLazy($platform, $receipts)) {
                return $subscriptions;
            }
        }

        switch ($platform) {
            case Device::PLATFORM_ANDROID:
                $transactions = $this->androidValidator->validate($receipts);
                break;
            case Device::PLATFORM_IOS:
                $transactions = $this->iosValidator->validate($receipts);
                break;
            default:
                throw new InvalidArgumentException(sprintf('Invalid platform %s', $platform));
        }

        if (!$transactions) {
            return [];
        }

        return $this->processTransactions($transactions);
    }

    /**
     * @param PurchaseSubscriptionData[] $transactions
     *
     * @return Subscription[]
     */
    private function processTransactions(array $transactions): array
    {
        $entities = [];

        foreach ($transactions as $transaction) {
            $subscriptionEntity = $this->findSubscriptionByPlatformAndReceipt(
                $transaction->getPlatform(),
                $transaction->getReceipt()
            );

            if (!$subscriptionEntity) {
                $subscriptionEntity = $this->subscriptionFactory->create(
                    $transaction->getPlatform(),
                    $transaction->getReceipt(),
                    $this->tokenStorage->getToken()->getUser()
                );

                if (Device::PLATFORM_ANDROID === $transaction->getPlatform()) {
                    $subscriptionEntity->setProductId($transaction->getProductId());
                }
            }

            $subscriptionEntity->setLastVerification(new DateTime());

            if ($transaction->hasError()) {
                $subscriptionEntity->setLastError($transaction->getError());
            } else {
                $subscriptionTransactionEntity = $this->subscriptionRepository->findSubscriptionTransactionByUnique(
                    ($transaction->getOrderId() ?? $transaction->getTransactionId())
                );

                if (!$subscriptionTransactionEntity) {
                    $subscriptionTransactionEntity = $this->subscriptionTransactionFactory->create();
                }

                $subscriptionTransactionEntity
                    ->setAutoRenewing($transaction->isAutoRenewing())
                    ->setCancelReason($transaction->getCancelReason())
                    ->setEnvironment($transaction->getEnvironment())
                    ->setOrderId($transaction->getOrderId())
                    ->setOriginalTransactionId($transaction->getOriginalTransactionId())
                    ->setPaymentState($transaction->getPaymentState())
                    ->setPriceAmountMicros($transaction->getPriceAmountMicros())
                    ->setPriceCurrencyCode($transaction->getPriceCurrencyCode())
                    ->setProductId($transaction->getProductId())
                    ->setPurchasedAt($transaction->getPurchasedAt())
                    ->setPurchasedAtCalculated($transaction->getPurchasedAt())
                    ->setPurchaseType($transaction->getPurchaseType())
                    ->setRenewals($transaction->getRenewals())
                    ->setTransactionId($transaction->getTransactionId())
                    ->setType($transaction->getType())
                    ->setUpgraded($transaction->isUpgraded())
                    ->setValidTo($transaction->getValidTo());

                if ((Device::PLATFORM_ANDROID === $transaction->getPlatform()) &&
                    in_array($transaction->getType(), array_keys(Subscription::SUBSCRIPTION_LENGTH))) {
                    $purchaseAtCalculated = clone $transaction->getValidTo();
                    $months = Subscription::SUBSCRIPTION_LENGTH[$transaction->getType()];
                    $purchaseAtCalculated->modify('-' . $months . 'months');
                    $subscriptionTransactionEntity->setPurchasedAtCalculated($purchaseAtCalculated);
                }

                $subscriptionEntity->addTransaction($subscriptionTransactionEntity);
            }

            $this->em->persist($subscriptionEntity);
            $this->em->getUnitOfWork()->commit($subscriptionEntity);

            $entities[] = $subscriptionEntity;
        }

        return $entities;
    }

    private function findSubscriptionByPlatformAndReceipt(string $platform, string $receipt): ?Subscription
    {
        return $this->subscriptionRepository->findOneBy([
            'platform' => $platform,
            'tokenHash' => md5($receipt),
        ]);
    }
}
