<?php

namespace App\Services\PurchaseSubscriptionValidation;

use App\Model\Entity\Analytics;
use App\Model\Entity\Device;
use Exception;
use Google_Client;
use Google_Service_AndroidPublisher;
use ReceiptValidator\GooglePlay\AbstractResponse;
use ReceiptValidator\GooglePlay\PurchaseResponse;
use ReceiptValidator\GooglePlay\SubscriptionResponse;
use ReceiptValidator\GooglePlay\Validator;
use Tracy\Debugger;
use Tracy\ILogger;

class AndroidValidator extends BaseValidator
{
    const PACKAGE_NAME = 'cz.calmio.app';

    /** @var Validator $validator */
    private $validator;

    public function __construct()
    {
        $googleClient = new Google_Client();
        $googleClient->setScopes([Google_Service_AndroidPublisher::ANDROIDPUBLISHER]);
        $googleClient->setAuthConfig(__DIR__ . '/../../../pc-api-6212647025628175347-779-217cd40c8051.json');

        $this->validator = new Validator(new Google_Service_AndroidPublisher($googleClient));
    }

    /**
     * @throws Exception
     */
    private function getData(string $productId, string $purchaseToken): AbstractResponse
    {
        try {
            return $this->validator->setPackageName(self::PACKAGE_NAME)
                ->setProductId($productId)
                ->setPurchaseToken($purchaseToken)
                ->validate();
        } catch (Exception $e) {
            $message = 'Purchase token verification failed! (purchaseToken: ' . $purchaseToken . ',productId: '
                . $productId . ', error: ' . $e->getMessage() . ')';

            Debugger::log($message, ILogger::EXCEPTION);
            throw $e;
        }
    }

    /**
     * @param array<int, array> $receipts
     *
     * @return PurchaseSubscriptionData[]
     */
    public function validate(array $receipts): array
    {
        return collect($receipts)
            ->map(function ($data) {
                $productId = $data['productId'];
                $purchaseToken = $data['receipt'];

                $purchaseSubscriptionData = (new PurchaseSubscriptionData(Device::PLATFORM_ANDROID))
                    ->setProductId($productId)
                    ->setReceipt($purchaseToken);

                try {
                    if (Analytics::SUBSCRIPTION_TYPE_LIFETIME === $productId) {
                        /** @var PurchaseResponse $response */
                        $response = $this->validatePurchase($productId, $purchaseToken);

                        $purchaseSubscriptionData->setAutoRenewing(false);
                        $purchaseSubscriptionData->setCancelReason(null);
                        $purchaseSubscriptionData->setPaymentState((int) $response->getPurchaseState());

                        $purchasedAt = $this->convertUnixTimestampToDatetime((int) $response->getPurchaseTimeMillis());
                        $purchaseSubscriptionData->setPurchasedAt($purchasedAt);
                        $purchaseSubscriptionData->setValidTo((clone $purchasedAt)->modify('+100years'));
                    } else {
                        /** @var SubscriptionResponse $response */
                        $response = $this->validateSubscription($productId, $purchaseToken);

                        $rawResponse = $response->getRawResponse();
                        $purchaseSubscriptionData->setAutoRenewing($rawResponse->getAutoRenewing());
                        $purchaseSubscriptionData->setCancelReason($rawResponse->getCancelReason());
                        $purchaseSubscriptionData->setPaymentState($rawResponse->getPaymentState());
                        $purchaseSubscriptionData->setPriceAmountMicros((int) $rawResponse->getPriceAmountMicros());
                        $purchaseSubscriptionData->setPriceCurrencyCode($rawResponse->getPriceCurrencyCode());

                        $purchasedAt = $this->convertUnixTimestampToDatetime((int) $response->getStartTimeMillis());
                        $purchaseSubscriptionData->setPurchasedAt($purchasedAt);
                        $validTo = $this->convertUnixTimestampToDatetime($response->getExpiryTimeMillis());
                        $purchaseSubscriptionData->setValidTo($validTo);
                    }
                } catch (Exception $e) {
                    $purchaseSubscriptionData->setError($e->getMessage());
                    return $purchaseSubscriptionData;
                }

                $response = $response->getRawResponse();
                $orderId = $response->getOrderId();
                if ($orderId && strpos($orderId, '..') !== false) {
                    $orderIdExploded = explode('..', $orderId);
                    $renewals = ((int) end($orderIdExploded)) + 1;
                }

                return $purchaseSubscriptionData
                    ->setOrderId($orderId)
                    ->setProductId($productId)
                    ->setPurchaseType($response->getPurchaseType())
                    ->setRenewals($renewals ?? 0);
            })
            ->toArray();
    }

    /**
     * @throws Exception
     */
    public function validatePurchase(string $productId, string $purchaseToken): AbstractResponse
    {
        $this->validator->setValidationModePurchase(true);
        return $this->getData($productId, $purchaseToken);
    }

    /**
     * @throws Exception
     */
    public function validateSubscription(string $productId, string $purchaseToken): AbstractResponse
    {
        $this->validator->setValidationModePurchase(false);
        return $this->getData($productId, $purchaseToken);
    }
}
