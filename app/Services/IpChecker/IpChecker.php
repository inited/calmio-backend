<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 27. 04. 2019
 */

declare(strict_types=1);

namespace App\Services\IpChecker;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\StatusCode;

/**
 * Class IpChecker
 * @package App\Services\IpChecker
 */
class IpChecker
{
    /**
     * @var string[]
     */
    private $whitelist = [];

    /**
     * IpChecker constructor.
     *
     * @param array $whitelist
     */
    public function __construct(array $whitelist)
    {
        $this->whitelist = $whitelist;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param mixed    $next
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response, $next)
    {
        $ip = $request->getAttribute('ip_address');

        if (!empty($ip) && in_array($ip, $this->whitelist)) {
            return $next($request, $response);
        }

        return $response->withStatus(StatusCode::HTTP_UNAUTHORIZED);
    }
}
