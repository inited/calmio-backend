<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 01.04.2020
 */

declare(strict_types=1);

namespace App\EntitySubscriber;

use App\Model\Entity\UserEntity;
use App\Services\MailchimpService\MailchimpServiceInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Exception;
use Tracy\Debugger;
use Tracy\ILogger;

/**
 * Class UserEntityMailchimpSyncSubscriber
 * @package App\Model\EntitySubscriber
 */
class UserEntityMailchimpSyncSubscriber implements EventSubscriber
{
    /** @var MailchimpServiceInterface */
    private $mailchimpService;

    public function __construct(MailchimpServiceInterface $mailchimpService)
    {
        $this->mailchimpService = $mailchimpService;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::postUpdate,
        ];
    }

    public function postPersist(LifecycleEventArgs $event): void
    {

        // Volani Mailchimpu vypnuto, nepouziva se
        return;

        /* $entity = $event->getEntity();

        if ($entity instanceof UserEntity) {
            if (!Debugger::$productionMode) {
                return;
            }

            try {
                $entity->setMailchimpId($this->mailchimpService->createMember($entity));
            } catch (Exception $e) {
                Debugger::log($e, ILogger::ERROR);
                return;
            }

            $event->getEntityManager()->getUnitOfWork()->commit($entity);
        } */
    }

    public function postUpdate(LifecycleEventArgs $event): void
    {

        // Volani Mailchimpu vypnuto, nepouziva se
        return;

        // /** @var UserEntity $entity */
        /* $entity = $event->getEntity();

        if ($entity instanceof UserEntity) {
            if (!Debugger::$productionMode) {
                return;
            }

            if (null === $entity->getMailchimpId()) {
                $this->postPersist($event);
                return;
            }

            $changeSet = $event->getEntityManager()->getUnitOfWork()->getEntityChangeSet($entity);

            if (array_key_exists('lastLogin', $changeSet)) {
                try {
                    $this->mailchimpService->updateMember($entity);
                } catch (Exception $e) {
                    Debugger::log($e, ILogger::ERROR);
                    return;
                }
            }
        } */
    }
}
