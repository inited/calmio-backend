<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11.03.2020
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Version;

/**
 * Class VersionRepository
 * @package App\Model\Repository
 * @method Version find($id, $lockMode = null, $lockVersion = null)
 */
final class VersionRepository extends BaseRepository implements VersionRepositoryInterface
{
}
