<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11. 08. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Course;
use App\Model\Entity\CourseLanguage;
use App\Model\Entity\CourseUser;
use App\Model\Entity\Language;
use App\Model\Entity\LessonEntity;
use Illuminate\Support\Collection;

/**
 * Class CourseRepository
 * @package App\Model\Repository
 */
final class CourseRepository extends BaseRepository implements CourseRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function findCourseByLesson(LessonEntity $lessonEntity): Course
    {
        $course = $this->_em->createQueryBuilder()
            ->select('course')
            ->from(Course::class, 'course')
            ->andWhere(':lesson MEMBER OF course.lessons')
            ->setParameter('lesson', $lessonEntity)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        if (0 < count($course)) {
            $course = reset($course);
            return $course;
        }

        throw  new EntityNotFoundException();
    }

    public function findDailyMeditationByLanguage(Language $language): ?Course
    {
        return $this->_em->createQueryBuilder()
            ->select('course')
            ->from(Course::class, 'course')
            ->join('course.languages', 'courseLanguage')
            ->andWhere('course.type = :type')
            ->andWhere('courseLanguage.language = :language AND courseLanguage.dailyMeditation = :dailyMeditation')
            ->setParameter('dailyMeditation', true)
            ->setParameter('language', $language)
            ->setParameter('type', Course::TYPE_MEDITATION)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findTomorrowPreferredDailyMeditation(): ?Course
    {
        $dailyMeditation = $this->_em->createQueryBuilder()
            ->select('course')
            ->from(Course::class, 'course')
            ->andWhere('course.type = :type')
            ->andWhere('course.useAsNextDailyMeditation = :true')
            ->setParameter('true', true)
            ->setParameter('type', Course::TYPE_MEDITATION)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        if (!$dailyMeditation) {
            return null;
        }

        /** @var Course $dailyMeditation */
        $dailyMeditation = reset($dailyMeditation);
        return $dailyMeditation;
    }

    public function getCourseLanguageEntitiesByLanguageForDailyMeditations(Language $language): array
    {
        return $this->_em->createQueryBuilder()
            ->select('courseLanguage')
            ->from(CourseLanguage::class, 'courseLanguage')
            ->join('courseLanguage.course', 'course')
            ->andWhere('courseLanguage.language = :language')
            ->andWhere('course.type = :type')
            ->setParameter('language', $language)
            ->setParameter('type', Course::TYPE_MEDITATION)
            ->addOrderBy('courseLanguage.course')
            ->getQuery()
            ->getResult();
    }

    public function getCourseUserStatistics(Course $course): array
    {
        $users = $this->_em->createQueryBuilder()
            ->select('user.email', 'courseUserLesson.time', 'lesson.order AS lessonOrder')
            ->from(CourseUser::class, 'courseUser')
            ->leftJoin('courseUser.user', 'user')
            ->leftJoin('courseUser.lessons', 'courseUserLesson')
            ->leftJoin('courseUserLesson.lesson', 'lesson')
            ->andWhere('courseUser.course = :course')
            ->setParameter('course', $course)
            ->getQuery()
            ->getArrayResult();

        return collect($users)
            ->groupBy('email')
            ->transform(function (Collection $lessons) {
                return $lessons->mapWithKeys(function ($lesson) {
                    return [$lesson['lessonOrder'] => $lesson['time']];
                });
            })
            ->toArray();
    }

    public function invalidateTomorrowDailyMeditation(): void
    {
        $this->_em->createQueryBuilder()
            ->update(Course::class, 'course')
            ->set('course.useAsNextDailyMeditation', 0)
            ->getQuery()
            ->execute();
    }

    public function clear(): void
    {
        $this->_em->clear();
    }
}
