<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 13. 08. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

/**
 * Class TagRepository
 * @package App\Model\Repository
 */
final class TagRepository extends BaseRepository implements TagRepositoryInterface
{
}
