<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InactiveUserException;
use App\Exceptions\InvalidCredentialsException;
use App\Model\Entity\Tag;
use App\Model\Entity\UserDevice;
use App\Model\Entity\UserEntity;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Class UserRepository
 * @package App\Model\Repository
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
final class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function findAllByPaginated(int $limit, int $offset): Paginator
    {
        return new Paginator(
            $this->_em->createQueryBuilder()
                ->select('user')
                ->from(UserEntity::class, 'user')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->addOrderBy('user.id')
                ->getQuery()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function findAllUsers(bool $onlyActive = false): array
    {
        $query = $this->createQueryBuilder('u')
            ->select('u.id', 'u.firstName AS firstname', 'u.lastName AS lastname', 'c.name AS company')
            ->addSelect('u.email', 'u.active', 'UNIX_TIMESTAMP(u.lastLogin) AS lastlogin')
            ->addSelect('UNIX_TIMESTAMP(u.created) AS registered')
            ->join('u.company', 'c')
            ->where('u.deleted = :deleted')
            ->setParameters([
                'deleted' => false,
            ]);

        if ($onlyActive) {
            $query
                ->andWhere('u.active = :active')
                ->setParameter('active', $onlyActive);
        }

        return $query->getQuery()->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * {@inheritdoc}
     */
    public function findByAppleToken(string $token): UserEntity
    {
        try {
            $user = $this->_em->createQueryBuilder()
                ->select('user')
                ->from(UserEntity::class, 'user')
                ->andWhere('user.appleToken = :token')
                ->setParameter('token', $token)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $user) {
                return $user;
            }
        } catch (NonUniqueResultException $e) {
            // Do nothing
        }

        throw new EntityNotFoundException();
    }

    /**
     * {@inheritdoc}
     */
    public function findByEmail(string $email): UserEntity
    {
        try {
            $user = $this->_em->createQueryBuilder()
                ->select('user')
                ->from(UserEntity::class, 'user')
                ->andWhere('user.email = :email')
                ->setParameter('email', $email)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $user) {
                return $user;
            }
        } catch (NonUniqueResultException $e) {
            // Do nothing
        }

        throw new EntityNotFoundException();
    }

    /**
     * {@inheritDoc}
     */
    public function findByExternalId(int $id): UserEntity
    {
        try {
            $user = $this->_em->createQueryBuilder()
                ->select('user')
                ->from(UserEntity::class, 'user')
                ->andWhere('user.externalId = :externalId')
                ->setParameter('externalId', $id)
                ->getQuery()
                ->getOneOrNullResult();

            if ($user instanceof UserEntity) {
                return $user;
            }
        } catch (NonUniqueResultException $e) {
            // Do nothing
        }
        throw new EntityNotFoundException();
    }

    /**
     * {@inheritdoc}
     */
    public function findByFacebookToken(string $token): UserEntity
    {
        try {
            $user = $this->_em->createQueryBuilder()
                ->select('user')
                ->from(UserEntity::class, 'user')
                ->andWhere('user.facebookToken = :token')
                ->setParameter('token', $token)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $user) {
                return $user;
            }
        } catch (NonUniqueResultException $e) {
            // Do nothing
        }

        throw new EntityNotFoundException();
    }

    /**
     * {@inheritdoc}
     */
    public function findByGoogleToken(string $token): UserEntity
    {
        try {
            $user = $this->_em->createQueryBuilder()
                ->select('user')
                ->from(UserEntity::class, 'user')
                ->andWhere('user.googleToken = :token')
                ->setParameter('token', $token)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $user) {
                return $user;
            }
        } catch (NonUniqueResultException $e) {
            // Do nothing
        }

        throw new EntityNotFoundException();
    }

    /**
     * {@inheritDoc}
     */
    public function findByPasswordResetHash(string $hash): UserEntity
    {
        try {
            $user = $this->_em->createQueryBuilder()
                ->select('user')
                ->from(UserEntity::class, 'user')
                ->andWhere('user.passwordResetHash = :hash')
                ->setParameter('hash', $hash)
                ->getQuery()
                ->getOneOrNullResult();

            if ($user instanceof UserEntity) {
                return $user;
            }
        } catch (NonUniqueResultException $e) {
            // Do nothing
        }

        throw new EntityNotFoundException();
    }

    /**
     * {@inheritdoc}
     */
    public function findByUsername(string $username): UserEntity
    {
        try {
            $user = $this->_em->createQueryBuilder()
                ->select('user')
                ->from(UserEntity::class, 'user')
                ->andWhere('user.username = :username')
                ->setParameter('username', $username)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $user) {
                return $user;
            }
        } catch (NonUniqueResultException $e) {
            // Do nothing
        }

        throw new EntityNotFoundException();
    }

    public function getQuestionnaireData(): array
    {
        return $this->_em->createQueryBuilder()
            ->select('user.email', 'userExperience.id AS experience')
            ->addSelect('IF(:betterSleep MEMBER OF user.installReason,\'X\',\'\') AS betterSleep')
            ->addSelect('IF(:contactWithBody MEMBER OF user.installReason,\'X\',\'\') AS contactWithBody')
            ->addSelect('IF(:focusAndProductivity MEMBER OF user.installReason,\'X\',\'\') AS focusAndProductivity')
            ->addSelect('IF(:mentalHealth MEMBER OF user.installReason,\'X\',\'\') AS mentalHealth')
            ->addSelect('IF(:personalGrowth MEMBER OF user.installReason,\'X\',\'\') AS personalGrowth')
            ->addSelect('IF(:stressAndAnxiety MEMBER OF user.installReason,\'X\',\'\') AS stressAndAnxiety')
            ->from(UserEntity::class, 'user')
            ->leftJoin('user.experience', 'userExperience')
            ->leftJoin('user.installReason', 'installReason')
            ->addGroupBy('user.email')
            ->setParameter('betterSleep', Tag::BETTER_SLEEP)
            ->setParameter('contactWithBody', Tag::CONTACT_WITH_BODY)
            ->setParameter('focusAndProductivity', Tag::FOCUS_AND_PRODUCTIVITY)
            ->setParameter('mentalHealth', Tag::MENTAL_HEALTH)
            ->setParameter('personalGrowth', Tag::PERSONAL_GROWTH)
            ->setParameter('stressAndAnxiety', Tag::STRESS_AND_ANXIETY)
            ->getQuery()
            ->getArrayResult();
    }

    public function getUserStatistics(): array
    {
        return $this->_em->createQueryBuilder()
            ->select('user.email', 'user.created', 'user.lastLogin')
            ->from(UserEntity::class, 'user')
            ->getQuery()
            ->getArrayResult();
    }

    public function invalidateSameUserDeviceToken(UserEntity $user, string $token): void
    {
        $this->_em->createQueryBuilder()
            ->update(UserDevice::class, 'userDevice')
            ->set('userDevice.valid', 0)
            ->andWhere('userDevice.token = :token')
            ->andWhere('userDevice.user != :user')
            ->setParameter('token', $token)
            ->setParameter('user', $user)
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function verifyUser($email, $password)
    {
        /** @var UserEntity $user */
        $user = $this->findByEmail($email);

        if (!$user->isActive()) {
            throw new InactiveUserException();
        }

        if (!password_verify($password, $user->getPassword())) {
            throw new InvalidCredentialsException();
        }

        return $user;
    }

    public function clear(): void
    {
        $this->_em->clear();
    }
}
