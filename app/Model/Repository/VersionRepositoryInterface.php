<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11.03.2020
 */

declare(strict_types=1);

namespace App\Model\Repository;

/**
 * Interface VersionRepositoryInterface
 * @package App\Model\Repository
 */
interface VersionRepositoryInterface extends BaseRepositoryInterface
{
}
