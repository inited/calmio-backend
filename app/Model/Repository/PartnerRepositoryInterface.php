<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Partner;

/**
 * @method Partner find($id, $lockMode = null, $lockVersion = null)
 * @method Partner findOneBy(array $criteria)
 * @method Partner[] findAll()
 * @method Partner[] findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
interface PartnerRepositoryInterface extends BaseRepositoryInterface
{
}
