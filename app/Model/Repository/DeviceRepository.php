<?php

namespace App\Model\Repository;

use App\Model\Entity\Device;
use Doctrine\ORM\EntityRepository;

/**
 * @method Device|null find($id, $lockMode = null, $lockVersion = null)
 */
final class DeviceRepository extends EntityRepository
{
}
