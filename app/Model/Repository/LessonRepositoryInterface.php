<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11. 08. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

/**
 * Interface LessonRepositoryInterface
 * @package App\Model\Repository
 */
interface LessonRepositoryInterface extends BaseRepositoryInterface
{
}
