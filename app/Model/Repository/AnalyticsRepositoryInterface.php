<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Analytics;
use App\Model\Entity\UserInterface;

interface AnalyticsRepositoryInterface extends BaseRepositoryInterface
{
    public function findByUser(UserInterface $user): ?Analytics;

    /** @return Analytics[] */
    public function findActiveByUserAndPlatform(UserInterface $user, string $platform): array;
}
