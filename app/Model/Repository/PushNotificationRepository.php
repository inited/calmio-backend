<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 14.06.2020
 */

declare(strict_types=1);

namespace App\Model\Repository;

final class PushNotificationRepository extends BaseRepository implements PushNotificationRepositoryInterface
{

}
