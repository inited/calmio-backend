<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 21. 01. 2020
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Role;

/**
 * Class RoleRepository
 * @package App\Model\Repository
 * @method Role find($id, $lockMode = null, $lockVersion = null)
 * @method Role[] findAll()
 * @method Role[] findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
final class RoleRepository extends BaseRepository implements RoleRepositoryInterface
{

}
