<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

/**
 * Class DailyMeditationRepository
 * @package App\Model\Repository
 */
final class DailyMeditationRepository extends BaseRepository implements DailyMeditationRepositoryInterface
{

}
