<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 13. 08. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

/**
 * Interface TagRepositoryInterface
 * @package App\Model\Repository
 */
interface TagRepositoryInterface extends BaseRepositoryInterface
{


}
