<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\InvalidAuthTokenException;
use App\Model\Entity\TokenEntity;
use App\Model\Entity\UserEntity;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;

/**
 * Class TokenRepository
 * @package App\Model\Repository\Token
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
final class TokenRepository extends BaseRepository implements TokenRepositoryInterface
{

    /**
     * @param UserEntity $entity
     *
     * @return mixed
     * @throws Exception
     */
    public function generateToken(UserEntity $entity)
    {
        $token = new TokenEntity();
        $token->setUser($entity);
        return $this->saveEntity($token);
    }

    /**
     * @param TokenEntity $token
     *
     * @return bool
     */
    public function invalidateToken(TokenEntity $token): bool
    {
        try {
            $token->setValid(false);
            $this->saveEntity($token);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function verifyToken($token)
    {
        try {
            /** @var TokenEntity $tokenEntity */
            $tokenEntity = $this->findOneBy([
                'id' => $token,
            ]);

            if ($tokenEntity instanceof TokenEntity && $tokenEntity->isValid()) {
                return $tokenEntity;
            }
        } catch (Exception $e) {
            // Do nothing
        }

        throw new InvalidAuthTokenException();
    }

    /**
     * @param UserEntity $user
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function invalidateUserTokens(UserEntity $user): void
    {
        $tokens = $this->findBy([
            'user' => $user,
            'valid' => true,
        ]);
        if ($tokens) {
            /** @var TokenEntity $token */
            foreach ($tokens as $token) {
                $token->setValid(false);
            }
            $this->_em->flush();
        }
    }
}
