<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\PromoCode;

final class PromoCodeRepository extends BaseRepository implements PromoCodeRepositoryInterface
{
    public function getAvailableGiftPromoCodeValues(): array
    {
        $values = $this->_em->createQueryBuilder()
            ->select("promoCode.value")
            ->from(PromoCode::class, "promoCode")
            ->andWhere("promoCode.type = :type")
            ->andWhere("promoCode.available > promoCode.used")
            ->setParameter("type", PromoCode::TYPE_GIFT)
            ->addGroupBy("promoCode.value")
            ->getQuery()
            ->getArrayResult();

        return collect($values)
            ->pluck("value")
            ->all();
    }

    public function getNumberOfAvailableGiftPromoCodeByValue(int $value): int
    {
        return (int) $this->_em->createQueryBuilder()
            ->select("COUNT(promoCode)")
            ->from(PromoCode::class, "promoCode")
            ->andWhere("promoCode.type = :type")
            ->andWhere("promoCode.value = :value")
            ->andWhere("promoCode.available > promoCode.used")
            ->setParameter("type", PromoCode::TYPE_GIFT)
            ->setParameter("value", $value)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
