<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Partner;
use Doctrine\ORM\NonUniqueResultException;
use Tracy\Debugger;
use Tracy\ILogger;

final class PartnerRepository extends BaseRepository implements PartnerRepositoryInterface
{
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        try {
            $partner = $this->_em->createQueryBuilder()
                ->select('partner')
                ->from(Partner::class, 'partner')
                ->andWhere('partner.id = :id')
                ->andWhere('partner.deleted = :deleted')
                ->setParameter('id', $id)
                ->setParameter('deleted', false)
                ->getQuery()
                ->getOneOrNullResult();

            if ($partner instanceof Partner) {
                return $partner;
            }
        } catch (NonUniqueResultException $e) {
            Debugger::log($e, ILogger::EXCEPTION);
        }

        $crop = strrchr($this->getClassName(), "\\");

        throw new EntityNotFoundException(
            sprintf(substr($crop ? $crop : '', 1) . ' [%d] was not found.', $id)
        );
    }

    public function findAll()
    {
        return $this->_em->createQueryBuilder()
            ->select('partner')
            ->from(Partner::class, 'partner')
            ->andWhere('partner.deleted = :deleted')
            ->setParameter('deleted', false)
            ->getQuery()
            ->getResult();
    }
}
