<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11.03.2020
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Language;

/**
 * Interface LanguageRepositoryInterface
 * @package App\Model\Repository
 * @method Language[] findAll()
 */
interface LanguageRepositoryInterface extends BaseRepositoryInterface
{
}
