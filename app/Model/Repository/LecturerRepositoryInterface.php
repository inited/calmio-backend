<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Lecturer;

/**
 * Interface LecturerRepositoryInterface
 * @package App\Model\Repository
 * @method Lecturer find($id, $lockMode = null, $lockVersion = null)
 * @method Lecturer|null findOneBy(array $criteria)
 * @method Lecturer[] findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
interface LecturerRepositoryInterface extends BaseRepositoryInterface
{
}
