<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 21. 01. 2020
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Role;

/**
 * Interface RoleRepositoryInterface
 * @package App\Model\Repository
 * @method Role[] findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 */
interface RoleRepositoryInterface extends BaseRepositoryInterface
{
}
