<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\PromoCode;

/**
 * Interface PromoCodeRepositoryInterface
 * @package App\Model\Repository
 * @method PromoCode find($id, $lockMode = null, $lockVersion = null)
 */
interface PromoCodeRepositoryInterface extends BaseRepositoryInterface
{
    /** @return int[] */
    public function getAvailableGiftPromoCodeValues(): array;

    public function getNumberOfAvailableGiftPromoCodeByValue(int $value): int;
}
