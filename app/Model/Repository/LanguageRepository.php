<?php

/**
 *
 */

declare(strict_types=1);

namespace App\Model\Repository;

/**
 * Class LanguageRepository
 * @package App\Model\Repository
 */
final class LanguageRepository extends BaseRepository implements LanguageRepositoryInterface
{

}
