<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

/**
 * Class FileRepository
 * @package App\Model\Repository
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
final class FileRepository extends BaseRepository implements FileRepositoryInterface
{

}
