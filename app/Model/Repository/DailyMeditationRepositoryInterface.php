<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

/**
 * Interface DailyMeditationRepositoryInterface
 * @package App\Model\Repository
 */
interface DailyMeditationRepositoryInterface extends BaseRepositoryInterface
{

}
