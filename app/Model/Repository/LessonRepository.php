<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11. 08. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

/**
 * Class LessonRepository
 * @package App\Model\Repository
 */
final class LessonRepository extends BaseRepository implements LessonRepositoryInterface
{

}
