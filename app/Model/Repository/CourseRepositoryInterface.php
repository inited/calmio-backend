<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11. 08. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Course;
use App\Model\Entity\CourseLanguage;
use App\Model\Entity\Language;
use App\Model\Entity\LessonEntity;

/**
 * Interface CourseRepositoryInterface
 * @package App\Model\Repository
 * @method Course find($id, $lockMode = null, $lockVersion = null)
 * @method Course[] findAll()
 */
interface CourseRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @param LessonEntity $lessonEntity
     *
     * @return Course
     * @throws EntityNotFoundException
     */
    public function findCourseByLesson(LessonEntity $lessonEntity): Course;

    public function findDailyMeditationByLanguage(Language $language): ?Course;

    public function findTomorrowPreferredDailyMeditation(): ?Course;

    /**
     * @return CourseLanguage[]
     */
    public function getCourseLanguageEntitiesByLanguageForDailyMeditations(Language $language): array;

    /**
     * @param Course $course
     *
     * @return array<string,mixed>
     */
    public function getCourseUserStatistics(Course $course): array;

    public function invalidateTomorrowDailyMeditation(): void;

    public function clear(): void;
}
