<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;

/**
 * Class BaseRepository
 * @package App\Model\Repository
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 */
abstract class BaseRepository extends EntityRepository implements BaseRepositoryInterface
{
    public function count(array $criteria = []): int
    {
        return (int) parent::count($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        if ($entity = parent::find($id, $lockMode, $lockVersion)) {
            return $entity;
        }

        $crop = strrchr($this->getClassName(), "\\");

        throw new EntityNotFoundException(
            sprintf(substr($crop ? $crop : '', 1) . ' [%d] was not found.', $id)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function persistEntity($object)
    {
        $this->_em->persist($object);
    }

    /**
     * {@inheritdoc}
     */
    public function saveEntities(array $entities)
    {
        foreach ($entities as $entity) {
            $this->_em->persist($entity);
        }
        $this->_em->flush();
        return $entities;
    }

    /**
     * {@inheritdoc}
     */
    public function saveEntity($object)
    {
        $this->_em->persist($object);
        $this->_em->flush();
        return $object;
    }
}
