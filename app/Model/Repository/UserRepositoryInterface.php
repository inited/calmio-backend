<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Exceptions\InactiveUserException;
use App\Exceptions\InvalidCredentialsException;
use App\Model\Entity\UserEntity;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * Interface UserRepositoryInterface
 * @package App\Model\Repository
 */
interface UserRepositoryInterface extends BaseRepositoryInterface
{
    public function findAllByPaginated(int $limit, int $offset): Paginator;

    /**
     * @param bool $onlyActive
     *
     * @return string[]
     */
    public function findAllUsers(bool $onlyActive = false): array;

    /**
     * @param string $token
     *
     * @return UserEntity
     * @throws EntityNotFoundException
     */
    public function findByAppleToken(string $token): UserEntity;

    /**
     * @param string $email
     *
     * @return UserEntity
     * @throws EntityNotFoundException
     */
    public function findByEmail(string $email): UserEntity;

    /**
     * @param int $id
     *
     * @return UserEntity
     * @throws EntityNotFoundException
     */
    public function findByExternalId(int $id): UserEntity;

    /**
     * @throws EntityNotFoundException
     */
    public function findByFacebookToken(string $token): UserEntity;

    /**
     * @param string $token
     *
     * @return UserEntity
     * @throws EntityNotFoundException
     */
    public function findByGoogleToken(string $token): UserEntity;

    /**
     * @param string $hash
     *
     * @return UserEntity
     * @throws EntityNotFoundException
     */
    public function findByPasswordResetHash(string $hash): UserEntity;

    /**
     * @param string $username
     *
     * @return UserEntity
     * @throws EntityNotFoundException
     */
    public function findByUsername(string $username): UserEntity;

    /**
     * @return array<string, mixed>
     */
    public function getQuestionnaireData(): array;

    /**
     * @return array<string, mixed>
     */
    public function getUserStatistics(): array;

    public function invalidateSameUserDeviceToken(UserEntity $user, string $token): void;

    /**
     * @param mixed $email
     * @param mixed $password
     *
     * @return UserEntity
     * @throws EntityNotFoundException
     * @throws InactiveUserException
     * @throws InvalidCredentialsException
     */
    public function verifyUser($email, $password);

    public function clear(): void;
}
