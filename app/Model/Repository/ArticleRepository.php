<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Article;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Class ArticleRepository
 * @package App\Model\Repository
 */
final class ArticleRepository extends BaseRepository implements ArticleRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function findByKeyAndLanguage(string $key, string $language): Article
    {
        try {
            $article = $this->_em->createQueryBuilder()
                ->select('article')
                ->from(Article::class, 'article')
                ->andWhere('article.key = :key')
                ->andWhere('article.language = :language')
                ->setParameter('key', $key)
                ->setParameter('language', $language)
                ->getQuery()
                ->getOneOrNullResult();

            if (null !== $article) {
                return $article;
            }
        } catch (NonUniqueResultException $e) {
            // Do nothing
        }

        throw new EntityNotFoundException();
    }
}
