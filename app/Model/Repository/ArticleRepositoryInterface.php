<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Repository;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Article;

/**
 * Interface ArticleRepositoryInterface
 * @package App\Model\Repository
 */
interface ArticleRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @param string $key
     * @param string $language
     *
     * @return Article
     * @throws EntityNotFoundException
     */
    public function findByKeyAndLanguage(string $key, string $language): Article;

}
