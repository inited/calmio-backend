<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Entity\Analytics;
use App\Model\Entity\UserInterface;
use Doctrine\ORM\NonUniqueResultException;
use Tracy\Debugger;
use Tracy\ILogger;

final class AnalyticsRepository extends BaseRepository implements AnalyticsRepositoryInterface
{
    public function findByUser(UserInterface $user): ?Analytics
    {
        try {
            return $this->_em->createQueryBuilder()
                ->select('analytics')
                ->from(Analytics::class, 'analytics')
                ->andWhere('analytics.user=:user')
                ->setParameter('user', $user)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            Debugger::log($e, ILogger::EXCEPTION);
            return null;
        }
    }

    public function findActiveByUserAndPlatform(UserInterface $user, string $platform): array
    {
        return $this->_em->createQueryBuilder()
            ->select('analytics')
            ->from(Analytics::class, 'analytics')
            ->andWhere('analytics.user=:user')
            ->andWhere('analytics.platform=:platform')
            ->andWhere('analytics.active = :active')
            ->setParameter('active', true)
            ->setParameter('platform', $platform)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
