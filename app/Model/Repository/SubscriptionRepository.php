<?php

namespace App\Model\Repository;

use App\Model\Entity\Device;
use App\Model\Entity\Subscription;
use App\Model\Entity\SubscriptionTransaction;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Generator;

final class SubscriptionRepository extends EntityRepository
{
    public function findSubscriptionTransactionByUnique(string $unique): ?SubscriptionTransaction
    {
        $exprBuilder = $this->_em->getExpressionBuilder();

        return $this->_em->createQueryBuilder()
            ->select('subscriptionTransaction')
            ->from(SubscriptionTransaction::class, 'subscriptionTransaction')
            ->andWhere($exprBuilder->orX(
                $exprBuilder->eq('subscriptionTransaction.orderId', ':unique'),
                $exprBuilder->eq('subscriptionTransaction.transactionId', ':unique')
            ))
            ->setParameter('unique', $unique)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return Generator|Subscription[]
     */
    public function getAllUsersSubscriptionsToVerify()
    {
        $offset = 0;
        $chunkSize = 50;
        $exprBuilder = $this->_em->getExpressionBuilder();
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder->select('subscription')
            ->from(Subscription::class, 'subscription')
            ->andWhere(
                $exprBuilder->orX(
                    $exprBuilder->andX(
                        $exprBuilder->eq('subscription.platform', ':android'),
                        $exprBuilder->eq('subscription.lastTransactionIsAutoRenewing', ':autoRenewing'),
                        $exprBuilder->orX(
                            $exprBuilder->isNull('subscription.lastTransactionValidTo'),
                            $exprBuilder->lt('subscription.lastTransactionValidTo', ':validTo')
                        )
                    ),
                    $exprBuilder->andX(
                        $exprBuilder->eq('subscription.platform', ':ios'),
                        $exprBuilder->eq('subscription.lastTransactionIsUpgraded', ':lastUpgraded'),
                        $exprBuilder->orX(
                            $exprBuilder->isNull('subscription.lastTransactionValidTo'),
                            $exprBuilder->andX(
                                $exprBuilder->lt('subscription.lastTransactionValidTo', ':validTo'),
                                $exprBuilder->gte('subscription.lastTransactionValidTo', ':validTo14days')
                            )
                        )
                    )
                ))
            ->setParameter('android', Device::PLATFORM_ANDROID)
            ->setParameter('autoRenewing', true)
            ->setParameter('ios', Device::PLATFORM_IOS)
            ->setParameter('lastUpgraded', false)
            ->setParameter('validTo', (new DateTime())->format('Y-m-d H:i:s'))
            ->setParameter('validTo14days', (new DateTime('-14days'))->format('Y-m-d H:i:s'))
            ->groupBy('subscription')
            ->orderBy('subscription.id');

        while (true) {
            $queryBuilder->setFirstResult($offset);
            $queryBuilder->setMaxResults($chunkSize);

            $subscriptions = $queryBuilder->getQuery()->getResult();

            if (!count($subscriptions)) {
                break;
            }

            foreach ($subscriptions as $subscription) {
                yield $subscription;
                $this->_em->detach($subscription);
            }

            $offset += $chunkSize;
        }
    }
}
