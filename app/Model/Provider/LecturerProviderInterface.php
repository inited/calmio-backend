<?php

declare(strict_types=1);

namespace App\Model\Provider;

use App\Model\Entity\Lecturer;

interface LecturerProviderInterface
{
    /**
     * @param string[] $data
     */
    public function updateLecturer(Lecturer $lecturer, array $data): void;
}
