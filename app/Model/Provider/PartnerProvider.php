<?php

declare(strict_types=1);

namespace App\Model\Provider;

use App\Model\Entity\Partner;

class PartnerProvider implements PartnerProviderInterface
{
    public function updatePartner(Partner $partner, array $data): void
    {
        $partner->setAddress((string) $data["address"] ?? $partner->getAddress());
        $partner->setName((string) $data["name"] ?? $partner->getName());
    }
}
