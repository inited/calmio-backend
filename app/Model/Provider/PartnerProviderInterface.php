<?php

declare(strict_types=1);

namespace App\Model\Provider;

use App\Model\Entity\Partner;

interface PartnerProviderInterface
{
    /**
     * @param Partner              $partner
     * @param array<string, mixed> $data
     */
    public function updatePartner(Partner $partner, array $data): void;
}
