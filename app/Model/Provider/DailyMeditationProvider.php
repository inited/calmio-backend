<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Provider;

use App\Model\Entity\DailyMeditation;
use App\Model\Entity\Language;
use App\Model\Entity\LessonEntity;
use App\Model\Repository\LanguageRepositoryInterface;
use DateTime;

/**
 * Class DailyMeditationProvider
 * @package App\Model\Provider
 */
class DailyMeditationProvider implements DailyMeditationProviderInterface
{
    /**
     * @var LanguageRepositoryInterface
     */
    private $languageRepository;

    /**
     * @var LessonProviderInterface
     */
    private $lessonsProvider;

    /**
     * DailyMeditationProvider constructor.
     *
     * @param LanguageRepositoryInterface $languageRepository
     * @param LessonProviderInterface     $lessonsProvider
     */
    public function __construct(
        LanguageRepositoryInterface $languageRepository,
        LessonProviderInterface $lessonsProvider
    ) {
        $this->languageRepository = $languageRepository;
        $this->lessonsProvider = $lessonsProvider;
    }

    /**
     * {@inheritDoc}
     */
    public function updateDailyMeditation(DailyMeditation $dailyMeditation, array $data): void
    {
        if (isset($data["from"]) && is_string($data["from"])) {
            $dailyMeditation->setFrom(new DateTime($data["from"]));
        }

        if (isset($data["languages"])) {
            /** @var Language[] $languages */
            $languages = empty($data["languages"]) ? [] : $this->languageRepository->findBy([
                "id" => $data["languages"],
            ]);
            $dailyMeditation->setLanguages($languages);
        }

        if (array_key_exists("lesson", $data)) {
            if (empty($data["lesson"])) {
                $dailyMeditation->setLesson(null);
            } else {
                $lesson = $dailyMeditation->getLesson() ?? new LessonEntity();
                $this->lessonsProvider->updateLesson($lesson, $data["lesson"]);
                $dailyMeditation->setLesson($lesson);
            }
        }

        if (isset($data["name"]) && is_string($data["name"])) {
            $dailyMeditation->setName($data["name"]);
        }

        if (isset($data["subtitle"]) && is_string($data["subtitle"])) {
            $dailyMeditation->setSubtitle($data["subtitle"]);
        }

        if (isset($data["thumbnail"]) && is_string($data["thumbnail"])) {
            $dailyMeditation->setThumbnail($data["thumbnail"]);
        }

        if (isset($data["to"]) && is_string($data["to"])) {
            $dailyMeditation->setTo(new DateTime($data["to"]));
        }

        if (isset($data["visibility"]) && is_bool($data["visibility"])) {
            $dailyMeditation->setVisibility($data["visibility"]);
        }
    }
}
