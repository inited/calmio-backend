<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Provider;

use App\Model\Entity\DailyMeditation;
use Exception;

/**
 * Interface DailyMeditationProviderInterface
 * @package App\Model\Provider
 */
interface DailyMeditationProviderInterface
{
    /**
     * @param DailyMeditation $dailyMeditation
     * @param array           $data
     *
     * @throws Exception
     */
    public function updateDailyMeditation(DailyMeditation $dailyMeditation, array $data): void;
}
