<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Provider;

use App\Exceptions\EntityNotFoundException;
use App\Model\Entity\Course;
use App\Model\Entity\CourseLanguage;
use App\Model\Entity\Language;
use App\Model\Entity\LessonEntity;
use App\Model\Entity\Tag;
use App\Model\Repository\LanguageRepositoryInterface;
use App\Model\Repository\TagRepositoryInterface;

/**
 * Class CourseProvider
 * @package App\Model\Provider
 */
class CourseProvider implements CourseProviderInterface
{
    /**
     * @var LanguageRepositoryInterface
     */
    private $languageRepository;

    /**
     * @var LessonProviderInterface
     */
    private $lessonProvider;

    /**
     * @var TagRepositoryInterface
     */
    private $tagRepository;

    /**
     * CourseProvider constructor.
     *
     * @param LanguageRepositoryInterface $languageRepository
     * @param LessonProviderInterface     $lessonProvider
     * @param TagRepositoryInterface      $tagRepository
     */
    public function __construct(
        LanguageRepositoryInterface $languageRepository,
        LessonProviderInterface $lessonProvider,
        TagRepositoryInterface $tagRepository
    ) {
        $this->languageRepository = $languageRepository;
        $this->lessonProvider = $lessonProvider;
        $this->tagRepository = $tagRepository;
    }

    /**
     * @param string[] $languages
     */
    private function processLanguages(Course $course, array $languages): void
    {
        if (!$languages) {
            $course->setLanguages([]);
            return;
        }

        $courseLanguages = [];
        foreach ($languages as $record) {
            try {
                /** @var Language $language */
                $language = $this->languageRepository->find(strtolower($record));
            } catch (EntityNotFoundException $e) {
                continue;
            }

            if (!$courseLanguage = $course->getCourseLanguageByLanguage($language)) {
                $courseLanguage = new CourseLanguage();
                $courseLanguage->setLanguage($language);
            }
            $courseLanguages[] = $courseLanguage;
        }

        $course->setLanguages($courseLanguages);
    }

    /**
     * {@inheritDoc}
     */
    public function updateCourse(Course $course, array $data): void
    {
        if (isset($data["description"]) && is_string($data["description"])) {
            $course->setDescription($data["description"]);
        }

        if (isset($data["free"]) && is_bool($data["free"])) {
            $course->setFree($data["free"]);
        }

        if (array_key_exists("languages", $data)) {
            $this->processLanguages($course, $data["languages"]);
        }

        if (isset($data["lessons"])) {
            $lessons = [];

            if (Course::TYPE_MEDITATION === $course->getType() && 1 < count($data["lessons"])) {
                $firstLesson = reset($data["lessons"]);
                $data["lessons"] = [$firstLesson];
            }

            foreach ((array) $data["lessons"] as $record) {
                $lesson = null;
                if (array_key_exists("id", $record) && 0 < intval($record["id"])) {
                    $lesson = $course->getLessonById(intval($record["id"]));
                }

                if (!isset($lesson)) {
                    $lesson = new LessonEntity();
                }

                $this->lessonProvider->updateLesson($lesson, $record);
                $lessons[] = $lesson;
            }

            $course->setLessons($lessons);
        }

        if (isset($data["name"]) && is_string($data["name"])) {
            $course->setName($data["name"]);
        }

        if (isset($data["subtitle"]) && is_string($data["subtitle"])) {
            $course->setSubTitle($data["subtitle"]);
        }

        if (isset($data["tags"])) {
            /** @var Tag[] $tags */
            $tags = empty($data["tags"]) ? [] : $this->tagRepository->findBy([
                "id" => $data["tags"],
            ]);
            $course->setTags($tags);
        }

        if (isset($data["thumbnail"]) && is_string($data["thumbnail"])) {
            $course->setThumbnail($data["thumbnail"]);
        }

        if (isset($data["titlePhoto"]) && is_string($data["titlePhoto"])) {
            $course->setTitlePhoto($data["titlePhoto"]);
        }
    }
}
