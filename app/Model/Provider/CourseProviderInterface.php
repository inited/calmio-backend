<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Provider;

use App\Model\Entity\Course;

/**
 * Interface CourseProviderInterface
 * @package App\Model\Provider
 */
interface CourseProviderInterface
{
    /**
     * @param Course $course
     * @param array $data
     */
    public function updateCourse(Course $course, array $data): void;
}
