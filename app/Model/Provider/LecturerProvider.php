<?php

declare(strict_types=1);

namespace App\Model\Provider;

use App\Model\Entity\Lecturer;

class LecturerProvider implements LecturerProviderInterface
{
    public function updateLecturer(Lecturer $lecturer, array $data): void
    {
        if (isset($data['gender']) && in_array($data['gender'], [Lecturer::GENDER_MAN, Lecturer::GENDER_WOMAN])) {
            $lecturer->setGender(strtolower($data['gender']));
        }

        if (isset($data['name'])) {
            $lecturer->setName($data['name']);
        }

        if (array_key_exists('previewUrl', $data)) {
            $lecturer->setPreviewUrl($data['previewUrl']);
        }
    }
}
