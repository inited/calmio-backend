<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Provider;

use App\Model\Entity\LessonEntity;

/**
 * Interface LessonProviderInterface
 * @package App\Model\Provider
 */
interface LessonProviderInterface
{
    /**
     * @param LessonEntity $lesson
     * @param array        $data
     */
    public function updateLesson(LessonEntity $lesson, array $data): void;
}
