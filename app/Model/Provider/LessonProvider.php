<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Provider;

use App\Model\Entity\LessonEntity;
use App\Model\Entity\LessonIntroEntity;
use App\Model\Entity\LessonSourceEntity;
use App\Model\Entity\Voice;
use App\Model\Repository\LecturerRepositoryInterface;

/**
 * Class LessonProvider
 * @package App\Model\Provider
 */
class LessonProvider implements LessonProviderInterface
{
    /** @var LecturerRepositoryInterface */
    private $lecturerRepository;

    public function __construct(LecturerRepositoryInterface $lecturerRepository)
    {
        $this->lecturerRepository = $lecturerRepository;
    }

    /**
     * @param LessonSourceEntity $entity
     * @param mixed              $lecturer
     */
    private function processLecturer(LessonSourceEntity $entity, $lecturer): void
    {
        if (empty($lecturer)) {
            $entity->setLecturer(null);
        }

        $lecturer = $this->lecturerRepository->findOneBy([
            'id' => (int) $lecturer,
            'deleted' => false,
        ]);

        if ($lecturer) {
            $entity->setLecturer($lecturer);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function updateLesson(LessonEntity $lesson, array $data): void
    {
        if (isset($data["description"]) && is_string($data["description"])) {
            $lesson->setDescription($data["description"]);
        }

        if (array_key_exists("intro", $data)) {
            if (empty($data["intro"])) {
                $lesson->setIntro(null);
            } else {
                $intro = $data["intro"];
                $introEntity = $lesson->getIntro() ?? new LessonIntroEntity();
                $introEntity->setText(($intro["text"] ?? null));
                $introEntity->setVideo(($intro["video"] ?? null));
                $lesson->setIntro($introEntity);
            }
        }

        if (isset($data["name"]) && is_string($data["name"])) {
            $lesson->setName($data["name"]);
        }

        if (isset($data["order"])) {
            $lesson->setOrder(intval($data["order"]));
        }

        if (array_key_exists("source", $data)) {
            $source = [];

            foreach ((array) $data["source"] as $record) {
                $voice = isset($record["voice"]) && in_array($record["voice"], [Voice::MAN, Voice::WOMAN])
                    ? strtolower($record["voice"])
                    : Voice::MAN;

                $sourceEntity = new LessonSourceEntity();
                $sourceEntity->setSrc(($record["src"] ?? null));
                $sourceEntity->setTitle(($record["title"] ?? null));
                $sourceEntity->setVoice($voice);

                if (array_key_exists("lecturer", $record)) {
                    $this->processLecturer($sourceEntity, $record["lecturer"]);
                }

                $source[] = $sourceEntity;
            }

            $lesson->setSource($source);
        }

        if (isset($data["type"]) && is_string($data["type"])) {
            $lesson->setType($data["type"]);
        }
    }
}
