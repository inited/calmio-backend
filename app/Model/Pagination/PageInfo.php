<?php

declare(strict_types=1);

namespace App\Model\Pagination;

class PageInfo
{
    /** @var int */
    private $limit;

    /** @var int */
    private $offset;

    /** @var bool */
    private $hasNextPage;

    public function __construct(
        bool $hasNextPage,
        int $limit,
        int $offset
    ) {
        $this->hasNextPage = $hasNextPage;
        $this->limit = $limit;
        $this->offset = $offset;
    }

    public function transform(): array
    {
        return [
            'limit' => $this->limit,
            'offset' => $this->offset,
            'hasNextPage' => $this->hasNextPage,
        ];
    }
}
