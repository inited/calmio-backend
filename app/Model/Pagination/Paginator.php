<?php

declare(strict_types=1);

namespace App\Model\Pagination;

class Paginator
{
    public const LIMIT = 50;
    public const OFFSET = 0;

    /** @var int */
    private $totalCount;

    /** @var PageInfo */
    private $pageInfo;

    /** @var mixed[] */
    private $data = [];

    public function __construct(array $data, int $totalCount, int $limit, int $offset)
    {
        $this->data = $data;
        $this->totalCount = $totalCount;
        $this->pageInfo = new PageInfo(($offset + $limit) < $this->totalCount, $limit, $offset);
    }

    /**
     * @return array<string, mixed>
     */
    public function transform(): array
    {
        return [
            'data' => $this->data,
            'totalCount' => $this->totalCount,
            'pageInfo' => $this->pageInfo->transform(),
        ];
    }
}
