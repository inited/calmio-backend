<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 21. 01. 2020
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Role
 * @package App\Model\Entity
 * @ORM\Entity(readOnly=true)
 * @ORM\Table(name="system__role")
 */
class Role
{
    const TYPE_ADMIN = "ADMIN";
    const TYPE_TEST = "TEST";
    const TYPE_USER = "USER";

    /**
     * @var string
     * @ORM\Column(name="id", type="string")
     * @ORM\Id()
     */
    private $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

}
