<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11. 08. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Course
 * @package App\Model\Entity
 * @ORM\Entity()
 * @ORM\Table(name="course__course", indexes={
 *     @ORM\Index(columns={"type"}),
 * })
 */
class Course
{
    const TYPE_COURSE = 'course';
    const TYPE_MEDITATION = 'meditation';

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="description", type="text")
     */
    private $description = '';

    /**
     * @var bool
     * @ORM\Column(name="free", type="boolean")
     */
    private $free = true;

    /**
     * @var ArrayCollection|CourseLanguage[]
     * @ORM\OneToMany(targetEntity="App\Model\Entity\CourseLanguage", cascade={"persist","merge"}, mappedBy="course", indexBy="id", orphanRemoval=true)
     */
    private $languages;

    /**
     * @var ArrayCollection|LessonEntity[]
     * @ORM\ManyToMany(targetEntity="App\Model\Entity\LessonEntity", cascade={"persist","merge"})
     * @ORM\JoinTable(name="course__lesson",
     *      joinColumns={@ORM\JoinColumn(onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(onDelete="CASCADE")})
     */
    private $lessons;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name = '';

    /**
     * @var string
     * @ORM\Column(name="sub_title", type="string")
     */
    private $subTitle = '';

    /**
     * @var ArrayCollection|Tag[]
     * @ORM\ManyToMany(targetEntity="App\Model\Entity\Tag")
     * @ORM\JoinTable(name="course__tag",
     *      joinColumns={@ORM\JoinColumn(onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(onDelete="CASCADE")})
     */
    private $tags;

    /**
     * @var string|null
     * @ORM\Column(name="title_photo", type="string", nullable=true)
     */
    private $titlePhoto;

    /**
     * @var string|null
     * @ORM\Column(name="thumbnail", type="string", nullable=true)
     */
    private $thumbnail;

    /**
     * @var string
     * @ORM\Column()
     */
    private $type = self::TYPE_COURSE;

    /**
     * @var bool
     * @ORM\Column(name="use_as_next_daily_meditation", type="boolean")
     */
    private $useAsNextDailyMeditation = false;

    /**
     * @var ArrayCollection|CourseUser[]
     * @ORM\OneToMany(targetEntity="App\Model\Entity\CourseUser", mappedBy="course", orphanRemoval=true,
     *     cascade={"persist","merge"}, fetch="EXTRA_LAZY")
     */
    private $users;

    public function __construct()
    {
        $this->languages = new ArrayCollection();
        $this->lessons = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function __toArray(): array
    {
        return [
            "id" => (string) $this->id,
            "description" => $this->description,
            "free" => $this->free,
            "tomorrowPreferredDailyMeditation" => $this->useAsNextDailyMeditation,
            "isTodayDailyMeditation" => collect($this->languages)
                ->filter(function (CourseLanguage $courseLanguage) {
                    $language = $courseLanguage->getLanguage()->getId();
                    return $language === Language::TYPE_CS && $courseLanguage->isDailyMeditation();
                })
                ->isNotEmpty(),
            "languages" => collect($this->languages->toArray())
                ->map(function (CourseLanguage $courseLanguage) {
                    return $courseLanguage->getLanguage()->getId();
                })
                ->values()
                ->all(),
            "lessons" => array_map(function (LessonEntity $lesson) {
                return $lesson->__toArray();
            }, $this->getLessons()),
            "name" => $this->name,
            "subtitle" => $this->subTitle,
            "tags" => array_map(function (Tag $tag) {
                return $tag->getId();
            }, $this->getTags()),
            "titlePhoto" => $this->titlePhoto ?? "",
            "thumbnail" => $this->thumbnail ?? "",
        ];
    }

    /**
     * @return array
     */
    public function __toArrayShort(): array
    {
        return [
            "id" => (string) $this->id,
            "description" => $this->description,
            "free" => $this->free,
            "languages" => collect($this->languages->toArray())
                ->map(function (CourseLanguage $courseLanguage) {
                    return $courseLanguage->getLanguage()->getId();
                })
                ->values()
                ->all(),
            "lessonsTotal" => $this->lessons->count(),
            "name" => $this->name,
            "subtitle" => $this->subTitle,
            "tags" => array_map(function (Tag $tag) {
                return $tag->getId();
            }, $this->getTags()),
            "titlePhoto" => $this->titlePhoto ?? "",
            "thumbnail" => $this->thumbnail ?? "",
        ];
    }

    public function getCourseLanguageByLanguage(Language $language): ?CourseLanguage
    {
        foreach ($this->languages as $courseLanguage) {
            if ($language === $courseLanguage->getLanguage()) {
                return $courseLanguage;
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Course
     */
    public function setDescription(string $description): Course
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return LessonEntity|null
     */
    public function getLessonById(int $id): ?LessonEntity
    {
        foreach ($this->lessons as $lesson) {
            if ($id === $lesson->getId()) {
                return $lesson;
            }
        }
        return null;
    }

    /**
     * @return LessonEntity[]
     */
    public function getLessons(): array
    {
        $lessons = $this->lessons->toArray();

        usort($lessons, function (LessonEntity $a, LessonEntity $b) {
            return $a->getOrder() <=> $b->getOrder();
        });

        return $lessons;
    }

    /**
     * @param LessonEntity[] $lessons
     *
     * @return Course
     */
    public function setLessons(array $lessons): Course
    {
        foreach ($this->lessons as $lesson) {
            if (!in_array($lesson, $lessons, true)) {
                $this->lessons->removeElement($lesson);
            }
        }

        foreach ($lessons as $lesson) {
            if (!$this->lessons->contains($lesson)) {
                $this->lessons->add($lesson);
            }
        }

        return $this;
    }

    public function getLessonsCount(): int
    {
        return $this->lessons->count();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Course
     */
    public function setName(string $name): Course
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubTitle(): string
    {
        return $this->subTitle;
    }

    /**
     * @param string $subTitle
     *
     * @return Course
     */
    public function setSubTitle(string $subTitle): Course
    {
        $this->subTitle = $subTitle;
        return $this;
    }

    /**
     * @return Tag[]
     */
    public function getTags()
    {
        return $this->tags->toArray();
    }

    /**
     * @param Tag[] $tags
     *
     * @return Course
     */
    public function setTags(array $tags): Course
    {
        foreach ($this->tags as $tag) {
            if (!in_array($tag, $tags, true)) {
                $this->tags->removeElement($tag);
            }
        }

        foreach ($tags as $tag) {
            if (!$this->tags->contains($tag)) {
                $this->tags->add($tag);
            }
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    /**
     * @param string|null $thumbnail
     *
     * @return Course
     */
    public function setThumbnail(?string $thumbnail): Course
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitlePhoto(): ?string
    {
        return $this->titlePhoto;
    }

    /**
     * @param string|null $titlePhoto
     *
     * @return Course
     */
    public function setTitlePhoto(?string $titlePhoto): Course
    {
        $this->titlePhoto = $titlePhoto;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Course
     */
    public function setType(string $type): Course
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return UserEntity[]
     */
    public function getUsers()
    {
        return $this->users->toArray();
    }

    /**
     * @param LessonEntity $lesson
     *
     * @return bool
     */
    public function hasLesson(LessonEntity $lesson): bool
    {
        foreach ($this->lessons as $entity) {
            if ($lesson === $entity) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isFree(): bool
    {
        return $this->free;
    }

    /**
     * @param bool $free
     *
     * @return Course
     */
    public function setFree(bool $free): Course
    {
        $this->free = $free;
        return $this;
    }

    public function isUseAsNextDailyMeditation(): bool
    {
        return $this->useAsNextDailyMeditation;
    }

    /**
     * @param CourseLanguage[] $languages
     *
     * @return Course
     */
    public function setLanguages(array $languages): Course
    {
        foreach ($this->languages as $language) {
            if (!in_array($language, $languages, true)) {
                $this->languages->removeElement($language);
            }
        }

        foreach ($languages as $language) {
            if (!$this->languages->contains($language)) {
                $language->setCourse($this);
                $this->languages->add($language);
            }
        }

        return $this;
    }

    public function useAsTomorrowDailyMeditation(): Course
    {
        if (self::TYPE_MEDITATION === $this->type) {
            $this->useAsNextDailyMeditation = true;
        }

        return $this;
    }
}

