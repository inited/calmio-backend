<?php

namespace App\Model\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CourseUserLesson
 * @package App\Model\Entity
 * @ORM\Entity()
 * @ORM\Table(name="course__user_lesson")
 */
class CourseUserLesson
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var CourseUser
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\CourseUser", inversedBy="lessons")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $courseUser;

    /**
     * @var bool
     * @ORM\Column(name="current_session_completed", type="boolean")
     */
    private $currentSessionCompleted = false;

    /**
     * @var int
     * @ORM\Column(name="current_session_time", type="integer")
     */
    private $currentSessionTime = 0;

    /**
     * @var DateTime
     * @ORM\Column(name="finished", type="datetime")
     */
    private $finished;

    /**
     * @var Language
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Language")
     * @ORM\JoinColumn()
     */
    private $language;

    /**
     * @var Lecturer|null
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Lecturer")
     * @ORM\JoinColumn()
     */
    private $lecturer;

    /**
     * @var LessonEntity
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\LessonEntity")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $lesson;

    /**
     * @var int
     * @ORM\Column(name="lesson_duration_seconds", type="integer")
     */
    private $lessonDurationSeconds = 0;

    /**
     * @var int
     * @ORM\Column(name="time", type="integer")
     */
    private $time = 0;

    /**
     * @var int
     * @ORM\Column(name="unfinished_time", type="integer")
     */
    private $unfinishedTime = 0;

    /**
     * @var bool
     * @ORM\Column(name="completed", type="boolean")
     */
    private $completed;

    /**
     * @var string
     * @ORM\Column(name="voice")
     */
    private $voice = Voice::MAN;

    /**
     * @return CourseUser
     */
    public function getCourseUser(): CourseUser
    {
        return $this->courseUser;
    }

    /**
     * @param CourseUser $courseUser
     *
     * @return CourseUserLesson
     */
    public function setCourseUser(CourseUser $courseUser): CourseUserLesson
    {
        $this->courseUser = $courseUser;
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentSessionTime(): int
    {
        return $this->currentSessionTime;
    }

    /**
     * @param int $currentSessionTime
     *
     * @return CourseUserLesson
     */
    public function setCurrentSessionTime(int $currentSessionTime): CourseUserLesson
    {
        $this->currentSessionTime = $currentSessionTime;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getFinished(): DateTime
    {
        return $this->finished;
    }

    /**
     * @param DateTime $finished
     *
     * @return CourseUserLesson
     */
    public function setFinished(DateTime $finished): CourseUserLesson
    {
        $this->finished = $finished;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /** @return Language */
    public function getLanguage(): Language
    {
        return $this->language;
    }

    /** @return CourseUserLesson */
    public function setLanguage(Language $language): CourseUserLesson
    {
        $this->language = $language;
        return $this;
    }

    public function getLecturer(): ?Lecturer
    {
        return $this->lecturer;
    }

    public function setLecturer(?Lecturer $lecturer): CourseUserLesson
    {
        $this->lecturer = $lecturer;
        return $this;
    }

    /**
     * @return LessonEntity
     */
    public function getLesson(): LessonEntity
    {
        return $this->lesson;
    }

    /**
     * @param LessonEntity $lesson
     *
     * @return CourseUserLesson
     */
    public function setLesson(LessonEntity $lesson): CourseUserLesson
    {
        $this->lesson = $lesson;
        return $this;
    }

    /**
     * @return int
     */
    public function getLessonDurationSeconds(): int
    {
        return $this->lessonDurationSeconds;
    }

    /**
     * @param int $lessonDurationSeconds
     *
     * @return CourseUserLesson
     */
    public function setLessonDurationSeconds(int $lessonDurationSeconds): CourseUserLesson
    {
        $this->lessonDurationSeconds = $lessonDurationSeconds;
        return $this;
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * @param int $time
     *
     * @return CourseUserLesson
     */
    public function setTime(int $time): CourseUserLesson
    {
        $this->time = $time;
        return $this;
    }

    /**
     * @return int
     */
    public function getUnfinishedTime(): int
    {
        return $this->unfinishedTime;
    }

    /**
     * @param int $unfinishedTime
     *
     * @return CourseUserLesson
     */
    public function setUnfinishedTime(int $unfinishedTime): CourseUserLesson
    {
        $this->unfinishedTime = $unfinishedTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getVoice(): string
    {
        return $this->voice;
    }

    /**
     * @param string $voice
     *
     * @return CourseUserLesson
     */
    public function setVoice(string $voice): CourseUserLesson
    {
        $this->voice = $voice;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->completed;
    }

    /**
     * @param bool $completed
     *
     * @return CourseUserLesson
     */
    public function setCompleted(bool $completed): CourseUserLesson
    {
        $this->completed = $completed;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCurrentSessionCompleted(): bool
    {
        return $this->currentSessionCompleted;
    }

    /**
     * @param bool $currentSessionCompleted
     *
     * @return CourseUserLesson
     */
    public function setCurrentSessionCompleted(bool $currentSessionCompleted): CourseUserLesson
    {
        $this->currentSessionCompleted = $currentSessionCompleted;
        return $this;
    }

}
