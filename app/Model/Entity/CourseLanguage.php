<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="course__course_language")
 */
class CourseLanguage
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var Course
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Course", inversedBy="languages")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $course;

    /**
     * @var Language
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Language")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $language;

    /**
     * @var boolean
     * @ORM\Column(name="daily_meditation", type="boolean")
     */
    private $dailyMeditation = false;

    public function getCourse(): Course
    {
        return $this->course;
    }

    public function setCourse(Course $course): CourseLanguage
    {
        $this->course = $course;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function setLanguage(Language $language): CourseLanguage
    {
        $this->language = $language;
        return $this;
    }

    public function isDailyMeditation(): bool
    {
        return $this->dailyMeditation;
    }

    public function setDailyMeditation(bool $dailyMeditation): CourseLanguage
    {
        $this->dailyMeditation = $dailyMeditation;
        return $this;
    }
}
