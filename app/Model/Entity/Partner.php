<?php

declare(strict_types=1);

namespace App\Model\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="partner__partner")
 */
class Partner implements SerializableInterface
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="address")
     */
    private $address = '';

    /**
     * @var DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var bool
     * @ORM\Column(name="deleted", type="boolean")
     */
    private $deleted = false;

    /**
     * @var string
     * @ORM\Column(name="name")
     */
    private $name = '';

    /**
     * @var PromoCode[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Model\Entity\PromoCode", mappedBy="partner", cascade={"persist","merge"}, orphanRemoval=true)
     */
    private $promoCodes;

    /**
     * @var integer
     * @ORM\Column(name="purchases", type="integer")
     */
    private $purchases = 0;

    /**
     * @var int
     * @ORM\Column(name="used_codes", type="integer")
     */
    private $usedCodes = 0;

    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->promoCodes = new ArrayCollection();
    }

    public function __toArray(): array
    {
        $promoCode = $this->getValidPromoCode();

        return [
            'id' => $this->id,
            'address' => $this->address,
            'name' => $this->name,
            'created' => $this->createdAt->getTimestamp(),
            'code' => !$promoCode ? null : [
                'name' => $promoCode->getId(),
                'created' => $promoCode->getCreatedAt()->getTimestamp(),
                'value' => $promoCode->getValue(),
                'usage' => [
                    'total' => $promoCode->getUsed(),
                    'conversions' => $promoCode->getConversions(),
                ],
            ],
            'usage' => [
                'total' => $this->usedCodes,
                'conversions' => $this->getConversions(),
            ],
        ];
    }

    public function addPurchases(int $increment = 1): Partner
    {
        $this->purchases += $increment;
        return $this;
    }

    public function addUsage(int $increment = 1): Partner
    {
        $this->usedCodes += $increment;
        return $this;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): Partner
    {
        $this->address = $address;
        return $this;
    }

    public function getConversions(): float
    {
        return $this->purchases ? round($this->purchases / $this->usedCodes, 2) : 0;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Partner
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return PromoCode[]|ArrayCollection
     */
    public function getPromoCodes()
    {
        return $this->promoCodes;
    }

    public function getPromoCodeByCode(string $code): ?PromoCode
    {
        foreach ($this->promoCodes as $promoCode) {
            if ($code === $promoCode->getId()) {
                return $promoCode;
            }
        }

        return null;
    }

    public function getUsedCodes(): int
    {
        return $this->usedCodes;
    }

    public function getValidPromoCode(): ?PromoCode
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->isNull('deletedAt'))
            ->setMaxResults(1);

        if ($promoCode = $this->promoCodes->matching($criteria)->first()) {
            return $promoCode;
        }

        return null;
    }

    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): Partner
    {
        $this->deleted = $deleted;
        return $this;
    }

}
