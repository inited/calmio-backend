<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 14.06.2020
 */

declare(strict_types=1);

namespace App\Model\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class PushNotification
 * @package App\Model\Entity
 * @ORM\Entity()
 * @ORM\Table(name="system__push_notification")
 */
class PushNotification
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     * @ORM\Column(name="message",type="text")
     */
    private $message = '';

    /**
     * @var bool
     * @ORM\Column(name="test", type="boolean")
     */
    private $test = true;

    /**
     * @var string
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @var UserEntity
     * @ORM\ManyToOne(targetEntity="UserEntity")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $user;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @return array<string, mixed>
     */
    public function __toArray(): array
    {
        return [
            "id" => $this->id,
            "createdAt" => $this->createdAt->getTimestamp(),
            "createdBy" => [
                "id" => $this->user->getId(),
                "name" => $this->user->getName(),
                "surname" => $this->user->getSurname(),
            ],
            "message" => $this->message,
            "test" => $this->test,
            "title" => $this->title,
        ];
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     *
     * @return PushNotification
     */
    public function setCreatedAt(DateTime $createdAt): PushNotification
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return PushNotification
     */
    public function setId(int $id): PushNotification
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return PushNotification
     */
    public function setMessage(string $message): PushNotification
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return PushNotification
     */
    public function setTitle(string $title): PushNotification
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return UserEntity
     */
    public function getUser(): UserEntity
    {
        return $this->user;
    }

    /**
     * @param UserEntity $user
     *
     * @return PushNotification
     */
    public function setUser(UserEntity $user): PushNotification
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTest(): bool
    {
        return $this->test;
    }

    /**
     * @param bool $test
     *
     * @return PushNotification
     */
    public function setTest(bool $test): PushNotification
    {
        $this->test = $test;
        return $this;
    }

}
