<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11.03.2020
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Version
 * @package App\Model\Entity
 * @ORM\Entity()
 * @ORM\Table(name="version")
 */
class Version
{
    const APP_VERSION_1_1_16 = 1116;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(name="force_update_version", nullable=true)
     */
    private $forceUpdateVersion;

    /**
     * @var string
     * @ORM\Column(name="version")
     */
    private $version = '';

    public function getForceUpdateVersion(): ?string
    {
        return $this->forceUpdateVersion;
    }

    /**
     * @param string $forceUpdateVersion
     */
    public function setForceUpdateVersion(string $forceUpdateVersion): Version
    {
        $this->forceUpdateVersion = $forceUpdateVersion;

        return $this;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @param string $version
     *
     * @return Version
     */
    public function setVersion(string $version): Version
    {
        $this->version = $version;

        return $this;
    }
}
