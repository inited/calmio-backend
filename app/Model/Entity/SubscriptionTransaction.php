<?php
namespace App\Model\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @package App\Model\Entity
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="subscription__transaction", indexes={
 *     @ORM\Index(columns={"valid_to"}),
 *     @ORM\Index(columns={"product_id"})
 * })
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class SubscriptionTransaction
{
    use Identifier;
    use Timestampable;

    /**
     * @var bool
     * @ORM\Column(name="auto_renewing", type="boolean")
     */
    private $autoRenewing = false;

    /**
     * @var string|null
     * @ORM\Column(name="cancel_reason", nullable=true)
     */
    private $cancelReason;

    /**
     * @var string|null
     * @ORM\Column(name="environment", nullable=true)
     */
    private $environment;

    /**
     * @var string|null
     * @ORM\Column(name="order_id", nullable=true)
     */
    private $orderId;

    /**
     * @var string|null
     * @ORM\Column(name="original_transaction_id", nullable=true)
     */
    private $originalTransactionId;

    /**
     * @var integer|null
     * @ORM\Column(name="payment_state", type="integer", nullable=true)
     */
    private $paymentState;

    /**
     * @var int
     * @ORM\Column(name="price_amount_micros", type="integer")
     */
    private $priceAmountMicros;

    /**
     * @var string|null
     * @ORM\Column(name="price_currency_code", nullable=true)
     */
    private $priceCurrencyCode;

    /**
     * @var string
     * @ORM\Column(name="product_id")
     */
    private $productId;

    /**
     * @var DateTime
     * @ORM\Column(name="purchased_at", type="datetime")
     */
    private $purchasedAt;

    /**
     * @var DateTime|null
     * @ORM\Column(name="purchased_at_calculated", type="datetime", nullable=true)
     */
    private $purchasedAtCalculated;

    /**
     * @var string|null
     * @ORM\Column(name="purchase_type", nullable=true)
     */
    private $purchaseType;

    /**
     * @var integer|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $renewals;

    /**
     * @var ArrayCollection|Subscription[]
     * @ORM\ManyToMany(targetEntity="App\Model\Entity\Subscription", inversedBy="transactions")
     * @ORM\JoinTable(name="subscription__transaction_transaction")
     */
    private $subscriptions;

    /**
     * @var string|null
     * @ORM\Column(name="transaction_id", nullable=true)
     */
    private $transactionId;

    /**
     * @var string
     * @ORM\Column()
     */
    private $type;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $upgraded = false;

    /**
     * @var DateTime
     * @ORM\Column(name="valid_to", type="datetime")
     */
    private $validTo;

    public function __construct()
    {
        $this->subscriptions = new ArrayCollection();
    }

    public function addSubscription(Subscription $subscription): self
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions->add($subscription);
        }

        return $this;
    }

    public function getCancelReason(): ?string
    {
        return $this->cancelReason;
    }

    public function setCancelReason(?string $cancelReason): SubscriptionTransaction
    {
        $this->cancelReason = $cancelReason;
        return $this;
    }

    public function getEnvironment(): ?string
    {
        return $this->environment;
    }

    public function setEnvironment(?string $environment): SubscriptionTransaction
    {
        $this->environment = $environment;
        return $this;
    }

    public function getOrderId(): ?string
    {
        return $this->orderId;
    }

    public function setOrderId(?string $orderId): SubscriptionTransaction
    {
        $this->orderId = $orderId;
        return $this;
    }

    public function getOriginalTransactionId(): ?string
    {
        return $this->originalTransactionId;
    }

    public function setOriginalTransactionId(?string $originalTransactionId): SubscriptionTransaction
    {
        $this->originalTransactionId = $originalTransactionId;
        return $this;
    }

    public function getPaymentState(): ?int
    {
        return $this->paymentState;
    }

    public function setPaymentState(?int $paymentState): SubscriptionTransaction
    {
        $this->paymentState = $paymentState;
        return $this;
    }

    public function getPriceAmountMicros(): int
    {
        return $this->priceAmountMicros;
    }

    public function setPriceAmountMicros(int $priceAmountMicros): SubscriptionTransaction
    {
        $this->priceAmountMicros = $priceAmountMicros;
        return $this;
    }

    public function getPriceCurrencyCode(): ?string
    {
        return $this->priceCurrencyCode;
    }

    public function setPriceCurrencyCode(?string $priceCurrencyCode): SubscriptionTransaction
    {
        $this->priceCurrencyCode = $priceCurrencyCode;
        return $this;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function setProductId(string $productId): SubscriptionTransaction
    {
        $this->productId = $productId;

        return $this;
    }

    public function getPurchaseType(): ?string
    {
        return $this->purchaseType;
    }

    public function setPurchaseType(?string $purchaseType): SubscriptionTransaction
    {
        $this->purchaseType = $purchaseType;
        return $this;
    }

    public function getPurchasedAt(): DateTime
    {
        return $this->purchasedAt;
    }

    public function setPurchasedAt(DateTime $purchasedAt): SubscriptionTransaction
    {
        $this->purchasedAt = $purchasedAt;
        return $this;
    }

    public function getPurchasedAtCalculated(): DateTime
    {
        return $this->purchasedAtCalculated;
    }

    public function setPurchasedAtCalculated(DateTime $purchasedAtCalculated): SubscriptionTransaction
    {
        $this->purchasedAtCalculated = $purchasedAtCalculated;
        return $this;
    }

    public function getRenewals(): ?int
    {
        return $this->renewals;
    }

    public function setRenewals(?int $renewals): SubscriptionTransaction
    {
        $this->renewals = $renewals;
        return $this;
    }

    public function getTransactionId(): ?string
    {
        return $this->transactionId;
    }

    public function setTransactionId(?string $transactionId): SubscriptionTransaction
    {
        $this->transactionId = $transactionId;
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): SubscriptionTransaction
    {
        $this->type = $type;
        return $this;
    }

    public function getValidTo(): DateTime
    {
        return $this->validTo;
    }

    public function setValidTo(DateTime $validTo): SubscriptionTransaction
    {
        $this->validTo = $validTo;
        return $this;
    }

    public function isAutoRenewing(): bool
    {
        return $this->autoRenewing;
    }

    public function setAutoRenewing(bool $autoRenewing): SubscriptionTransaction
    {
        $this->autoRenewing = $autoRenewing;
        return $this;
    }

    public function isUpgraded(): bool
    {
        return $this->upgraded;
    }

    public function setUpgraded(bool $upgraded): SubscriptionTransaction
    {
        $this->upgraded = $upgraded;
        return $this;
    }
}
