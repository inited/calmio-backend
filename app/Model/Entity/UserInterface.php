<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 22. 05. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

/**
 * Interface UserInterface
 * @package App\Model\Entity
 */
interface UserInterface
{
    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getSurname(): string;

    public function getLanguageCode(): string;

}
