<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11.03.2020
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Language
 * @package App\Model\Entity
 * @ORM\Entity(readOnly=true)
 * @ORM\Table(name="system__language")
 */
class Language
{
    const TYPE_CS = "cs";
    const TYPE_EN = "en";
    const TYPE_HU = "hu";
    const TYPE_PL = "pl";
    const TYPE_SK = "sk";

    /**
     * @return string[]
     */
    public static function getAvailableLanguages(): array
    {
        return [
            self::TYPE_CS,
            self::TYPE_EN,
            self::TYPE_HU,
            self::TYPE_PL,
            self::TYPE_SK,
        ];
    }

    /**
     * @var string
     * @ORM\Column(name="id", type="string")
     * @ORM\Id()
     */
    private $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

}
