<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;

/**
 * Class FileEntity
 * @package App\Model\Entity
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="file__file")
 * @ORM\Entity()
 */
class FileEntity
{

    /**
     * @var string
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var DateTime
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var bool
     * @ORM\Column(name="has_thumbnail", type="boolean")
     */
    private $hasThumbnail = false;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="original_name", type="string")
     */
    private $originalName;

    /**
     * @var integer
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * @var string
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    /**
     * FileEntity constructor.
     *
     * @param mixed $name
     * @param mixed $originalName
     * @param mixed $size
     * @param mixed $type
     *
     * @throws Exception
     */
    public function __construct($name, $originalName, $size, $type)
    {
        $this->id = Uuid::uuid4()->toString();
        $this->created = new DateTime();
        $this->name = $name;
        $this->originalName = $originalName;
        $this->size = $size;
        $this->type = $type;
    }

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function hasThumbnail(): bool
    {
        return $this->hasThumbnail;
    }

    /**
     * @param bool $hasThumbnail
     *
     * @return FileEntity
     */
    public function setHasThumbnail(bool $hasThumbnail): FileEntity
    {
        $this->hasThumbnail = $hasThumbnail;
        return $this;
    }

    /**
     * @return bool
     */
    public function isImage(): bool
    {
        return in_array($this->type, [
            "image/gif",
            "image/jpeg",
            "image/png",
        ]);
    }

}
