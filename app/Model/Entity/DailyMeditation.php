<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 05. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class DailyMeditation
 * @package App\Model\Entity
 * @ORM\Entity()
 * @ORM\Table(name="daily_meditation__daily_meditation")
 */
class DailyMeditation
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var DateTime
     * @ORM\Column(name="`from`", type="date")
     */
    private $from;

    /**
     * @var ArrayCollection|Language[]
     * @ORM\ManyToMany(targetEntity="App\Model\Entity\Language")
     * @ORM\JoinTable(name="daily_meditation__daily_meditation_language",
     *      joinColumns={@ORM\JoinColumn(onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(onDelete="CASCADE")})
     */
    private $languages;

    /**
     * @var null|LessonEntity
     * @ORM\OneToOne(targetEntity="App\Model\Entity\LessonEntity", cascade={"persist", "merge"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $lesson;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name = '';

    /**
     * @var string
     * @ORM\Column(name="subtitle", type="string")
     */
    private $subtitle = '';

    /**
     * @var string
     * @ORM\Column(name="thumbnail", type="string")
     */
    private $thumbnail = '';

    /**
     * @var DateTime
     * @ORM\Column(name="`to`", type="date")
     */
    private $to;

    /**
     * @var boolean
     * @ORM\Column(name="visibility", type="boolean")
     */
    private $visibility = true;

    /**
     * DailyMeditation constructor.
     */
    public function __construct()
    {
        $this->languages = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function __toArray(): array
    {
        return [
            "id" => (string) $this->id,
            "thumbnail" => $this->thumbnail,
            "name" => $this->name,
            "subtitle" => $this->subtitle,
            "lesson" => null === $this->lesson ? null : $this->lesson->__toArray(),
            "visibility" => $this->visibility,
            "from" => $this->from->format("Y-m-d"),
            "to" => $this->to->format("Y-m-d"),
            "languages" => array_map(function (Language $language) {
                return $language->getId();
            }, $this->languages->toArray()),
        ];
    }

    /**
     * @return DateTime
     */
    public function getFrom(): DateTime
    {
        return $this->from;
    }

    /**
     * @param DateTime $from
     *
     * @return DailyMeditation
     */
    public function setFrom(DateTime $from): DailyMeditation
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return LessonEntity|null
     */
    public function getLesson(): ?LessonEntity
    {
        return $this->lesson;
    }

    /**
     * @param LessonEntity|null $lesson
     *
     * @return DailyMeditation
     */
    public function setLesson(?LessonEntity $lesson): DailyMeditation
    {
        $this->lesson = $lesson;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return DailyMeditation
     */
    public function setName(string $name): DailyMeditation
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubtitle(): string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     *
     * @return DailyMeditation
     */
    public function setSubtitle(string $subtitle): DailyMeditation
    {
        $this->subtitle = $subtitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getThumbnail(): string
    {
        return $this->thumbnail;
    }

    /**
     * @param string $thumbnail
     *
     * @return DailyMeditation
     */
    public function setThumbnail(string $thumbnail): DailyMeditation
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTo(): DateTime
    {
        return $this->to;
    }

    /**
     * @param DateTime $to
     *
     * @return DailyMeditation
     */
    public function setTo(DateTime $to): DailyMeditation
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return bool
     */
    public function isVisibility(): bool
    {
        return $this->visibility;
    }

    /**
     * @param bool $visibility
     *
     * @return DailyMeditation
     */
    public function setVisibility(bool $visibility): DailyMeditation
    {
        $this->visibility = $visibility;
        return $this;
    }

    /**
     * @param Language[] $languages
     *
     * @return DailyMeditation
     */
    public function setLanguages(array $languages): DailyMeditation
    {
        foreach ($this->languages as $language) {
            if (!in_array($language, $languages, true)) {
                $this->languages->removeElement($language);
            }
        }

        foreach ($languages as $language) {
            if (!$this->languages->contains($language)) {
                $this->languages->add($language);
            }
        }

        return $this;
    }

}
