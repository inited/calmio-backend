<?php

declare(strict_types=1);

namespace App\Model\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @package App\Model\Entity
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="user__promo_code_history")
 * @ORM\Entity()
 */
class UserPromoCodeHistoryEntity
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var PromoCode
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\PromoCode")
     */
    private $promoCode;

    /**
     * @var DateTime
     * @ORM\Column(name="promo_code_active_from", type="datetime")
     */
    private $promoCodeActiveFrom;

    /**
     * @var DateTime
     * @ORM\Column(name="promo_code_active_to", type="datetime")
     */
    private $promoCodeActiveTo;

    /**
     * @var UserEntity
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\UserEntity", inversedBy="promoCodes")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * @var bool
     * @ORM\Column(name="valid", type="boolean")
     */
    private $valid = true;

    public function __construct(PromoCode $promoCode, UserEntity $user)
    {
        $this->promoCode = $promoCode;
        $this->promoCodeActiveFrom = new DateTime();
        $this->promoCodeActiveTo = new DateTime($promoCode->getValue() . 'days');
        $this->user = $user;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPromoCode(): PromoCode
    {
        return $this->promoCode;
    }

    public function getPromoCodeActiveFrom(): DateTime
    {
        return $this->promoCodeActiveFrom;
    }

    public function getPromoCodeActiveTo(): DateTime
    {
        return $this->promoCodeActiveTo;
    }

    public function invalidate(): void
    {
        $this->valid = false;
    }

    public function isValid(): bool
    {
        return $this->valid;
    }
}
