<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CourseUser
 * @package App\Model\Entity
 * @ORM\Table(name="course__user")
 * @ORM\Entity()
 */
class CourseUser
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var Course
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Course", inversedBy="users")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $course;

    /**
     * @var ArrayCollection|CourseUserLesson[]
     * @ORM\OneToMany(targetEntity="App\Model\Entity\CourseUserLesson", mappedBy="courseUser", orphanRemoval=true,
     *     cascade={"persist","merge"})
     */
    private $lessons;

    /**
     * @var string
     * @ORM\Column(name="preferred_voice")
     */
    private $preferredVoice = Voice::MAN;

    /**
     * @var UserEntity
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\UserEntity", inversedBy="enrolledCourses")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * CourseUser constructor.
     */
    public function __construct()
    {
        $this->lessons = new ArrayCollection();
    }

    public function addCourseUserLesson(CourseUserLesson $courseUserLesson): CourseUser
    {
        if (!$this->lessons->contains($courseUserLesson)) {
            $this->lessons->add($courseUserLesson);
        }

        return $this;
    }

    /**
     * @return Course
     */
    public function getCourse(): Course
    {
        return $this->course;
    }

    /**
     * @param Course $course
     *
     * @return CourseUser
     */
    public function setCourse(Course $course): CourseUser
    {
        $this->course = $course;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param LessonEntity $lessonEntity
     *
     * @return CourseUserLesson|null
     */
    public function getLesson(LessonEntity $lessonEntity): ?CourseUserLesson
    {
        foreach ($this->lessons as $lesson) {
            if ($lessonEntity === $lesson->getLesson()) {
                return $lesson;
            }
        }
        return null;
    }

    /**
     * @return CourseUserLesson[]
     */
    public function getLessons()
    {
        $lessons = $this->lessons->toArray();

        usort($lessons, function (CourseUserLesson $a, CourseUserLesson $b) {
            return $a->getLesson()->getOrder() <=> $b->getLesson()->getOrder();
        });

        return $lessons;
    }

    public function getLessonsCount(): int
    {
        return $this->lessons->count();
    }

    public function getLessonsFinishedCount(): int
    {
        $count = 0;

        foreach ($this->lessons as $lesson) {
            if ($lesson->isCompleted()) {
                $count++;
            }
        }

        return $count;
    }

    public function getLessonsSpendTotalTime(): int
    {
        $time = 0;

        foreach ($this->lessons as $lesson) {
            $time += $lesson->getTime();
        }

        return $time;
    }

    /**
     * @return string
     */
    public function getPreferredVoice(): string
    {
        return $this->preferredVoice;
    }

    /**
     * @param string $preferredVoice
     *
     * @return CourseUser
     */
    public function setPreferredVoice(string $preferredVoice): CourseUser
    {
        $this->preferredVoice = $preferredVoice;
        return $this;
    }

    /**
     * @return UserEntity
     */
    public function getUser(): UserEntity
    {
        return $this->user;
    }

    /**
     * @param UserEntity $user
     *
     * @return CourseUser
     */
    public function setUser(UserEntity $user): CourseUser
    {
        $this->user = $user;
        $this->preferredVoice = $user->getPreferredVoice();
        return $this;
    }
}
