<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 06.06.2020
 */

declare(strict_types=1);

namespace App\Model\Entity;

class Voice
{
    public const MAN = 'man';
    public const WOMAN = 'woman';
}
