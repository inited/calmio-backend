<?php

declare(strict_types=1);

namespace App\Model\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="promo_code__promo_code", indexes={
 *     @ORM\Index(columns={"type"}),
 *     @ORM\Index(columns={"value"}),
 * })
 */
class PromoCode implements SerializableInterface
{
    const TYPE_GIFT = 'gift';
    const TYPE_PROMO = 'promo';

    /**
     * @var string
     * @ORM\Column(name="id", type="string")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var int|null
     * @ORM\Column(name="available", type="integer", nullable=true)
     */
    private $available;

    /**
     * @var DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var DateTime|null
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var Partner|null
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Partner", inversedBy="promoCodes")
     */
    private $partner;

    /**
     * @var integer
     * @ORM\Column(name="purchases", type="integer")
     */
    private $purchases = 0;

    /**
     * @var string
     * @ORM\Column(name="type", type="string")
     */
    private $type = self::TYPE_PROMO;

    /**
     * @var int
     * @ORM\Column(name="used", type="integer")
     */
    private $used = 0;

    /**
     * @var int
     * @ORM\Column(name="value", type="integer")
     */
    private $value = 0;

    public function __construct(string $code)
    {
        $this->id = $code;
        $this->createdAt = new DateTime();
    }

    public function __toArray(): array
    {
        return [
            'name' => $this->id,
            'created' => $this->createdAt->getTimestamp(),
            'value' => $this->value,
            'usage' => [
                'total' => $this->used,
                'conversions' => $this->getConversions(),
            ],
        ];
    }

    public function addPurchases(int $increment = 1): PromoCode
    {
        $this->purchases += $increment;

        if ($this->partner) {
            $this->partner->addPurchases($increment);
        }

        return $this;
    }

    public function addUsage(int $increment = 1): PromoCode
    {
        $this->used += $increment;

        if ($this->partner) {
            $this->partner->addUsage($increment);
        }

        return $this;
    }

    public function getAvailable(): ?int
    {
        return $this->available;
    }

    public function setAvailable(int $available): PromoCode
    {
        $this->available = $available;
        return $this;
    }

    public function getConversions(): float
    {
        return $this->purchases ? round($this->purchases / $this->used, 2) : 0;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getDeletedAt(): ?DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?DateTime $deletedAt): PromoCode
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getPartner(): ?Partner
    {
        return $this->partner;
    }

    public function setPartner(Partner $partner): PromoCode
    {
        $this->partner = $partner;
        return $this;
    }

    public function getPartnerName(): ?string
    {
        if ($this->partner) {
            return $this->partner->getName();
        }

        return null;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): PromoCode
    {
        $this->type = $type;
        return $this;
    }

    public function getUsed(): int
    {
        return $this->used;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): PromoCode
    {
        $this->value = $value;
        return $this;
    }

    public function invalidate(): void
    {
        if ($this->isValid()) {
            $this->deletedAt = new DateTime();
        }
    }

    public function isValid(): bool
    {
        return null === $this->deletedAt && (null === $this->available || $this->available > $this->used);
    }
}
