<?php

declare(strict_types=1);

namespace App\Model\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @package App\Model\Entity
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="user__device_old", indexes={
 *     @ORM\Index(columns={"platform","valid"}),
 *     @ORM\Index(columns={"token"})
 * })
 * @ORM\Entity()
 * @deprecated
 */
class UserDeviceOld implements SerializableInterface
{
    const PLATFORM_ANDROID = 'android';
    const PLATFORM_IOS = 'ios';

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var DateTimeInterface
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     * @ORM\Column(name="platform")
     */
    private $platform;

    /**
     * @var string
     * @ORM\Column(name="token")
     */
    private $token;

    /**
     * @var UserEntity
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\UserEntity", inversedBy="devices")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * @var boolean
     * @ORM\Column(name="valid", type="boolean")
     */
    private $valid = true;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    public function __toArray(): array
    {
        return [
            'platform' => $this->platform,
            'token' => $this->token,
            'valid' => $this->valid,
        ];
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPlatform(): string
    {
        return $this->platform;
    }

    public function setPlatform(string $platform): self
    {
        $this->platform = $platform;
        return $this;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;
        return $this;
    }

    public function getUser(): UserEntity
    {
        return $this->user;
    }

    public function setUser(UserEntity $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function isValid(): bool
    {
        return $this->valid;
    }

    public function setValid(bool $valid): self
    {
        $this->valid = $valid;
        return $this;
    }

}
