<?php

declare(strict_types=1);

namespace App\Model\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @package App\Model\Entity
 * @ORM\Table(name="system__feedback")
 * @ORM\Entity()
 */
class Feedback
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="contact_email",type="string")
     */
    private $contactEmail;

    /**
     * @var DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string|null
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var string
     * @ORM\Column(name="metadata", type="text")
     */
    private $metadata;

    /**
     * @var string|null
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    public function getContactEmail(): string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(string $contactEmail): Feedback
    {
        $this->contactEmail = $contactEmail;
        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): Feedback
    {
        $this->email = $email;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): Feedback
    {
        $this->message = $message;
        return $this;
    }

    public function getMetadata(): string
    {
        return $this->metadata;
    }

    public function setMetadata(string $metadata): Feedback
    {
        $this->metadata = $metadata;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): Feedback
    {
        $this->name = $name;
        return $this;
    }
}
