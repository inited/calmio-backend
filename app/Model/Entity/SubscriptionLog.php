<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @package App\Model\Entity
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="subscription__log")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class SubscriptionLog
{
    use Identifier;
    use Timestampable;

    /**
     * @var Device
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Device")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $device;

    /**
     * @var string
     * @ORM\Column(name="subscriptions_app", type="text")
     */
    private $subscriptionsApp;

    /**
     * @var string
     * @ORM\Column(name="subscriptions_be", type="text")
     */
    private $subscriptionsBe;

    /**
     * @var bool
     * @ORM\Column(name="trouble", type="boolean")
     */
    private $trouble = false;

    public function __construct(Device $device, string $subscriptionsApp, string $subscriptionsBe, bool $trouble)
    {
        $this->device = $device;
        $this->subscriptionsApp = $subscriptionsApp;
        $this->subscriptionsBe = $subscriptionsBe;
        $this->trouble = $trouble;
    }
}
