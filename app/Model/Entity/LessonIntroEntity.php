<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11. 08. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class LessonIntroEntity
 * @package App\Model\Entity
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Embeddable()
 */
class LessonIntroEntity
{
    /**
     * @var null|string
     * @ORM\Column(name="text", type="string", nullable=true)
     */
    private $text;

    /**
     * @var null|string
     * @ORM\Column(name="video", type="string", nullable=true)
     */
    private $video;

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     * @return LessonIntroEntity
     */
    public function setText(?string $text): LessonIntroEntity
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVideo(): ?string
    {
        return $this->video;
    }

    /**
     * @param string|null $video
     * @return LessonIntroEntity
     */
    public function setVideo(?string $video): LessonIntroEntity
    {
        $this->video = $video;
        return $this;
    }


}
