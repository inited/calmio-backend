<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Article
 * @package App\Model\Entity
 * @ORM\Entity()
 * @ORM\Table(name="article__article")
 */
class Article
{
    const TYPE_ABOUT = "ABOUT";
    const TYPE_TERMS_AND_CONDITIONS = "TERMS_AND_CONDITIONS";

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="`key`", type="string")
     */
    private $key;

    /**
     * @var string
     * @ORM\Column(name="content",type="text")
     */
    private $content = '';

    /**
     * @var Language
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Language")
     * @ORM\JoinColumn()
     */
    private $language;

    /**
     * @return array
     */
    public function __toArray(): array
    {
        return [
            "id" => $this->id,
            "html" => $this->content,
            "key" => $this->key,
            "language" => $this->language->getId(),
        ];
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return Article
     */
    public function setContent(string $content): Article
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     *
     * @return Article
     */
    public function setKey(string $key): Article
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @param Language $language
     *
     * @return Article
     */
    public function setLanguage(Language $language): Article
    {
        $this->language = $language;
        return $this;
    }

}
