<?php

declare(strict_types=1);

namespace App\Model\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @package App\Model\Entity
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="user__device")
 * @ORM\Entity()
 */
class UserDevice
{
    use Identifier;

    /**
     * @var DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var Device
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Device")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $device;

    /**
     * @var UserEntity
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\UserEntity", inversedBy="devices")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    public function __construct(Device $device, UserEntity $user)
    {
        $this->createdAt = new DateTime();
        $this->device = $device;
        $this->user = $user;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getDevice(): Device
    {
        return $this->device;
    }
}
