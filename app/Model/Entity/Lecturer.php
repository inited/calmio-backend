<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11.03.2020
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @package App\Model\Entity
 * @ORM\Entity()
 * @ORM\Table(name="lecturer__lecturer")
 */
class Lecturer
{
    const GENDER_MAN = 'man';
    const GENDER_WOMAN = 'woman';

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $deleted = false;

    /**
     * @var string
     * @ORM\Column()
     */
    private $gender = self::GENDER_MAN;

    /**
     * @var string
     * @ORM\Column()
     */
    private $name = '';

    /**
     * @var string|null
     * @ORM\Column(name="preview_url", nullable=true)
     */
    private $previewUrl;

    public function __toArray(): array
    {
        return [
            'id' => $this->id,
            'gender' => $this->gender,
            'name' => $this->name,
            'previewUrl' => $this->previewUrl,
        ];
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    public function setGender(string $gender): Lecturer
    {
        $this->gender = $gender;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Lecturer
    {
        $this->name = $name;
        return $this;
    }

    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): Lecturer
    {
        $this->deleted = $deleted;
        return $this;
    }

    public function setPreviewUrl(?string $previewUrl): Lecturer
    {
        $this->previewUrl = $previewUrl;
        return $this;
    }
}
