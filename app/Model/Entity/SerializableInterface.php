<?php

declare(strict_types=1);

namespace App\Model\Entity;

interface SerializableInterface
{
    /**
     * @return array<string, mixed>
     */
    public function __toArray(): array;
}
