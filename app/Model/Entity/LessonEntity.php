<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11. 08. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class LessonEntity
 * @package App\Model\Entity
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="lesson__lesson")
 * @ORM\Entity()
 */
class LessonEntity
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="description", type="text")
     */
    private $description = '';

    /**
     * @var null|LessonIntroEntity
     * @ORM\Embedded(class="App\Model\Entity\LessonIntroEntity")
     */
    private $intro;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name = '';

    /**
     * @var int
     * @ORM\Column(name="`order`", type="integer")
     */
    private $order = 0;

    /**
     * @var ArrayCollection|LessonSourceEntity[]
     * @ORM\OneToMany(targetEntity="App\Model\Entity\LessonSourceEntity", mappedBy="lesson",
     *     cascade={"persist","merge"}, orphanRemoval=true)
     */
    private $source;

    /**
     * @var string
     * @ORM\Column(name="type", type="string")
     */
    private $type = '';

    /**
     * LessonEntity constructor.
     */
    public function __construct()
    {
        $this->source = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function __toArray(): array
    {
        return [
            "id" => (string) $this->id,
            "description" => $this->description,
            "intro" => (null === $this->intro ? null : [
                "text" => $this->intro->getText() ?? "",
                "video" => $this->intro->getVideo() ?? "",
            ]),
            "name" => $this->name,
            "order" => $this->order,
            "source" => array_map(function (LessonSourceEntity $source) {
                return [
                    "lecturer" => $source->getLecturer() ? $source->getLecturer()->__toArray() : null,
                    "title" => $source->getTitle() ?? "",
                    "src" => $source->getSrc() ?? "",
                    "voice" => $source->getVoice(),
                ];
            }, $this->getSource()),
            "type" => $this->type ?? "",
        ];
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return LessonEntity
     */
    public function setDescription(string $description): LessonEntity
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return LessonIntroEntity|null
     */
    public function getIntro(): ?LessonIntroEntity
    {
        return $this->intro;
    }

    /**
     * @param LessonIntroEntity|null $intro
     */
    public function setIntro(?LessonIntroEntity $intro): void
    {
        $this->intro = $intro;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return LessonEntity
     */
    public function setName(string $name): LessonEntity
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     *
     * @return LessonEntity
     */
    public function setOrder(int $order): LessonEntity
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return LessonSourceEntity[]
     */
    public function getSource(): array
    {
        return $this->source->toArray();
    }

    /**
     * @param LessonSourceEntity[] $source
     *
     * @return LessonEntity
     */
    public function setSource(array $source): LessonEntity
    {
        foreach ($this->source as $sourceEntity) {
            $this->source->removeElement($sourceEntity);
        }

        foreach ($source as $item) {
            $item->setLesson($this);
            $this->source->add($item);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     *
     * @return LessonEntity
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

}
