<?php

namespace App\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @package App\Model\Entity
 * @ORM\Entity()
 * @ORM\Table(name="device__device")
 * @ORM\HasLifecycleCallbacks()
 */
class Device
{
    use Timestampable;

    const PLATFORM_ANDROID = 'android';
    const PLATFORM_IOS = 'ios';

    const AVAILABLE_PLATFORMS = [
        self::PLATFORM_ANDROID,
        self::PLATFORM_IOS,
    ];

    /**
     * @var string
     * @ORM\Column(name="id")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(name="platform", nullable=true)
     */
    private $platform;

    /**
     * @var string|null
     * @ORM\Column(name="push_token", nullable=true)
     */
    private $pushToken;

    /**
     * @var bool
     * @ORM\Column(name="push_token_valid", type="boolean")
     */
    private $pushTokenValid = false;

    /**
     * @var ArrayCollection|Subscription[]
     * @ORM\ManyToMany(targetEntity="App\Model\Entity\Subscription", inversedBy="devices")
     * @ORM\JoinTable(name="device__subscription")
     */
    private $subscriptions;

    public function __construct(string $uuid)
    {
        $this->id = $uuid;
        $this->subscriptions = new ArrayCollection();
    }

    public function addSubscription(Subscription $subscription): self
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions->add($subscription);
        }

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    public function setPlatform(?string $platform): Device
    {
        $this->platform = $platform;
        return $this;
    }

    public function getPushToken(): ?string
    {
        return $this->pushToken;
    }

    public function setPushToken(?string $pushToken): Device
    {
        $this->pushToken = $pushToken;
        $this->pushTokenValid = true;
        return $this;
    }

    /**
     * @return Subscription[]
     */
    public function getSubscriptions(): array
    {
        return $this->subscriptions->toArray();
    }

    public function isPushTokenValid(): bool
    {
        return $this->pushTokenValid;
    }

    public function setPushTokenValid(bool $pushTokenValid): Device
    {
        $this->pushTokenValid = $pushTokenValid;
        return $this;
    }
}
