<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 07. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Analytics
 * @package App\Model\Entity
 * @ORM\Table(name="analytics__analytics", indexes={
 *     @ORM\Index(columns={"active"}),
 *     @ORM\Index(columns={"platform"})
 * })
 * @ORM\Entity()
 */
class Analytics
{
    const SUBSCRIPTION_TYPE_1 = 'subscription1';
    const SUBSCRIPTION_TYPE_2 = 'subscription_monthly';
    const SUBSCRIPTION_TYPE_3 = 'subscription_annual';
    const SUBSCRIPTION_TYPE_4 = 'subscription_monthly_11_2021';
    const SUBSCRIPTION_TYPE_LIFETIME = 'lifetime_full_version';

    const AVAILABLE_SUBSCRIPTION_TYPES = [
        self::SUBSCRIPTION_TYPE_1,
        self::SUBSCRIPTION_TYPE_2,
        self::SUBSCRIPTION_TYPE_3,
        self::SUBSCRIPTION_TYPE_4,
        self::SUBSCRIPTION_TYPE_LIFETIME,
    ];

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var bool
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = true;

    /**
     * @var DateTime
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var DateTime|null
     * @ORM\Column(name="first_payment", type="datetime", nullable=true)
     */
    private $firstPayment;

    /**
     * @deprecated
     * @var string|null
     * @ORM\Column(name="iap_products_android", type="text", nullable=true)
     */
    private $iapProductsAndroid;

    /**
     * @deprecated
     * @var string|null
     * @ORM\Column(name="iap_products_ios", type="text", nullable=true)
     */
    private $iapProductsIos;

    /**
     * @var string|null
     * @ORM\Column(name="iap_purchase_type", nullable=true)
     */
    private $iapPurchaseType;

    /**
     * @var DateTime|null
     * @ORM\Column(name="last_payment", type="datetime", nullable=true)
     */
    private $lastPayment;

    /**
     * @var string|null
     * @ORM\Column(name="platform", nullable=true)
     */
    private $platform;

    /**
     * @var string|null
     * @ORM\Column(name="receipt", type="text", nullable=true)
     */
    private $receipt;

    /**
     * @deprecated
     * @var DateTime|null
     * @ORM\Column(name="timestamp_android", type="datetime", nullable=true)
     */
    private $timestampAndroid;

    /**
     * @deprecated
     * @var DateTime
     * @ORM\Column(name="timestamp_ios", type="datetime", nullable=true)
     */
    private $timestampIos;

    /**
     * @var UserEntity
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\UserEntity", inversedBy="enrolledCourses")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function getFirstPayment(): ?DateTime
    {
        return $this->firstPayment;
    }

    public function setFirstPayment(DateTime $firstPayment): Analytics
    {
        $this->firstPayment = $firstPayment;
        return $this;
    }

    public function getIapPurchaseType(): ?string
    {
        return $this->iapPurchaseType;
    }

    public function setIapPurchaseType(string $iapPurchaseType): Analytics
    {
        $this->iapPurchaseType = $iapPurchaseType;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLastPayment(): ?DateTime
    {
        return $this->lastPayment;
    }

    public function setLastPayment(DateTime $lastPayment): Analytics
    {
        $this->lastPayment = $lastPayment;
        return $this;
    }

    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    public function setPlatform(string $platform): Analytics
    {
        $this->platform = $platform;
        return $this;
    }

    public function getUser(): UserEntity
    {
        return $this->user;
    }

    public function setUser(UserEntity $user): Analytics
    {
        $this->user = $user;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): Analytics
    {
        $this->active = $active;
        return $this;
    }

    public function setReceipt(?string $receipt): Analytics
    {
        $this->receipt = $receipt;
        return $this;
    }

}
