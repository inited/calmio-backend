<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 13. 08. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tag
 * @package App\Model\Entity
 * @ORM\Entity(readOnly=true)
 * @ORM\Table(name="tag__tag")
 */
class Tag
{
    const BETTER_SLEEP = 'BETTER_SLEEP';
    const CONTACT_WITH_BODY = 'CONTACT_WITH_BODY';
    const FOCUS_AND_PRODUCTIVITY = 'FOCUS_AND_PRODUCTIVITY';
    const MENTAL_HEALTH = 'MENTAL_HEALTH';
    const PERSONAL_GROWTH = 'PERSONAL_GROWTH';
    const STRESS_AND_ANXIETY = 'STRESS_AND_ANXIETY';

    // Experience
    const LOTS_OF_EXPERIENCE = 'LOTS_OF_EXPERIENCE';
    const MILD_EXPERIENCE = 'MILD_EXPERIENCE';
    const NO_EXPERIENCE = 'NO_EXPERIENCE';

    /**
     * @var string
     * @ORM\Column(name="id", type="string")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name = '';

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

}
