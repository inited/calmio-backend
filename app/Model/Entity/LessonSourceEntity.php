<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 11. 08. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class LessonSourceEntity
 * @package App\Model\Entity
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="lesson__lesson_source")
 * @ORM\Entity()
 */
class LessonSourceEntity
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    private $id;

    /**
     * @var Lecturer|null
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Lecturer")
     * @ORM\JoinColumn()
     */
    private $lecturer;

    /**
     * @var LessonEntity
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\LessonEntity", inversedBy="source")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $lesson;

    /**
     * @var null|string
     * @ORM\Column(name="src", type="string", nullable=true)
     */
    private $src;

    /**
     * @var null|string
     * @ORM\Column(name="title", type="string", nullable=true)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="voice")
     */
    private $voice = Voice::MAN;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function getLecturer(): ?Lecturer
    {
        return $this->lecturer;
    }

    public function setLecturer(?Lecturer $lecturer): LessonSourceEntity
    {
        $this->lecturer = $lecturer;
        return $this;
    }

    /**
     * @return LessonEntity
     */
    public function getLesson(): LessonEntity
    {
        return $this->lesson;
    }

    /**
     * @param LessonEntity $lesson
     *
     * @return LessonSourceEntity
     */
    public function setLesson(LessonEntity $lesson): LessonSourceEntity
    {
        $this->lesson = $lesson;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSrc(): ?string
    {
        return $this->src;
    }

    /**
     * @param string|null $src
     *
     * @return LessonSourceEntity
     */
    public function setSrc(?string $src): LessonSourceEntity
    {
        $this->src = $src;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return LessonSourceEntity
     */
    public function setTitle(?string $title): LessonSourceEntity
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getVoice(): string
    {
        return $this->voice;
    }

    /**
     * @param string $voice
     *
     * @return LessonSourceEntity
     */
    public function setVoice(string $voice): LessonSourceEntity
    {
        $this->voice = $voice;
        return $this;
    }

}
