<?php

namespace App\Model\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Identifier
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var integer|null
     */
    private $id;

    final public function getId(): int
    {
        return $this->id;
    }

    public function __clone()
    {
        $this->id = null;
    }
}
