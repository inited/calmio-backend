<?php
namespace App\Model\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @package App\Model\Entity
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="subscription__subscription", indexes={
 *     @ORM\Index(columns={"platform"}),
 *     @ORM\Index(columns={"token_hash"})
 * })
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Subscription
{
    use Identifier;
    use Timestampable;

    const TYPE_ANNUAL = 'ANNUAL';
    const TYPE_LIFETIME = 'LIFETIME';
    const TYPE_MONTHLY = 'MONTHLY';

    const SUBSCRIPTION_LENGTH = [
        self::TYPE_ANNUAL => 12,
        self::TYPE_MONTHLY => 1,
    ];

    /**
     * @var UserEntity
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\UserEntity")
     * @ORM\JoinColumn(onDelete="CASCADE", name="first_user_id")
     */
    private $firstUser;

    /**
     * @var Device[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Model\Entity\Device", mappedBy="subscriptions")
     */
    private $devices;

    /**
     * @var string|null
     * @ORM\Column(name="last_error", nullable=true)
     */
    private $lastError;

    /**
     * @var bool
     * @ORM\Column(name="last_transaction_is_auto_renewing", type="boolean")
     */
    private $lastTransactionIsAutoRenewing = true;

    /**
     * @var bool
     * @ORM\Column(name="last_transaction_is_upgraded", type="boolean")
     */
    private $lastTransactionIsUpgraded = false;

    /**
     * @var integer|null
     * @ORM\Column(name="last_transaction_payment_state", type="integer", nullable=true)
     */
    private $lastTransactionPaymentState;

    /**
     * @var DateTime|null
     * @ORM\Column(name="last_transaction_valid_to", type="datetime", nullable=true)
     */
    private $lastTransactionValidTo;

    /**
     * @var DateTime
     * @ORM\Column(name="last_verification", type="datetime")
     */
    private $lastVerification;

    /**
     * @var string
     * @ORM\Column()
     */
    private $platform;

    /**
     * @var string|null
     * @ORM\Column(name="product_id", nullable=true)
     */
    private $productId;

    /**
     * Android.purchaseToken, Ios.receipt
     * @var string
     * @ORM\Column(type="text")
     */
    private $token;

    /**
     * @var string
     * @ORM\Column(name="token_hash")
     */
    private $tokenHash;

    /**
     * @var SubscriptionTransaction[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Model\Entity\SubscriptionTransaction", mappedBy="subscription",
     *     cascade={"persist","merge"})
     */

    /**
     * @var ArrayCollection|SubscriptionTransaction[]
     * @ORM\ManyToMany(targetEntity="App\Model\Entity\SubscriptionTransaction", mappedBy="subscriptions",
     *     cascade={"persist", "merge"})
     */
    private $transactions;

    public function __construct(string $platform, string $token, UserEntity $user)
    {
        $this->devices = new ArrayCollection();
        $this->firstUser = $user;
        $this->platform = $platform;
        $this->token = $token;
        $this->tokenHash = md5($token);
        $this->transactions = new ArrayCollection();

        $this->transactions = new ArrayCollection();
    }

    public function addTransaction(SubscriptionTransaction $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $transaction->addSubscription($this);
            $this->transactions->add($transaction);
        }

        return $this;
    }

    public function getFirstUser(): UserEntity
    {
        return $this->firstUser;
    }

    public function getLastError(): ?string
    {
        return $this->lastError;
    }

    public function setLastError(?string $lastError): Subscription
    {
        $this->lastError = $lastError;
        return $this;
    }

    public function getLastValidTransaction(): ?SubscriptionTransaction
    {
        return collect($this->transactions)
            ->filter(function (SubscriptionTransaction $transaction) {
                return !$transaction->isUpgraded() || $transaction->isAutoRenewing();
            })
            ->sortBy(function (SubscriptionTransaction $transaction) {
                return $transaction->getValidTo()->getTimestamp();
            }, SORT_REGULAR, true)
            ->first();
    }

    public function getLastVerification(): DateTime
    {
        return $this->lastVerification;
    }

    public function setLastVerification(DateTime $lastVerification): Subscription
    {
        $this->lastVerification = $lastVerification;
        return $this;
    }

    public function getPlatform(): string
    {
        return $this->platform;
    }

    public function getProductId(): ?string
    {
        return $this->productId;
    }

    public function setProductId(string $productId): Subscription
    {
        $this->productId = $productId;
        return $this;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getTokenHash(): string
    {
        return $this->tokenHash;
    }

    public function getTransactionByTransactionId(string $transactionId): ?SubscriptionTransaction
    {
        return collect($this->transactions)
            ->filter(function (SubscriptionTransaction $transaction) use ($transactionId) {
                return $transactionId === $transaction->getOrderId()
                    || $transactionId === $transaction->getTransactionId();
            })
            ->first();
    }

    /** @return SubscriptionTransaction[] */
    public function getTransactions(): array
    {
        return $this->transactions->toArray();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateLastTransactionInfo(): void
    {
        /** @var SubscriptionTransaction|null $lastTransaction */
        $lastTransaction = collect($this->transactions)
            ->sortBy(function (SubscriptionTransaction $transaction) {
                return $transaction->getValidTo()->getTimestamp();
            }, SORT_REGULAR, true)
            ->first();

        if ($lastTransaction) {
            $this->lastTransactionIsAutoRenewing = $lastTransaction->isAutoRenewing();
            $this->lastTransactionIsUpgraded = $lastTransaction->isUpgraded();
            $this->lastTransactionPaymentState = $lastTransaction->getPaymentState();
            $this->lastTransactionValidTo = $lastTransaction->getValidTo();
        } else {
            $this->lastTransactionIsAutoRenewing = true;
            $this->lastTransactionIsUpgraded = false;
            $this->lastTransactionPaymentState = null;
            $this->lastTransactionValidTo = null;
        }
    }
}
