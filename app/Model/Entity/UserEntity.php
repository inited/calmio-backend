<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 12. 01. 2019
 */

declare(strict_types=1);

namespace App\Model\Entity;

use App\Services\MailchimpService\MailchimpMemberInterface;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Class UserEntity
 * @package App\Model\Entity
 * @author  Tomas Pavlik <info@tomaspavlik.cz>
 * @ORM\Table(name="user__user", indexes={@ORM\Index(columns={"role"})})
 * @ORM\Entity()
 */
class UserEntity implements MailchimpMemberInterface, UserInterface
{
    const ROLE_ADMIN = "ADMIN";
    const ROLE_USER = "USER";

    const TYPE_APPLE = "APPLE";
    const TYPE_FACEBOOK = "FACEBOOK";
    const TYPE_GOOGLE = "GOOGLE";

    use Identifier;

    /** @return array */
    public static function getTypes(): array
    {
        return [
            self::TYPE_APPLE,
            self::TYPE_FACEBOOK,
            self::TYPE_GOOGLE,
        ];
    }

    /**
     * @var boolean
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = false;

    /**
     * @var string|null
     * @ORM\Column(name="apple_token", nullable=true)
     */
    private $appleToken;

    /**
     * @var int
     * @ORM\Column(name="continuous_days", type="integer")
     */
    private $continuousDays = 0;

    /**
     * @var DateTime
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var ArrayCollection|UserDevice[]
     * @ORM\OneToMany(targetEntity="App\Model\Entity\UserDevice", mappedBy="user", cascade={"persist", "merge"})
     */
    private $devices;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", unique=true)
     */
    private $email;

    /**
     * @var ArrayCollection|CourseUser[]
     * @ORM\OneToMany(targetEntity="App\Model\Entity\CourseUser", mappedBy="user", orphanRemoval=true,
     *     cascade={"persist","merge"})
     */
    private $enrolledCourses;

    /**
     * @var null|Tag
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Tag")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $experience;

    /**
     * @var null|string
     * @ORM\Column(name="facebook_token", type="string", nullable=true)
     */
    private $facebookToken;

    /**
     * @var null|DateTime
     * @ORM\Column(name="full_version_expiration", type="datetime", nullable=true)
     */
    private $fullVersionExpiration;

    /**
     * @var ArrayCollection|Tag[]
     * @ORM\ManyToMany(targetEntity="App\Model\Entity\Tag")
     * @ORM\JoinTable(name="user__tag",
     *      joinColumns={@ORM\JoinColumn(onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(onDelete="CASCADE")})
     */
    private $installReason;

    /**
     * @var Language
     * @ORM\ManyToOne(targetEntity="App\Model\Entity\Language")
     * @ORM\JoinColumn()
     */
    private $language;

    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var null|string
     * @ORM\Column(name="google_token", type="string", nullable=true)
     */
    private $googleToken;

    /**
     * @var DateTime
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @var string|null
     * @ORM\Column(name="mailchimp_id", nullable=true)
     */
    private $mailchimpId;

    /**
     * @var bool
     * @ORM\Column(name="push_notification", type="boolean")
     */
    private $pushNotification = true;

    /**
     * @var string
     * @deprecated
     * @ORM\Column(name="role",type="string")
     */
    private $role = self::ROLE_USER;

    /**
     * @var ArrayCollection|Role[]
     * @ORM\ManyToMany(targetEntity="App\Model\Entity\Role", indexBy="role_id")
     * @ORM\JoinTable(name="user__role",
     *      joinColumns={@ORM\JoinColumn(onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(onDelete="CASCADE")})
     */
    private $roles;

    /**
     * @var string
     * @ORM\Column(name="surname", type="string")
     */
    private $surname;

    /**
     * @var null|string
     * @ORM\Column(name="password", type="string", nullable=true)
     */
    private $password;

    /**
     * @var null|string
     * @ORM\Column(name="password_reset_hash", type="string", nullable=true)
     */
    private $passwordResetHash;

    /**
     * @var string
     * @ORM\Column(name="preferred_voice")
     */
    private $preferredVoice = Voice::MAN;

    /**
     * @var UserPromoCodeHistoryEntity[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Model\Entity\UserPromoCodeHistoryEntity", mappedBy="user", cascade={"persist"})
     */
    private $promoCodes;

    /**
     * @var ArrayCollection|TokenEntity[]
     * @ORM\OneToMany(targetEntity="TokenEntity", mappedBy="user", fetch="EXTRA_LAZY", cascade={"persist"})
     */
    private $tokens;

    /**
     * @var bool
     * @ORM\Column(name="verified", type="boolean")
     */
    private $verified = false;

    /**
     * @var DateTime|null
     * @ORM\Column(name="verification_until", type="datetime", nullable=true)
     */
    private $verificationUntil;

    public function __construct()
    {
        $this->created = new DateTime();
        $this->devices = new ArrayCollection();
        $this->enrolledCourses = new ArrayCollection();
        $this->installReason = new ArrayCollection();
        $this->passwordResetHash = Uuid::uuid4()->toString();
        $this->promoCodes = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->tokens = new ArrayCollection();
        $this->verificationUntil = new DateTime('+3days');
    }

    public function addDevice(UserDevice $userDevice): self
    {
        if (!$this->hasDevice($userDevice->getDevice())) {
            $this->devices->add($userDevice);
        }

        return $this;
    }

    public function addToken(TokenEntity $tokenEntity): UserEntity
    {
        $tokenEntity->setUser($this);
        $this->tokens->add($tokenEntity);
        return $this;
    }

    public function getActivePromoCode(): ?UserPromoCodeHistoryEntity
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('valid', true))
            ->setMaxResults(1);

        /** @var UserPromoCodeHistoryEntity|false $promoCodeHistory */
        $promoCodeHistory = $this->promoCodes->matching($criteria)->first();
        if (!$promoCodeHistory) {
            return null;
        }

        $now = new DateTime();
        if ($now->getTimestamp() > $promoCodeHistory->getPromoCodeActiveTo()->getTimestamp()) {
            return null;
        }

        return $promoCodeHistory;
    }

    public function getAppleToken(): ?string
    {
        return $this->appleToken;
    }

    public function setAppleToken(?string $appleToken): UserEntity
    {
        $this->appleToken = $appleToken;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getAuthTypes(): array
    {
        $types = [];

        if ($this->password) {
            $types[] = 'password';
        }

        if ($this->facebookToken) {
            $types[] = 'facebook';
        }

        if ($this->googleToken) {
            $types[] = 'google';
        }

        return $types;
    }

    public function getContinuousDays(): int
    {
        return $this->continuousDays;
    }

    public function setContinuousDays(int $continuousDays): UserEntity
    {
        $this->continuousDays = $continuousDays;
        return $this;
    }

    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTime $created): UserEntity
    {
        $this->created = $created;
        return $this;
    }

    /** @return UserDevice[] */
    public function getDevices(): array
    {
        return $this->devices->toArray();
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): UserEntity
    {
        $this->email = $email;
        return $this;
    }

    public function getEnrolledCourseByCourse(Course $course): ?CourseUser
    {
        foreach ($this->enrolledCourses as $entity) {
            if ($course === $entity->getCourse()) {
                return $entity;
            }
        }
        return null;
    }

    /**
     * @return CourseUser[]
     */
    public function getEnrolledCourses(): array
    {
        return $this->enrolledCourses->toArray();
    }

    public function getExperience(): ?Tag
    {
        return $this->experience;
    }

    public function setExperience(?Tag $experience): UserEntity
    {
        $this->experience = $experience;
        return $this;
    }

    public function getFacebookToken(): ?string
    {
        return $this->facebookToken;
    }

    public function setFacebookToken(?string $facebookToken): UserEntity
    {
        $this->facebookToken = $facebookToken;
        return $this;
    }

    public function getFullVersionExpiration(): ?DateTime
    {
        return $this->fullVersionExpiration;
    }

    public function setFullVersionExpiration(?DateTime $fullVersionExpiration): UserEntity
    {
        $this->fullVersionExpiration = $fullVersionExpiration;
        return $this;
    }

    public function getGoogleToken(): ?string
    {
        return $this->googleToken;
    }

    public function setGoogleToken(?string $googleToken): UserEntity
    {
        $this->googleToken = $googleToken;
        return $this;
    }

    public function getInstallReason(): array
    {
        return $this->installReason->toArray();
    }

    public function setInstallReason(array $installReason): UserEntity
    {
        foreach ($this->installReason as $reason) {
            if (!in_array($reason, $installReason, true)) {
                $this->installReason->removeElement($reason);
            }
        }

        foreach ($installReason as $reason) {
            if (!$this->installReason->contains($reason)) {
                $this->installReason->add($reason);
            }
        }

        return $this;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function setLanguage(Language $language): void
    {
        $this->language = $language;
    }

    public function getLanguageCode(): string
    {
        return $this->language->getId();
    }

    public function getLastActivity(): DateTime
    {
        return $this->lastLogin;
    }

    public function getLastLogin(): DateTime
    {
        return $this->lastLogin;
    }

    public function setLastLogin(DateTime $lastLogin): UserEntity
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }

    public function getMailchimpId(): ?string
    {
        return $this->mailchimpId;
    }

    public function setMailchimpId(?string $mailchimpId): UserEntity
    {
        $this->mailchimpId = $mailchimpId;
        return $this;
    }

    public function getMailchimpTags(): array
    {
        return collect($this->installReason->toArray())
            ->map(function (Tag $tag) {
                return $tag->getId();
            })
            ->toArray();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): UserEntity
    {
        $this->name = $name;
        return $this;
    }

    public function getPartnerPromoCodeHistory(): ?UserPromoCodeHistoryEntity
    {
        foreach ($this->promoCodes as $promoCodeHistory) {
            $promoCode = $promoCodeHistory->getPromoCode();
            if (PromoCode::TYPE_PROMO === $promoCode->getType()) {
                return $promoCodeHistory;
            }
        }

        return null;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): UserEntity
    {
        $this->password = $password;
        return $this;
    }

    public function getPasswordResetHash(): ?string
    {
        return $this->passwordResetHash;
    }

    public function setPasswordResetHash(?string $passwordResetHash): UserEntity
    {
        $this->passwordResetHash = $passwordResetHash;
        return $this;
    }

    public function getPreferredVoice(): string
    {
        return $this->preferredVoice;
    }

    public function setPreferredVoice(string $preferredVoice): UserEntity
    {
        $this->preferredVoice = $preferredVoice;
        return $this;
    }

    /**
     * @return UserPromoCodeHistoryEntity[]
     */
    public function getPromoCodes(): array
    {
        return $this->promoCodes->toArray();
    }

    public function getRegistrationDate(): DateTime
    {
        return $this->created;
    }

    /**
     * @return ArrayCollection|Role[]
     */
    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles(array $roles): UserEntity
    {
        foreach ($this->roles as $role) {
            if (!in_array($role, $roles, true)) {
                $this->roles->removeElement($role);
            }
        }

        foreach ($roles as $role) {
            if (!$this->roles->contains($role)) {
                $this->roles->set($role->getId(), $role);
            }
        }

        return $this;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): UserEntity
    {
        $this->surname = $surname;
        return $this;
    }

    public function getVerificationUntil(): ?DateTime
    {
        return $this->verificationUntil;
    }

    public function setVerificationUntil(?DateTime $verificationUntil): UserEntity
    {
        $this->verificationUntil = $verificationUntil;
        return $this;
    }

    public function hasActivePromoCode(): bool
    {
        return $this->getActivePromoCode() instanceof UserPromoCodeHistoryEntity;
    }

    public function hasCourse(Course $course): bool
    {
        foreach ($this->enrolledCourses as $enrolledCourse) {
            if ($course === $enrolledCourse->getCourse()) {
                return true;
            }
        }
        return false;
    }

    public function hasDevice(Device $device): bool
    {
        return collect($this->devices)
            ->filter(function (UserDevice $userDevice) use ($device) {
                return $device === $userDevice->getDevice();
            })
            ->isNotEmpty();
    }

    public function hasPartnerPromoCode(): bool
    {
        foreach ($this->promoCodes as $promoCodeHistory) {
            $promoCode = $promoCodeHistory->getPromoCode();
            if (PromoCode::TYPE_PROMO === $promoCode->getType()) {
                return true;
            }
        }

        return false;
    }

    public function hasPartnerPromoCodeEverBeenUsed(): bool
    {
        foreach ($this->promoCodes as $promoCode) {
            if (PromoCode::TYPE_PROMO === $promoCode->getPromoCode()->getType()) {
                return true;
            }
        }

        return false;
    }

    public function hasPromoCodeEverBeenUsed(PromoCode $promoCode): bool
    {
        foreach ($this->promoCodes as $usedPromoCode) {
            if ($usedPromoCode->getPromoCode() === $promoCode) {
                return true;
            }
        }

        return false;
    }

    public function hasRole(Role $role): bool
    {
        return $this->roles->contains($role);
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): UserEntity
    {
        $this->active = $active;
        return $this;
    }

    public function isPushNotification(): bool
    {
        return $this->pushNotification;
    }

    public function setPushNotification(bool $pushNotification): UserEntity
    {
        $this->pushNotification = $pushNotification;
        return $this;
    }

    public function isTestUser(): bool
    {
        foreach ($this->roles as $role) {
            if (Role::TYPE_TEST === $role->getId()) {
                return true;
            }
        }

        return false;
    }

    public function isVerificationRequired(): bool
    {
        if ($this->verified) {
            return false;
        }

        if ((new DateTime('-2days'))->getTimestamp() < $this->created->getTimestamp()) {
            return false;
        }

        return true;
    }

    public function isVerified(): bool
    {
        return $this->verified;
    }

    public function setVerified(bool $verified): UserEntity
    {
        $this->verified = $verified;
        return $this;
    }

    public function signToCourse(Course $course): UserEntity
    {
        if (false === $this->hasCourse($course)) {
            $entity = new CourseUser();
            $entity->setCourse($course);
            $entity->setUser($this);

            $this->enrolledCourses->set($course->getId(), $entity);
        }

        return $this;
    }

    public function usePromoCode(PromoCode $promoCode): UserEntity
    {
        foreach ($this->promoCodes as $usedPromoCode) {
            $usedPromoCode->invalidate();
        }

        $promoCode->addUsage();
        $this->promoCodes->add(new UserPromoCodeHistoryEntity($promoCode, $this));
        return $this;
    }
}
