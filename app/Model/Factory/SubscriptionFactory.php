<?php

namespace App\Model\Factory;

use App\Model\Entity\Subscription;
use App\Model\Entity\UserEntity;

class SubscriptionFactory
{
    public function create(string $platform, string $token, UserEntity $user): Subscription
    {
        return new Subscription($platform, $token, $user);
    }
}
