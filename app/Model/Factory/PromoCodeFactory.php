<?php

namespace App\Model\Factory;

use App\Model\Entity\PromoCode;

class PromoCodeFactory
{
    public function create(string $code): PromoCode
    {
        return new PromoCode($code);
    }
}