<?php

namespace App\Model\Factory;

use App\Model\Entity\SubscriptionTransaction;

class SubscriptionTransactionFactory
{
    public function create(): SubscriptionTransaction
    {
        return new SubscriptionTransaction();
    }
}
