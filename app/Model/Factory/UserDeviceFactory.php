<?php

namespace App\Model\Factory;

use App\Model\Entity\Device;
use App\Model\Entity\UserDevice;
use App\Model\Entity\UserEntity;

class UserDeviceFactory
{
    public function create(Device $device, UserEntity $user): UserDevice
    {
        return new UserDevice($device, $user);
    }
}
