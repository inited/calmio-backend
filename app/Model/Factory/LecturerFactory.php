<?php

namespace App\Model\Factory;

use App\Model\Entity\Lecturer;

class LecturerFactory
{
    public function create(): Lecturer
    {
        return new Lecturer();
    }
}
