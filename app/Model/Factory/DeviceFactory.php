<?php

namespace App\Model\Factory;

use App\Model\Entity\Device;

class DeviceFactory
{
    public function create(string $uuid): Device
    {
        return new Device($uuid);
    }
}
