<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 25. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\ValueObject;

/**
 * Class GoogleSettings
 * @package App\Model\ValueObject
 */
class GoogleSettings
{
    /** @var string */
    private $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }
}
