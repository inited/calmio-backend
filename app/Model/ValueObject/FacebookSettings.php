<?php

/**
 * @author: Tomas Pavlik <info@tomaspavlik.cz>
 * created: 25. 09. 2019
 */

declare(strict_types=1);

namespace App\Model\ValueObject;

/**
 * Class FacebookSettings
 * @package App\Model\ValueObject
 */
class FacebookSettings
{
    /**
     * @var string
     */
    private $appId;

    /**
     * @var string
     */
    private $appSecret;

    /**
     * FacebookSettings constructor.
     *
     * @param string $appId
     * @param string $appSecret
     */
    public function __construct(string $appId, string $appSecret)
    {
        $this->appId = $appId;
        $this->appSecret = $appSecret;
    }

    /**
     * @return string
     */
    public function getAppId(): string
    {
        return $this->appId;
    }

    /**
     * @return string
     */
    public function getAppSecret(): string
    {
        return $this->appSecret;
    }

}
