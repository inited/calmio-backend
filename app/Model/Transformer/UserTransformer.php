<?php
namespace App\Model\Transformer;

use App\Model\Entity\CourseUser;
use App\Model\Entity\CourseUserLesson;
use App\Model\Entity\Role;
use App\Model\Entity\Subscription;
use App\Model\Entity\Tag;
use App\Model\Entity\UserDevice;
use App\Model\Entity\UserEntity;
use App\Services\Storage\DeviceStorage;
use DateTime;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /** @var DeviceStorage */
    private $deviceStorage;

    public function __construct(DeviceStorage $deviceStorage)
    {
        $this->deviceStorage = $deviceStorage;
    }

    private function getUserSubscriptions(UserEntity $user): array
    {
        if (!$device = $this->deviceStorage->getDevice()) {
            return [];
        }

        $hasUserDevice = collect($user->getDevices())
            ->filter(function (UserDevice $userDevice) use ($device) {
                return $userDevice->getDevice() === $device;
            })
            ->count();

        if ($hasUserDevice === 0) {
            return [];
        }

        $currentTimestamp = (new DateTime('-1day'))->getTimestamp();

        return collect($device->getSubscriptions())
            ->filter(function (Subscription $subscription) use ($currentTimestamp) {
                if (!$lastValidTransaction = $subscription->getLastValidTransaction()) {
                    return false;
                }

                if ($lastValidTransaction->getValidTo()->getTimestamp() <= $currentTimestamp) {
                    return false;
                }

                return true;
            })
            ->map(function (Subscription $subscription) {
                $lastValidTransaction = $subscription->getLastValidTransaction();

                return [
                    "productId" => $lastValidTransaction->getProductId(),
                    "type" => $lastValidTransaction->getType(),
                    "validTo" => $lastValidTransaction->getValidTo()->getTimestamp(),
                ];
            })
            ->sortByDesc("validTo")
            ->unique("productId")
            ->values()
            ->toArray();
    }

    /**
     * @return mixed[]
     */
    public function transform(UserEntity $user): array
    {
        return [
            "id" => (string) $user->getId(),
            "auth" => $user->getAuthTypes(),
            "continuousDays" => $user->getContinuousDays(),
            "coursesOwned" => collect($user->getEnrolledCourses())
                ->map(function (CourseUser $courseUser) {
                    return [
                        "id" => (string) $courseUser->getCourse()->getId(),
                        "free" => $courseUser->getCourse()->isFree(),
                        "preferredVoice" => $courseUser->getPreferredVoice(),
                        "lessonsFinished" => collect($courseUser->getLessons())
                            ->map(function (CourseUserLesson $courseUserLesson) {
                                return [
                                    "id" => (string) $courseUserLesson->getLesson()->getId(),
                                    "time" => $courseUserLesson->getTime(),
                                    "unfinishedTime" => $courseUserLesson->getUnfinishedTime(),
                                    "completed" => $courseUserLesson->isCompleted(),
                                    "timestamp" => $courseUserLesson->getFinished()->getTimestamp(),
                                    "currentSessionCompleted" => $courseUserLesson->isCurrentSessionCompleted(),
                                    "currentSessionTime" => $courseUserLesson->getCurrentSessionTime(),
                                    "lessonDurationSeconds" => $courseUserLesson->getLessonDurationSeconds(),
                                    "voice" => $courseUserLesson->getVoice(),
                                ];
                            })
                            ->values(),
                    ];
                })
                ->values(),
            "created" => $user->getCreated()->getTimestamp(),
            "email" => $user->getEmail(),
            "experience" => (!$tag = $user->getExperience()) ? null : $tag->getId(),
            "fullVersionExpiration" => (!$expr = $user->getFullVersionExpiration()) ? 0 : $expr->getTimestamp(),
            "installReason" => collect($user->getInstallReason())
                ->map(function (Tag $tag) {
                    return $tag->getId();
                })
                ->values(),
            "language" => $user->getLanguage()->getId(),
            "lastLogin" => $user->getLastLogin()->getTimestamp(),
            "name" => $user->getName(),
            "preferredVoice" => $user->getPreferredVoice(),
            "promo" => (!$promoCode = $user->getActivePromoCode()) ? null : [
                "name" => $promoCode->getPromoCode()->getId(),
                "type" => $promoCode->getPromoCode()->getType(),
                "partnerName" => $promoCode->getPromoCode()->getPartnerName(),
                "expires" => $promoCode->getPromoCodeActiveTo()->getTimestamp(),
            ],
            "pushNotification" => $user->isPushNotification(),
            "roles" => collect($user->getRoles())
                ->map(function (Role $role) {
                    return $role->getId();
                })
                ->values(),
            "subscriptions" => $this->getUserSubscriptions($user),
            "surname" => $user->getSurname(),
            "verificationRequired" => $user->isVerificationRequired(),
        ];
    }
}
