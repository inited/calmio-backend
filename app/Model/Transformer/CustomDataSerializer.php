<?php

namespace App\Model\Transformer;

use League\Fractal\Serializer\ArraySerializer;

class CustomDataSerializer extends ArraySerializer
{
    /**
     * @param string $resourceKey
     * @param array  $data
     *
     * @return array|array[]
     */
    public function item($resourceKey, array $data): array
    {
        if ($resourceKey) {
            return [$resourceKey => $data];
        }

        return $data;
    }
}
