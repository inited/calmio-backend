<?php

namespace App\Command;

use App\Services\Job\VerifySubscriptions;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class VerifyPurchaseSubscriptionsCommand extends Command
{
    /** @var VerifySubscriptions */
    private $verifySubscriptions;

    public function __construct(VerifySubscriptions $verifySubscriptions)
    {
        parent::__construct();
        $this->verifySubscriptions = $verifySubscriptions;
    }

    protected function configure(): void
    {
        $this
            ->setName("calmio:verify-subscriptions")
            ->setDescription("Verify Apple/Google subscriptions");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("Running ...");

        $this->verifySubscriptions->run();

        $output->writeln("Finished.");

        return 0;
    }
}
