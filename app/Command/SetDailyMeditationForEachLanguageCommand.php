<?php

namespace App\Command;

use App\Services\Job\SetDailyMeditationForEachLanguage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetDailyMeditationForEachLanguageCommand extends Command
{
    /** @var SetDailyMeditationForEachLanguage */
    private $job;

    public function __construct(SetDailyMeditationForEachLanguage $job)
    {
        parent::__construct();
        $this->job = $job;
    }

    protected function configure(): void
    {
        $this
            ->setName("calmio:set-daily-meditation-for-each-language")
            ->setDescription("Set daily meditation for each language.");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("Running ...");

        $this->job->run();

        $output->writeln("Finished.");

        return 0;
    }
}
