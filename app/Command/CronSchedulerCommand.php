<?php

namespace App\Command;

use GO\Scheduler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CronSchedulerCommand extends Command
{
    /** @var Scheduler */
    private $scheduler;

    public function __construct(Scheduler $scheduler)
    {
        parent::__construct();
        $this->scheduler = $scheduler;
    }

    protected function configure(): void
    {
        $this
            ->setName("scheduler:run")
            ->setDescription("Run cron scheduler.");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("Running ...");

        $this->scheduler->run();

        $output->writeln("Finished.");

        return 0;
    }
}
