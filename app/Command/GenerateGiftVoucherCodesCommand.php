<?php

namespace App\Command;

use App\Model\Entity\PromoCode;
use App\Model\Factory\PromoCodeFactory;
use CouponCode\CouponCode;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateGiftVoucherCodesCommand extends Command
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var PromoCodeFactory */
    private $promoCodeFactory;

    public function __construct(
        EntityManagerInterface $em,
        PromoCodeFactory $promoCodeFactory
    ) {
        parent::__construct();
        $this->em = $em;
        $this->promoCodeFactory = $promoCodeFactory;
    }

    protected function configure(): void
    {
        $this
            ->setName("calmio:generate-gift-codes")
            ->setDescription("Generate gift voucher codes.");

        $this->addArgument(
            "pieces",
            InputOption::VALUE_REQUIRED,
            "Number of generated codes"
        );

        $this->addArgument(
            "days",
            InputOption::VALUE_REQUIRED,
            "Duration of validity in days"
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln("Running ...");

        $days = (int) $input->getArgument("days");
        $pieces = (int) $input->getArgument("pieces");

        if ($days <= 0) {
            throw new Exception("Invalid argument \"days\"! (min: 1)");
        }

        if ($pieces <= 0 || $pieces > 1000) {
            throw new Exception("Invalid argument \"pieces\"! (min: 1, max: 1000)");
        }

        $codeGenerator = new CouponCode([
            "parts" => 1,
            "partLength" => 8,
        ]);

        for ($i = 1; $i <= $pieces; $i++) {
            $promoCode = $this->promoCodeFactory->create($codeGenerator->generate());
            $promoCode->setAvailable(1);
            $promoCode->setType(PromoCode::TYPE_GIFT);
            $promoCode->setValue($days);

            $this->em->persist($promoCode);
        }

        $this->em->flush();

        $output->writeln("Finished.");

        return 0;
    }
}
