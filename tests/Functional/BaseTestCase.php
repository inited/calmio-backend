<?php

namespace Tests\Functional;

use PHPUnit\Framework\TestCase;
use Slim\App;
use Slim\Container;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

class BaseTestCase extends TestCase
{
    /** @var App */
    protected $app;

    protected function setUp(): void
    {
        parent::setUp();

        $this->createApp();
        $this->runDatabaseMigrations();
    }

    private function createApp(): void
    {
        $settings = require __DIR__ . '/../../src/settings.php';

        $app = new App($settings);
        $this->app = $app;

        require __DIR__ . '/../../src/dependencies.php';
        require __DIR__ . '/../../src/routes.php';
    }

    private function runDatabaseMigrations(): void
    {
        /** @var ConsoleApplication $consoleApplication */
        $consoleApplication = $this->getContainer()->get(ConsoleApplication::class);
        $consoleApplication->setAutoExit(false);

        $input = new ArrayInput([
            'command' => 'migrations:migrate',
            '--quiet'
        ]);

        $output = new BufferedOutput();
        $consoleApplication->run($input, $output);
    }

    protected function getContainer(): Container
    {
        /** @var Container $container */
        $container = $this->app->getContainer();
        return $container;
    }

    public function makeHttpRequest(string $requestMethod, string $requestUri, $requestData = null): Response
    {
        $environment = Environment::mock(
            [
                'REQUEST_METHOD' => $requestMethod,
                'REQUEST_URI' => $requestUri
            ]
        );

        $request = Request::createFromEnvironment($environment);

        if (isset($requestData)) {
            $request = $request->withParsedBody($requestData);
        }

        $response = new Response();

        /** @var Response $response */
        $response = $this->app->process($request, $response);
        return $response;
    }
}
