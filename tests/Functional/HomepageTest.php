<?php

namespace Tests\Functional;

class HomepageTest extends BaseTestCase
{
    public function testGetHomepageWithoutName()
    {
        $response = $this->makeHttpRequest('GET', '/api/app-version');

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertStringContainsString('SlimFramework', (string) $response->getBody());
        $this->assertStringNotContainsString('Hello', (string) $response->getBody());
    }
}
