#!/usr/bin/env php
<?php

use Slim\App;
use Symfony\Component\Console\Application as ConsoleApplication;
use Tracy\Debugger;

require __DIR__ . '/../vendor/autoload.php';

Debugger::enable([], __DIR__ . '/../logs/', 'info@tomaspavlik.cz');

$settings = require __DIR__ . '/../src/settings.php';
if (is_file(__DIR__ . '/../src/settings.local.php')) {
    $settingsLocal = require __DIR__ . '/../src/settings.local.php';
    $settings = array_replace_recursive($settings, $settingsLocal);
}

Sentry\init(['dsn' => ($settings['settings']['sentryDsn'] ?? '')]);

$app = new App($settings);
$container = $app->getContainer();
require __DIR__ . '/../src/dependencies.php';

$consoleApplication = $container->get(ConsoleApplication::class);
$consoleApplication->run();
