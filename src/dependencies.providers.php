<?php

declare(strict_types=1);

/** @var ContainerInterface $container */

use App\Model\Provider;
use App\Model\Repository;
use Psr\Container\ContainerInterface;

$container[Provider\CourseProvider::class] = function (ContainerInterface $container) {
    return new Provider\CourseProvider(
        $container->get(Repository\LanguageRepository::class),
        $container->get(Provider\LessonProvider::class),
        $container->get(Repository\TagRepository::class)
    );
};

$container[Provider\DailyMeditationProvider::class] = function (ContainerInterface $container) {
    return new Provider\DailyMeditationProvider(
        $container->get(Repository\LanguageRepository::class),
        $container->get(Provider\LessonProvider::class)
    );
};

$container[Provider\LecturerProvider::class] = function () {
    return new Provider\LecturerProvider();
};

$container[Provider\LessonProvider::class] = function (ContainerInterface $container) {
    return new Provider\LessonProvider(
        $container->get(Repository\LecturerRepository::class)
    );
};

$container[Provider\PartnerProvider::class] = function () {
    return new Provider\PartnerProvider();
};
