<?php

declare(strict_types=1);

use App\Command\CronSchedulerCommand;
use App\Command\GenerateGiftVoucherCodesCommand;
use App\Command\SetDailyMeditationForEachLanguageCommand;
use App\Command\VerifyPurchaseSubscriptionsCommand;
use App\Model\Factory\PromoCodeFactory;
use App\Services\Job\SetDailyMeditationForEachLanguage;
use App\Services\Job\VerifySubscriptions;
use Doctrine\ORM\EntityManager;
use GO\Scheduler;
use Psr\Container\ContainerInterface;

$container[CronSchedulerCommand::class] = function (ContainerInterface $container) {
    return new CronSchedulerCommand(
        $container->get(Scheduler::class)
    );
};

$container[GenerateGiftVoucherCodesCommand::class] = function (ContainerInterface $container) {
    return new GenerateGiftVoucherCodesCommand(
        $container->get(EntityManager::class),
        $container->get(PromoCodeFactory::class)
    );
};

$container[SetDailyMeditationForEachLanguageCommand::class] = function (ContainerInterface $container) {
    return new SetDailyMeditationForEachLanguageCommand(
        $container->get(SetDailyMeditationForEachLanguage::class)
    );
};

$container[VerifyPurchaseSubscriptionsCommand::class] = function (ContainerInterface $container) {
    return new VerifyPurchaseSubscriptionsCommand(
        $container->get(VerifySubscriptions::class)
    );
};
