<?php

declare(strict_types=1);

/** @var ContainerInterface $container */

use App\EventSubscriber\UserRegisteredEventSubscriber;
use App\Services;
use Psr\Container\ContainerInterface;
use Symfony\Component\Translation\Translator;

$container[UserRegisteredEventSubscriber::class] = function (ContainerInterface $container) {
    return new UserRegisteredEventSubscriber(
        $container->get('settings')['apiUri'],
        $container->get(Services\MailService\MailService::class),
        $container->get(Translator::class)
    );
};
