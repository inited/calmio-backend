<?php

use App\Middleware\Cors;
use App\Middleware\Device;
use App\Middleware\Language;
use App\Model\Factory\DeviceFactory;
use App\Model\Repository\DeviceRepository;
use App\Model\Repository\LanguageRepository;
use App\Services\Storage\DeviceStorage;
use App\Services\Storage\LanguageStorage;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use RKA\Middleware\IpAddress;
use Symfony\Component\Translation\Translator;

$container[Cors::class] = function () {
    return new Cors();
};

$container[Device::class] = function (ContainerInterface $container) {
    return new Device(
        $container->get(DeviceFactory::class),
        $container->get(DeviceRepository::class),
        $container->get(DeviceStorage::class),
        $container->get(EntityManager::class)
    );
};

$container[IpAddress::class] = function () {
    return new IpAddress();
};

$container[Language::class] = function (ContainerInterface $container) {
    return new Language(
        $container->get(LanguageRepository::class),
        $container->get(LanguageStorage::class),
        $container->get(Translator::class)
    );
};
