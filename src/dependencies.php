<?php

declare(strict_types=1);

// DIC configuration
use App\Command;
use App\Controllers;
use App\EntitySubscriber\UserEntityMailchimpSyncSubscriber;
use App\EventSubscriber\UserRegisteredEventSubscriber;
use App\Handler\NotFoundHandler;
use App\Model\Entity;
use App\Model\Factory\UserDeviceFactory;
use App\Model\Provider;
use App\Model\Repository;
use App\Services;
use App\Services\Translation\Translator as ValidationTranslator;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\Migrations\Tools\Console\Command as DoctrineConsoleCommand;
use Doctrine\ORM;
use DoctrineExtensions\Query\Mysql;
use Psr\Container\ContainerInterface;
use Slim\App;
use Slim\Http\Environment;
use Slim\Http\Uri;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\Translator;
use Twig\TwigFunction;

/**
 * @var  App $app
 * @var ContainerInterface $container
 */
$container = $app->getContainer();

$container['notFoundHandler'] = function (ContainerInterface $container) {
    return new NotFoundHandler($container->get(Twig::class));
};

$container[ConsoleApplication::class] = function (ContainerInterface $container) {
    $application = new ConsoleApplication();

    $application->setHelperSet(
        new Symfony\Component\Console\Helper\HelperSet([
            'connection' => new ConnectionHelper($container->get(ORM\EntityManager::class)->getConnection()),
            'dialog' => new QuestionHelper(),
            'entityManager' => new ORM\Tools\Console\Helper\EntityManagerHelper(
                $container->get(ORM\EntityManager::class)
            ),
        ])
    );

    $application->add(new DoctrineConsoleCommand\ExecuteCommand());
    $application->add(new DoctrineConsoleCommand\GenerateCommand());
    $application->add(new DoctrineConsoleCommand\LatestCommand());
    $application->add(new DoctrineConsoleCommand\MigrateCommand());
    $application->add(new DoctrineConsoleCommand\DiffCommand());
    $application->add(new DoctrineConsoleCommand\UpToDateCommand());
    $application->add(new DoctrineConsoleCommand\StatusCommand());
    $application->add(new DoctrineConsoleCommand\VersionCommand());

    $application->add($container->get(Command\CronSchedulerCommand::class));
    $application->add($container->get(Command\GenerateGiftVoucherCodesCommand::class));
    $application->add($container->get(Command\SetDailyMeditationForEachLanguageCommand::class));
    $application->add($container->get(Command\VerifyPurchaseSubscriptionsCommand::class));

    return $application;
};

// Twig renderer
$container[Twig::class] = function (ContainerInterface $container) {
    $settings = $container->get('settings');
    $twigSettings = isset($settings['twig']) ? $settings['twig'] : [];
    $view = new Twig(__DIR__ . '/../templates/', $twigSettings);

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = Uri::createFromEnvironment(new Environment($_SERVER));

    /** @var Translator $translator */
    $translator = $container->get(Translator::class);

    // Add extensions
    $view->addExtension(new TwigExtension($router, $uri));
    $view->addExtension(new TranslationExtension($translator));

    // Functions
    $view->getEnvironment()->addFunction(
        new TwigFunction('asset', function ($asset) use ($settings) {
            $asset = ltrim($asset, '/');

            return rtrim($settings['assetsBaseUri'], '/') . '/assets/' . $asset;
        })
    );

    // Default variables
    $view->offsetSet('base_path', rtrim($settings['apiUri'], '/'));
    $view->offsetSet('lang', $translator->getLocale());
    $view->offsetSet('web_name', $translator->trans('common.name'));

    return $view;
};

$container[Translator::class] = function () {
    $translator = new Translator('cs');
    $translator->setFallbackLocales(['cs']);
    $yamlLoader = new YamlFileLoader();
    $translator->addLoader('yaml', $yamlLoader);

    $langPath = __DIR__ . '/../lang/';
    $files = array_diff(scandir($langPath), ['.', '..']);

    foreach ($files as $file) {
        [$domain, $lang] = explode('.', $file);
        $translator->addResource('yaml', $langPath . $file, $lang, $domain);
    }

    return $translator;
};

$container[Services\Validation\ValidationFactory::class] = function (ContainerInterface $container) {
    return new Services\Validation\ValidationFactory($container->get(ValidationTranslator::class));
};

$container[ValidationTranslator::class] = function (ContainerInterface $container) {
    return new ValidationTranslator($container->get(Translator::class));
};

// monolog
$container['logger'] = function (ContainerInterface $c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));

    return $logger;
};

# DOCTRINE ENTITY MANAGER
$container[ORM\EntityManager::class] = function (ContainerInterface $container) {
    $settings = $container->get('settings')['doctrine'];
    $config = ORM\Tools\Setup::createAnnotationMetadataConfiguration(
        $settings['meta']['entity_path'],
        $settings['meta']['auto_generate_proxies'],
        $settings['meta']['proxy_dir'],
        $settings['meta']['cache'],
        false
    );

    $em = ORM\EntityManager::create($settings['connection'], $config);
    // Add custom functions
    $em->getConfiguration()->addCustomDatetimeFunction('date', Mysql\Date::class);
    $em->getConfiguration()->addCustomDatetimeFunction('date_format', Mysql\DateFormat::class);
    $em->getConfiguration()->addCustomDatetimeFunction('unix_timestamp', Mysql\UnixTimestamp::class);
    $em->getConfiguration()->addCustomStringFunction('ifnull', Mysql\IfNull::class);
    $em->getConfiguration()->addCustomStringFunction('if', Mysql\IfElse::class);

    // Entity subscribers
    $em->getEventManager()->addEventSubscriber($container->get('UserEntityMailchimpSyncSubscriber'));

    // Return entity manager
    return $em;
};

$container['em'] = function (ContainerInterface $container) {
    return $container->get(ORM\EntityManager::class);
};

$container[EventDispatcher::class] = function (ContainerInterface $container) {
    $dispatcher = new EventDispatcher();
    $dispatcher->addSubscriber($container->get(UserRegisteredEventSubscriber::class));

    return $dispatcher;
};

# EVENT DISPATCHER
$container['event_dispatcher'] = function (ContainerInterface $container) {
    $dispatcher = new EventDispatcher();

    return $dispatcher;
};

# CONTROLLERS
# ADMIN
$container['Controller_Admin_CourseCreate'] = function (ContainerInterface $container) {
    return new Controllers\Admin\CourseCreateController(
        $container->get(Provider\CourseProvider::class),
        $container->get(ORM\EntityManager::class)
    );
};

$container['Controller_Admin_CourseDelete'] = function (ContainerInterface $container) {
    return new Controllers\Admin\CourseDeleteController(
        $container->get(Repository\CourseRepository::class),
        $container->get(ORM\EntityManager::class)
    );
};

$container['Controller_Admin_CourseDetail'] = function (ContainerInterface $container) {
    return new Controllers\Admin\CourseDetailController(
        $container->get(Repository\CourseRepository::class)
    );
};

$container['Controller_Admin_CourseList'] = function (ContainerInterface $container) {
    return new Controllers\Admin\CourseListController(
        $container->get(Repository\CourseRepository::class)
    );
};

$container['Controller_Admin_CourseUpdate'] = function (ContainerInterface $container) {
    return new Controllers\Admin\CourseUpdateController(
        $container->get(Provider\CourseProvider::class),
        $container->get(Repository\CourseRepository::class),
        $container->get(ORM\EntityManager::class)
    );
};

$container['Controller_Admin_DailyMeditationCreate'] = function (ContainerInterface $container) {
    return new Controllers\Admin\DailyMeditationCreateController(
        $container->get(Provider\CourseProvider::class),
        $container->get(ORM\EntityManager::class)
    );
};

$container['Controller_Admin_DailyMeditationDelete'] = function (ContainerInterface $container) {
    return new Controllers\Admin\DailyMeditationDeleteController(
        $container->get(Repository\CourseRepository::class),
        $container->get(ORM\EntityManager::class)
    );
};

$container['Controller_Admin_DailyMeditationDetail'] = function (ContainerInterface $container) {
    return new Controllers\Admin\DailyMeditationDetailController(
        $container->get(Repository\CourseRepository::class)
    );
};

$container['Controller_Admin_DailyMeditationList'] = function (ContainerInterface $container) {
    return new Controllers\Admin\DailyMeditationListController(
        $container->get(Repository\CourseRepository::class)
    );
};

$container['Controller_Admin_DailyMeditationUpdate'] = function (ContainerInterface $container) {
    return new Controllers\Admin\DailyMeditationUpdateController(
        $container->get(Provider\CourseProvider::class),
        $container->get(Repository\CourseRepository::class),
        $container->get(ORM\EntityManager::class)
    );
};

$container['Controller_Admin_LessonCreate'] = function (ContainerInterface $container) {
    return new Controllers\Admin\LessonCreateController(
        $container->get(ORM\EntityManager::class),
        $container->get(Provider\LessonProvider::class)
    );
};

$container['Controller_Admin_LessonDelete'] = function (ContainerInterface $container) {
    return new Controllers\Admin\LessonDeleteController(
        $container->get('em'),
        $container->get('LessonRepository')
    );
};

$container['Controller_Admin_LessonDetail'] = function (ContainerInterface $container) {
    return new Controllers\Admin\LessonDetailController(
        $container->get('LessonRepository')
    );
};

$container['Controller_Admin_LessonList'] = function (ContainerInterface $container) {
    return new Controllers\Admin\LessonListController(
        $container->get('LessonRepository')
    );
};

$container['Controller_Admin_LessonUpdate'] = function (ContainerInterface $container) {
    return new Controllers\Admin\LessonUpdateController(
        $container->get(ORM\EntityManager::class),
        $container->get(Provider\LessonProvider::class),
        $container->get('LessonRepository')
    );
};

$container['Controller_Admin_PushNotification'] = function (ContainerInterface $container) {
    return new Controllers\Admin\PushNotificationController(
        $container->get(ORM\EntityManager::class),
        $container->get(Services\FirebaseMessageService\FirebaseMessageService::class),
        $container->get(Services\TokenStorage\TokenStorage::class)
    );
};

$container['Controller_Admin_PushNotificationList'] = function (ContainerInterface $container) {
    return new Controllers\Admin\PushNotificationListController(
        $container->get('PushNotificationRepository')
    );
};

$container['Controller_Admin_Static'] = function (ContainerInterface $container) {
    return new Controllers\Admin\StaticController(
        $container->get('ArticleRepository')
    );
};

$container['Controller_Admin_StaticUpdate'] = function (ContainerInterface $container) {
    return new Controllers\Admin\StaticUpdateController(
        $container->get('ArticleRepository'),
        $container->get('em')
    );
};

$container['Controller_Admin_UserDelete'] = function (ContainerInterface $container) {
    return new Controllers\Admin\UserDeleteController(
        $container->get('em'),
        $container->get(Services\TokenStorage\TokenStorage::class),
        $container->get('UserRepository')
    );
};

$container['Controller_Admin_UserLogin'] = function (ContainerInterface $container) {
    return new Controllers\Admin\UserLoginController(
        $container->get('em'),
        $container->get('UserRepository')
    );
};

$container['Controller_Admin_AppVersionUpdate'] = function (ContainerInterface $container) {
    return new Controllers\Admin\AppVersionUpdateController(
        $container->get('em'),
        $container->get('VersionRepository')
    );
};

$container['FileController'] = function (ContainerInterface $container) {
    return new Controllers\FileController(
        $container->get('FileRepository'),
        $container->get('FileService'),
        $container->get('settings')['fileDownloadUrl']
    );
};

$container['Controller_AppVersion'] = function (ContainerInterface $container) {
    return new Controllers\AppVersionController(
        $container->get('VersionRepository')
    );
};

$container['FileUploadController'] = function (ContainerInterface $container) {
    return new Controllers\FileUploadController(
        $container->get('settings')['downloadUri'],
        $container->get('FileService')
    );
};

$container['Controller_CourseDailyMeditationList'] = function (ContainerInterface $container) {
    return new Controllers\CourseDailyMeditationListController(
        $container->get('CourseRepository'),
        $container->get(Services\Storage\LanguageStorage::class)
    );
};

$container['Controller_CourseDetail'] = function (ContainerInterface $container) {
    return new Controllers\CourseDetailController(
        $container->get('CourseRepository')
    );
};

$container['Controller_CourseSign'] = function (ContainerInterface $container) {
    return new Controllers\CourseSignController(
        $container->get('em'),
        $container->get('CourseRepository'),
        $container->get(Services\TokenStorage\TokenStorage::class)
    );
};

$container['Controller_CourseUpdate'] = function (ContainerInterface $container) {
    return new Controllers\CourseUpdateController(
        $container->get('em'),
        $container->get('CourseRepository'),
        $container->get(Services\TokenStorage\TokenStorage::class)
    );
};

$container['Controller_LessonDetail'] = function (ContainerInterface $container) {
    return new Controllers\LessonDetailController(
        $container->get('LessonRepository')
    );
};

$container['Controller_LessonFinish'] = function (ContainerInterface $container) {
    return new Controllers\LessonFinishController(
        $container->get('CourseRepository'),
        $container->get('em'),
        $container->get(Services\Storage\LanguageStorage::class),
        $container->get(Repository\LecturerRepository::class),
        $container->get(Services\TokenStorage\TokenStorage::class)
    );
};

$container['Controller_StaticDetail'] = function (ContainerInterface $container) {
    return new Controllers\StaticDetailController(
        $container->get('ArticleRepository')
    );
};

$container['Controller_UserLoginOAuth'] = function (ContainerInterface $container) {
    return new Controllers\UserLoginOAuthController(
        $container->get('em'),
        $container->get('FacebookService'),
        $container->get('GoogleService'),
        $container->get('UserRepository')
    );
};

$container['Controller_UserLoginPassword'] = function (ContainerInterface $container) {
    return new Controllers\UserLoginPasswordController(
        $container->get('em'),
        $container->get('UserRepository')
    );
};

$container['Controller_UserPurchaseAnalytics'] = function (ContainerInterface $container) {
    return new Controllers\UserPurchaseAnalyticsController(
        $container->get('AnalyticsRepository'),
        $container->get('UserRepository'),
        $container->get('em')
    );
};

$container['Controller_UserPing'] = function (ContainerInterface $container) {
    return new Controllers\UserPingController(
        $container->get('em'),
        $container->get(Services\TokenStorage\TokenStorage::class)
    );
};

$container['Controller_UserRegistrationOAuth'] = function (ContainerInterface $container) {
    return new Controllers\UserRegistrationOAuthController(
        $container->get('em'),
        $container->get('UserRepository')
    );
};

$container['Controller_UserResetPassword'] = function (ContainerInterface $container) {
    return new Controllers\UserResetPasswordController(
        $container->get('settings')['apiUri'],
        $container->get('em'),
        $container->get(Services\MailService\MailService::class),
        $container->get(Translator::class),
        $container->get('UserRepository')
    );
};

$container['Controller_UserSavePassword'] = function (ContainerInterface $container) {
    return new Controllers\UserSavePasswordController(
        $container->get('em'),
        $container->get(Twig::class),
        $container->get('UserRepository')
    );
};

$container['Controller_UserSetPassword'] = function (ContainerInterface $container) {
    return new Controllers\UserSetPasswordController(
        $container->get(Twig::class),
        $container->get('UserRepository')
    );
};

$container['Controller_UsersMailchimpSync'] = function (ContainerInterface $container) {
    return new Controllers\UsersMailchimpSyncController(
        $container->get('em'),
        $container->get('MailchimpService'),
        $container->get('UserRepository')
    );
};

# REPOSITORY
$container['ArticleRepository'] = function (ContainerInterface $container) {
    return new Repository\ArticleRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Article::class)
    );
};

$container['AnalyticsRepository'] = function (ContainerInterface $container) {
    return new Repository\AnalyticsRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Analytics::class)
    );
};

$container['CourseRepository'] = function (ContainerInterface $container) {
    return new Repository\CourseRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Course::class)
    );
};

$container['DailyMeditationRepository'] = function (ContainerInterface $container) {
    return new Repository\DailyMeditationRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\DailyMeditation::class)
    );
};

$container['FileRepository'] = function (ContainerInterface $container) {
    return new Repository\FileRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\FileEntity::class)
    );
};

$container['PushNotificationRepository'] = function (ContainerInterface $container) {
    return new Repository\PushNotificationRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\PushNotification::class)
    );
};

$container['RoleRepository'] = function (ContainerInterface $container) {
    return new Repository\RoleRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Role::class)
    );
};

$container['LanguageRepository'] = function (ContainerInterface $container) {
    return new Repository\LanguageRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Language::class)
    );
};

$container['LessonRepository'] = function (ContainerInterface $container) {
    return new Repository\LessonRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\LessonEntity::class)
    );
};

$container['TagRepository'] = function (ContainerInterface $container) {
    return new Repository\TagRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Tag::class)
    );
};

$container['UserRepository'] = function (ContainerInterface $container) {
    return new Repository\UserRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\UserEntity::class)
    );
};

$container['VersionRepository'] = function (ContainerInterface $container) {
    return new Repository\VersionRepository(
        $container->get('em'),
        $container->get('em')->getClassMetadata(Entity\Version::class)
    );
};

# SERVICES
$container[Services\AuthenticationService\AuthenticationService::class] = function (ContainerInterface $container) {
    return new Services\AuthenticationService\AuthenticationService(
        $container->get(Services\Storage\DeviceStorage::class),
        $container->get(ORM\EntityManager::class),
        $container->get(Repository\TokenRepository::class),
        $container->get(Services\TokenStorage\TokenStorage::class),
        $container->get(UserDeviceFactory::class)
    );
};

$container[Services\AuthorizationService\AuthorizationService::class] = function (ContainerInterface $container) {
    return new Services\AuthorizationService\AuthorizationService(
        $container->get(ORM\EntityManager::class),
        $container->get(Services\TokenStorage\TokenStorage::class)
    );
};

$container['FacebookService'] = function (ContainerInterface $container) {
    return new Services\FacebookService\FacebookService(
        $container->get('settings')['facebookApp']
    );
};

$container['FileService'] = function (ContainerInterface $container) {
    return new Services\FileService\FileService(
        $container->get('settings')['uploadsDirectory'],
        $container->get('FileRepository')
    );
};

$container['GoogleService'] = function (ContainerInterface $container) {
    return new Services\GoogleService\GoogleService();
};

$container['MailchimpService'] = function (ContainerInterface $container) {
    return new Services\MailchimpService\MailchimpService(
        $container->get('settings')['mailchimp']
    );
};

$container['Service_IpChecker'] = function (ContainerInterface $container) {
    return new Services\IpChecker\IpChecker(
        $container->get('settings')['cronWhitelist']
    );
};

# ENTITY SUBSCRIBER
$container['UserEntityMailchimpSyncSubscriber'] = function (ContainerInterface $container) {
    return new UserEntityMailchimpSyncSubscriber(
        $container->get('MailchimpService')
    );
};

require_once __DIR__ . '/dependencies.commands.php';
require_once __DIR__ . '/dependencies.controllers.php';
require_once __DIR__ . '/dependencies.cron_scheduler.php';
require_once __DIR__ . '/dependencies.event_subscribers.php';
require_once __DIR__ . '/dependencies.factories.php';
require_once __DIR__ . '/dependencies.providers.php';
require_once __DIR__ . '/dependencies.middleware.php';
require_once __DIR__ . '/dependencies.repositories.php';
require_once __DIR__ . '/dependencies.services.php';
require_once __DIR__ . '/dependencies.transformers.php';
