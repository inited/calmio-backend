<?php

declare(strict_types=1);

/** @var ContainerInterface $container */

use App\Model\Entity;
use App\Model\Repository;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

$container[Repository\CourseRepository::class] = function (ContainerInterface $container) {
    return new Repository\CourseRepository(
        $container->get(EntityManager::class),
        $container->get(EntityManager::class)->getClassMetadata(Entity\Course::class)
    );
};

$container[Repository\DeviceRepository::class] = function (ContainerInterface $container) {
    return new Repository\DeviceRepository(
        $container->get(EntityManager::class),
        $container->get(EntityManager::class)->getClassMetadata(Entity\Device::class)
    );
};

$container[Repository\FeedbackRepository::class] = function (ContainerInterface $container) {
    return new Repository\FeedbackRepository(
        $container->get(EntityManager::class),
        $container->get(EntityManager::class)->getClassMetadata(Entity\Feedback::class)
    );
};

$container[Repository\LanguageRepository::class] = function (ContainerInterface $container) {
    return new Repository\LanguageRepository(
        $container->get(EntityManager::class),
        $container->get(EntityManager::class)->getClassMetadata(Entity\Language::class)
    );
};

$container[Repository\LecturerRepository::class] = function (ContainerInterface $container) {
    return new Repository\LecturerRepository(
        $container->get(EntityManager::class),
        $container->get(EntityManager::class)->getClassMetadata(Entity\Lecturer::class)
    );
};

$container[Repository\PartnerRepository::class] = function (ContainerInterface $container) {
    return new Repository\PartnerRepository(
        $container->get(EntityManager::class),
        $container->get(EntityManager::class)->getClassMetadata(Entity\Partner::class)
    );
};

$container[Repository\PromoCodeRepository::class] = function (ContainerInterface $container) {
    return new Repository\PromoCodeRepository(
        $container->get(EntityManager::class),
        $container->get(EntityManager::class)->getClassMetadata(Entity\PromoCode::class)
    );
};

$container[Repository\RoleRepository::class] = function (ContainerInterface $container) {
    return new Repository\RoleRepository(
        $container->get(EntityManager::class),
        $container->get(EntityManager::class)->getClassMetadata(Entity\Role::class)
    );
};

$container[Repository\SubscriptionRepository::class] = function (ContainerInterface $container) {
    return new Repository\SubscriptionRepository(
        $container->get(EntityManager::class),
        $container->get(EntityManager::class)->getClassMetadata(Entity\Subscription::class)
    );
};

$container[Repository\TagRepository::class] = function (ContainerInterface $container) {
    return new Repository\TagRepository(
        $container->get(EntityManager::class),
        $container->get(EntityManager::class)->getClassMetadata(Entity\Tag::class)
    );
};

$container[Repository\TokenRepository::class] = function (ContainerInterface $container) {
    return new Repository\TokenRepository(
        $container->get(EntityManager::class),
        $container->get(EntityManager::class)->getClassMetadata(Entity\TokenEntity::class)
    );
};

$container[Repository\UserRepository::class] = function (ContainerInterface $container) {
    return new Repository\UserRepository(
        $container->get(EntityManager::class),
        $container->get(EntityManager::class)->getClassMetadata(Entity\UserEntity::class)
    );
};

