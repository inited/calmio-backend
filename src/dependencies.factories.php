<?php

use App\Model\Factory\DeviceFactory;
use App\Model\Factory\LecturerFactory;
use App\Model\Factory\PromoCodeFactory;
use App\Model\Factory\SubscriptionFactory;
use App\Model\Factory\SubscriptionTransactionFactory;
use App\Model\Factory\UserDeviceFactory;

$container[DeviceFactory::class] = function () {
    return new DeviceFactory();
};

$container[LecturerFactory::class] = function () {
    return new LecturerFactory();
};

$container[PromoCodeFactory::class] = function () {
    return new PromoCodeFactory();
};

$container[SubscriptionFactory::class] = function () {
    return new SubscriptionFactory();
};

$container[SubscriptionTransactionFactory::class] = function () {
    return new SubscriptionTransactionFactory();
};

$container[UserDeviceFactory::class] = function () {
    return new UserDeviceFactory();
};
