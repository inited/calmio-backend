<?php

use App\Controllers;
use App\Middleware;
use App\Services\AuthenticationService\AuthenticationService;
use App\Services\AuthorizationService\AuthorizationService;
use Psr\Container\ContainerInterface;
use RKA\Middleware\IpAddress;
use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Routes
 * @author Tomas Pavlik <info@tomaspavlik.cz>
 * @var App $app
 * @var ContainerInterface $container
 */
$app->options('/{routes:.+}', function (Request $request, Response $response, array $args) {
    return $response;
});

// Middleware
$app->add($container->get(Middleware\Cors::class));
$app->add($container->get(Middleware\Device::class));
$app->add($container->get(Middleware\Language::class));
$app->add($container->get(IpAddress::class));

// API
$app->group('/api', function () use ($container) {
    // App
    $this->get('/app-version', 'Controller_AppVersion:defaultAction');

    // Admin login
    $this->post('/admin/login-password', 'Controller_Admin_UserLogin:defaultAction');

    // Feedback
    $this->post('/feedback', Controllers\FeedbackController::class);

    // Mailchimp
    $this->get('/users-mailchimp-sync', 'Controller_UsersMailchimpSync:defaultAction'); // Synchronizace vsech uzivatelu na Mailchimp

    // Partner name
    $this->get('/partner-code/{id}', Controllers\PartnerCodeController::class);

    // Static
    $this->get('/static', 'Controller_StaticDetail:defaultAction');

    // User
    $this->group('/{language}/user', function () {
        $this->post('/save-new-password', 'Controller_UserSavePassword:defaultAction');
        $this->get('/set-password/{hash}', 'Controller_UserSetPassword:defaultAction');
        $this->get('/activation/{hash}', Controllers\UserAccountActivationController::class);
    });

    $this->group('/user', function () {
        $this->post('/login-oauth', 'Controller_UserLoginOAuth:defaultAction');
        $this->post('/login-password', 'Controller_UserLoginPassword:defaultAction');
        $this->post('/registration-password', Controllers\UserRegistrationPasswordController::class);
        $this->post('/reset-password', 'Controller_UserResetPassword:defaultAction');
    });

    // Secured routes
    $this->group('', function () use ($container) {
        // Admin
        $this->group('/admin', function () use ($container) {
            // Courses
            $this->get('/courses', 'Controller_Admin_CourseList:defaultAction');
            $this->delete('/courses/{id}', 'Controller_Admin_CourseDelete:defaultAction');
            $this->get('/courses/{id}', 'Controller_Admin_CourseDetail:defaultAction');
            $this->put('/courses/{id}', 'Controller_Admin_CourseUpdate:defaultAction');
            $this->post('/courses', 'Controller_Admin_CourseCreate:defaultAction');
            // Daily meditation
            $this->get('/daily-meditation', 'Controller_Admin_DailyMeditationList:defaultAction');
            $this->delete('/daily-meditation/{id}', 'Controller_Admin_DailyMeditationDelete:defaultAction');
            $this->get('/daily-meditation/{id}', 'Controller_Admin_DailyMeditationDetail:defaultAction');
            $this->put('/daily-meditation/{id}', 'Controller_Admin_DailyMeditationUpdate:defaultAction');
            $this->put('/daily-meditation/{id}/tomorrow', Controllers\Admin\DailyMeditationTomorrowController::class);
            $this->post('/daily-meditation', 'Controller_Admin_DailyMeditationCreate:defaultAction');
            // File
            $this->post('/file-upload', 'FileUploadController:defaultAction');

            // Lecturer
            $this->get('/lecturers', Controllers\Admin\Lecturer\LecturerListController::class);
            $this->delete('/lecturer/{id}', Controllers\Admin\Lecturer\LecturerDeleteController::class);
            $this->get('/lecturer/{id}', Controllers\Admin\Lecturer\LecturerDetailController::class);
            $this->put('/lecturer/{id}', Controllers\Admin\Lecturer\LecturerUpdateController::class);
            $this->post('/lecturer', Controllers\Admin\Lecturer\LecturerCreateController::class);

            // Partner
            $this->get('/partners', Controllers\Admin\PartnerListController::class);
            $this->get('/partner/{id}', Controllers\Admin\PartnerController::class);
            $this->delete('/partner/{id}', Controllers\Admin\PartnerDeleteController::class);
            $this->post('/partner', Controllers\Admin\PartnerCreateController::class);
            $this->post('/partner/{id}/code-create', Controllers\Admin\PartnerCodeCreateController::class);
            $this->put('/partner/{id}/code/{code}', Controllers\Admin\PartnerCodeUpdateController::class);
            $this->delete('/partner/{id}/code/{code}', Controllers\Admin\PartnerCodeDeleteController::class);
            $this->put('/partner/{id}', Controllers\Admin\PartnerUpdateController::class);

            // Push notification
            $this->post('/push-notification', 'Controller_Admin_PushNotification:defaultAction');
            $this->get('/push-notifications', 'Controller_Admin_PushNotificationList:defaultAction');

            // Static
            $this->get('/static', 'Controller_Admin_Static:defaultAction');
            $this->post('/static', 'Controller_Admin_StaticUpdate:defaultAction');

            // Statistics
            $this->group('/statistics', function () {
                $this->get('/users', Controllers\Admin\StatisticsUsersController::class);
                $this->get('/export', Controllers\Admin\StatisticsExportController::class);
            });

            // User
            $this->delete('/users', 'Controller_Admin_UserDelete:defaultAction');
            $this->get('/users', Controllers\Admin\UserListController::class);
            $this->put('/users', Controllers\Admin\UserUpdateController::class);
            // Version
            $this->put('/app-version', 'Controller_Admin_AppVersionUpdate:defaultAction');
        })->add($container->get(AuthorizationService::class));

        // Course
        $this->get('/courses-all', 'Controller_CourseDailyMeditationList:defaultAction');
        $this->get('/courses/{id}/sign', 'Controller_CourseSign:defaultAction');
        $this->get('/courses/{id}', 'Controller_CourseDetail:defaultAction');
        $this->put('/courses/{id}', 'Controller_CourseUpdate:defaultAction');

        // Lesson
        $this->get('/lessons/{id}', 'Controller_LessonDetail:defaultAction');
        $this->post('/lessons/finish', 'Controller_LessonFinish:defaultAction');

        // Purchase, subscription verification
        $this->post('/purchase-subscription-verification', Controllers\PurchaseSubscriptionVerificationController::class);

        // User
        $this->group('/user', function () {
            $this->delete('', Controllers\UserController::class . ':destroy');
            $this->get('', Controllers\UserController::class . ':show');
            $this->put('', Controllers\UserUpdateController::class);
            $this->post('/add-push-token', Controllers\UserAddPushTokenController::class);
            $this->post('/apply-partner-code', Controllers\UserApplyPromoCodeController::class . ':promoAction');
            $this->post('/apply-voucher-code', Controllers\UserApplyPromoCodeController::class . ':giftAction');
            $this->post('/ping', 'Controller_UserPing:defaultAction');
            $this->post('/purchase-analytics', 'Controller_UserPurchaseAnalytics:defaultAction');
            $this->post('/resend-verification-email', Controllers\UserResendVerificationEmailController::class);
        });
    })->add($container->get(AuthenticationService::class));
});
