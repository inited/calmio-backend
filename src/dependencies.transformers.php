<?php

/** @var ContainerInterface $container */

use App\Model\Transformer\UserTransformer;
use App\Services;
use Psr\Container\ContainerInterface;

$container[UserTransformer::class] = function (ContainerInterface $container) {
    return new UserTransformer(
        $container->get(Services\Storage\DeviceStorage::class)
    );
};
