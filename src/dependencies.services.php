<?php

declare(strict_types=1);

/** @var ContainerInterface $container */

use App\Model\Factory\SubscriptionFactory;
use App\Model\Factory\SubscriptionTransactionFactory;
use App\Model\Repository;
use App\Model\ValueObject\GoogleSettings;
use App\Services;
use App\Services\PurchaseSubscriptionValidation\PurchaseValidationService;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig;
use sngrl\PhpFirebaseCloudMessaging\Client as FirebaseClient;

$container[FirebaseClient::class] = function (ContainerInterface $container) {
    /** @var GoogleSettings $googleSettings */
    $googleSettings = $container->get('settings')['fcm_server_key'];

    $client = new FirebaseClient();
    $client->setApiKey($googleSettings->getApiKey());

    return $client;
};

$container[Services\FirebaseMessageService\FirebaseMessageService::class] = function (ContainerInterface $container) {
    return new Services\FirebaseMessageService\FirebaseMessageService(
        $container->get(FirebaseClient::class)
    );
};

$container[Services\Job\CheckAvailableGiftVoucherCodes::class] = function (ContainerInterface $container) {
    return new Services\Job\CheckAvailableGiftVoucherCodes(
        $container->get(Repository\PromoCodeRepository::class)
    );
};

$container[Services\Job\SetDailyMeditationForEachLanguage::class] = function (ContainerInterface $container) {
    return new Services\Job\SetDailyMeditationForEachLanguage(
        $container->get(EntityManager::class),
        $container->get(Repository\CourseRepository::class),
        $container->get(Repository\LanguageRepository::class)
    );
};

$container[Services\Job\VerifySubscriptions::class] = function (ContainerInterface $container) {
    return new Services\Job\VerifySubscriptions(
        $container->get(PurchaseValidationService::class),
        $container->get(Repository\SubscriptionRepository::class)
    );
};

$container[Services\MailService\MailService::class] = function (ContainerInterface $container) {
    return new Services\MailService\MailService(
        $container->get('settings')['mail'],
        $container->get(Twig::class)
    );
};

$container[PurchaseValidationService::class] = function (ContainerInterface $container) {
    return new Services\PurchaseSubscriptionValidation\PurchaseValidationService(
        $container->get(EntityManager::class),
        $container->get(Services\PurchaseSubscriptionValidation\AndroidValidator::class),
        $container->get(Services\PurchaseSubscriptionValidation\IosValidator::class),
        $container->get(SubscriptionFactory::class),
        $container->get(SubscriptionTransactionFactory::class),
        $container->get(Repository\SubscriptionRepository::class),
        $container->get(App\Services\TokenStorage\TokenStorage::class)
    );
};

$container[Services\Statistics\UserStatistics::class] = function (ContainerInterface $container) {
    return new Services\Statistics\UserStatistics();
};

$container[Services\Statistics\StatisticsExport::class] = function (ContainerInterface $container) {
    return new Services\Statistics\StatisticsExport(
        $container->get(Repository\CourseRepository::class),
        $container->get(Repository\UserRepository::class),
        $container->get(Services\Statistics\UserStatistics::class)
    );
};

# STORAGES
$container[Services\Storage\DeviceStorage::class] = function () {
    return new Services\Storage\DeviceStorage();
};

$container[Services\Storage\LanguageStorage::class] = function () {
    return new Services\Storage\LanguageStorage();
};

$container[Services\TokenStorage\TokenStorage::class] = function () {
    return new App\Services\TokenStorage\TokenStorage();
};

$container[Services\PurchaseSubscriptionValidation\AndroidValidator::class] = function () {
    return new Services\PurchaseSubscriptionValidation\AndroidValidator();
};

$container[Services\PurchaseSubscriptionValidation\IosValidator::class] = function () {
    return new Services\PurchaseSubscriptionValidation\IosValidator();
};
