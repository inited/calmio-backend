<?php

declare(strict_types=1);

use App\Services\Job\CheckAvailableGiftVoucherCodes;
use App\Services\Job\SetDailyMeditationForEachLanguage;
use App\Services\Job\VerifySubscriptions;
use GO\Scheduler;
use Psr\Container\ContainerInterface;

$container[Scheduler::class] = function (ContainerInterface $container) {
    $scheduler = new Scheduler([
        "tempDir" => $container->get("settings")["tmpDirectory"],
    ]);

    $scheduler
        ->call(function () use ($container) {
            /** @var  SetDailyMeditationForEachLanguage $job */
            $job = $container->get(SetDailyMeditationForEachLanguage::class);
            $job->run();
        })
        ->daily();

    $scheduler
        ->call(function () use ($container) {
            /** @var  VerifySubscriptions $job */
            $job = $container->get(VerifySubscriptions::class);
            $job->run();
        })
        ->daily(0, 1);

    $scheduler
        ->call(function () use ($container) {
            /** @var  CheckAvailableGiftVoucherCodes $job */
            $job = $container->get(CheckAvailableGiftVoucherCodes::class);
            $job->run();
        })
        ->daily(1);

    return $scheduler;
};
