<?php

use App\Model\ValueObject\FacebookSettings;
use App\Model\ValueObject\GoogleSettings;
use Doctrine\ORM\Query;
use Monolog\Logger;

$serverUri = $_ENV['WEB_URL'] ?? 'https://calmio.iniapps.com/';
$apiUri = $serverUri . 'api/';

return [
    'settings' => [
        'cronWhitelist' => [
            '127.0.0.1', // localhost
            '217.170.105.135', // T.Pavlik
        ],
        'determineRouteBeforeAppMiddleware' => true,
        'feedbackEmail' => 'calmiollcapp@gmail.com',
        'serverUri' => $serverUri,
        'assetsBaseUri' => $apiUri,
        'apiUri' => $apiUri,
        'facebookApp' => new FacebookSettings('2520499468236358', '21990f2be669ae2d01639c78530db79d'),
        'fcm_server_key' => new GoogleSettings('AAAAkvbEQCM:APA91bGMfJsUV2OAJVA8Ikj4n6kCxIEaBdkO9YnyV6gjMk_Dg8B5-6c2NFJHkK9RXmN48K9aL13GoVPD41lc5E4LYnnv3Aj4y63ujN1CNHXd4mY-dCBzzyxe71RfGIz5L64zYJ657e6l'),
        'tmpDirectory' => __DIR__ . '/../temp/',
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'debug' => false,
        'uploadsDirectory' => __DIR__ . '/../public/uploads/',
        'downloadUri' => $apiUri . 'uploads/',
        'sentryDsn' => $_ENV['SENTRY_DSN'] ?? 'https://c0de576d0feb4598b315a4f4f0a9bd61@o329963.ingest.sentry.io/4503998982782976',

        // MailChimp
        'mailchimp' => [
            'apiKey' => '93f55c48dead087e894e9caed9c3b763-us4',
            'listId' => '48c36979ba',
            'mergeFields' => [
                'LAST_ACTIVITY' => 'MMERGE6',
                'REG_DATE' => 'MMERGE7',
            ],
        ],

        // Mail
        'mail' => [
            'from' => 'no-reply@calmio.cz',
            'fromName' => 'Calmio',
            'smtp' => true,
            'host' => 'ini.inited.cz',
            'port' => 1935,
            //'secure'=> 'tls',
            //'username' => '',
            //'password' => '',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => Logger::DEBUG,
        ],

        // Doctrine
        'doctrine' => [
            'connection' => [
                'driver' => $_ENV['DB_DRIVER'] ?? 'pdo_mysql',
                'host' => $_ENV['MYSQL_HOSTNAME'] ?? 'localhost',
                'dbname' => $_ENV['MYSQL_DATABASE'] ?? 'ki196499_calmio',
                'user' => $_ENV['MYSQL_USER'] ?? 'ki196499_calmio',
                'password' => $_ENV['MYSQL_PASSWORD'] ?? 'pqK?TySYvlNd',
                'charset' => 'utf8',
                'driverOptions' => [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                ],
            ],
            'meta' => [
                'entity_path' => [
                    'app/Model/Entity/',
                ],
                'auto_generate_proxies' => true,
                'cache' => null,
                'proxy_dir' => __DIR__ . '/../temp/cache/proxies',
                'customHydrationModes' => Query::HYDRATE_ARRAY,
            ],
        ],

        // Twig
        'twig' => [
            'cache' => __DIR__ . '/../temp/',
        ],
    ],
];
