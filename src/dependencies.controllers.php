<?php

declare(strict_types=1);

/** @var ContainerInterface $container */

use App\Controllers;
use App\Controllers\Admin;
use App\Model\Factory;
use App\Model\Provider;
use App\Model\Repository;
use App\Model\Transformer;
use App\Services;
use Doctrine\ORM;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig;
use Symfony\Component\EventDispatcher\EventDispatcher;

// Daily meditation
$container[Admin\DailyMeditationTomorrowController::class] = function (ContainerInterface $container) {
    return new Admin\DailyMeditationTomorrowController(
        $container->get(Repository\CourseRepository::class),
        $container->get(ORM\EntityManager::class)
    );
};

// Lecturer
$container[Admin\Lecturer\LecturerCreateController::class] = function (ContainerInterface $container) {
    return new Admin\Lecturer\LecturerCreateController(
        $container->get(ORM\EntityManager::class),
        $container->get(Factory\LecturerFactory::class),
        $container->get(Provider\LecturerProvider::class)
    );
};

$container[Admin\Lecturer\LecturerDeleteController::class] = function (ContainerInterface $container) {
    return new Admin\Lecturer\LecturerDeleteController(
        $container->get(ORM\EntityManager::class),
        $container->get(Repository\LecturerRepository::class)
    );
};

$container[Admin\Lecturer\LecturerDetailController::class] = function (ContainerInterface $container) {
    return new Admin\Lecturer\LecturerDetailController(
        $container->get(Repository\LecturerRepository::class)
    );
};

$container[Admin\Lecturer\LecturerListController::class] = function (ContainerInterface $container) {
    return new Admin\Lecturer\LecturerListController(
        $container->get(Repository\LecturerRepository::class)
    );
};

$container[Admin\Lecturer\LecturerUpdateController::class] = function (ContainerInterface $container) {
    return new Admin\Lecturer\LecturerUpdateController(
        $container->get(ORM\EntityManager::class),
        $container->get(Provider\LecturerProvider::class),
        $container->get(Repository\LecturerRepository::class)
    );
};

// Partner
$container[Admin\PartnerController::class] = function (ContainerInterface $container) {
    return new Admin\PartnerController(
        $container->get(Repository\PartnerRepository::class)
    );
};

$container[Admin\PartnerCodeCreateController::class] = function (ContainerInterface $container) {
    return new Admin\PartnerCodeCreateController(
        $container->get(ORM\EntityManager::class),
        $container->get(Repository\PartnerRepository::class)
    );
};

$container[Admin\PartnerCodeDeleteController::class] = function (ContainerInterface $container) {
    return new Admin\PartnerCodeDeleteController(
        $container->get(ORM\EntityManager::class),
        $container->get(Repository\PartnerRepository::class)
    );
};

$container[Admin\PartnerCodeUpdateController::class] = function (ContainerInterface $container) {
    return new Admin\PartnerCodeUpdateController(
        $container->get(ORM\EntityManager::class),
        $container->get(Repository\PartnerRepository::class)
    );
};

$container[Admin\PartnerCreateController::class] = function (ContainerInterface $container) {
    return new Admin\PartnerCreateController(
        $container->get(ORM\EntityManager::class),
        $container->get(Provider\PartnerProvider::class)
    );
};

$container[Admin\PartnerDeleteController::class] = function (ContainerInterface $container) {
    return new Admin\PartnerDeleteController(
        $container->get(ORM\EntityManager::class),
        $container->get(Repository\PartnerRepository::class)
    );
};

$container[Admin\PartnerListController::class] = function (ContainerInterface $container) {
    return new Admin\PartnerListController(
        $container->get(Repository\PartnerRepository::class)
    );
};

$container[Admin\PartnerUpdateController::class] = function (ContainerInterface $container) {
    return new Admin\PartnerUpdateController(
        $container->get(ORM\EntityManager::class),
        $container->get(Provider\PartnerProvider::class),
        $container->get(Repository\PartnerRepository::class)
    );
};

$container[Controllers\Admin\UserListController::class] = function (ContainerInterface $container) {
    return new Controllers\Admin\UserListController(
        $container->get(Repository\UserRepository::class),
        $container->get(Transformer\UserTransformer::class)
    );
};

$container[Controllers\Admin\UserUpdateController::class] = function (ContainerInterface $container) {
    return new Controllers\Admin\UserUpdateController(
        $container->get(ORM\EntityManager::class),
        $container->get(Repository\RoleRepository::class),
        $container->get(Repository\TagRepository::class),
        $container->get(Repository\UserRepository::class),
        $container->get(Transformer\UserTransformer::class)
    );
};

// USER
$container[Controllers\UserController::class] = function (ContainerInterface $container) {
    return new Controllers\UserController(
        $container->get(ORM\EntityManager::class),
        $container->get(Services\TokenStorage\TokenStorage::class),
        $container->get(Transformer\UserTransformer::class)
    );
};

$container[Controllers\UserUpdateController::class] = function (ContainerInterface $container) {
    return new Controllers\UserUpdateController(
        $container->get(ORM\EntityManager::class),
        $container->get(Repository\TagRepository::class),
        $container->get(Services\TokenStorage\TokenStorage::class),
        $container->get(Transformer\UserTransformer::class)
    );
};

$container[Controllers\PurchaseSubscriptionVerificationController::class] = function (ContainerInterface $container) {
    return new Controllers\PurchaseSubscriptionVerificationController(
        $container->get(Services\Storage\DeviceStorage::class),
        $container->get(ORM\EntityManager::class),
        $container->get(Services\PurchaseSubscriptionValidation\PurchaseValidationService::class),
        $container->get(Services\Validation\ValidationFactory::class)
    );
};

$container[Controllers\FeedbackController::class] = function (ContainerInterface $container) {
    return new Controllers\FeedbackController(
        $container->get('settings')['feedbackEmail'],
        $container->get(ORM\EntityManager::class),
        $container->get(Services\MailService\MailService::class)
    );
};

$container[Controllers\PartnerCodeController::class] = function (ContainerInterface $container) {
    return new Controllers\PartnerCodeController(
        $container->get(Repository\PromoCodeRepository::class)
    );
};

$container[Admin\StatisticsExportController::class] = function (ContainerInterface $container) {
    return new Admin\StatisticsExportController(
        $container->get(Services\Statistics\StatisticsExport::class)
    );
};

$container[Admin\StatisticsUsersController::class] = function (ContainerInterface $container) {
    return new Admin\StatisticsUsersController(
        $container->get(Repository\UserRepository::class),
        $container->get(Services\Statistics\UserStatistics::class)
    );
};

$container[Controllers\UserAddPushTokenController::class] = function (ContainerInterface $container) {
    return new Controllers\UserAddPushTokenController(
        $container->get(Services\Storage\DeviceStorage::class),
        $container->get(ORM\EntityManager::class)
    );
};

$container[Controllers\UserAccountActivationController::class] = function (ContainerInterface $container) {
    return new Controllers\UserAccountActivationController(
        $container->get(ORM\EntityManager::class),
        $container->get(Twig::class),
        $container->get(Repository\UserRepository::class)
    );
};

$container[Controllers\UserApplyPromoCodeController::class] = function (ContainerInterface $container) {
    return new Controllers\UserApplyPromoCodeController(
        $container->get(ORM\EntityManager::class),
        $container->get(Repository\PromoCodeRepository::class),
        $container->get(Services\TokenStorage\TokenStorage::class),
        $container->get(Transformer\UserTransformer::class)
    );
};

$container[Controllers\UserRegistrationPasswordController::class] = function (ContainerInterface $container) {
    return new Controllers\UserRegistrationPasswordController(
        $container->get(ORM\EntityManager::class),
        $container->get(EventDispatcher::class),
        $container->get(Repository\UserRepository::class)
    );
};

$container[Controllers\UserResendVerificationEmailController::class] = function (ContainerInterface $container) {
    return new Controllers\UserResendVerificationEmailController(
        $container->get(ORM\EntityManager::class),
        $container->get(EventDispatcher::class),
        $container->get(Services\TokenStorage\TokenStorage::class)
    );
};
