## CALMIO API

### Commands
#### Generování dárkových kódů
- četnost: manuálně dle potřeby
- příkaz přes CLI `php bin/console.php calmio:generate-gift-codes`

#### Nastavení denní metidace pro každý jazyk
- četnost: 1x denně
- příkaz přes CLI `php bin/console.php calmio:set-daily-meditation-for-each-language`

#### Ověřování Google/Apple předplatného
- četnost: 1x denně
- příkaz přes CLI `php bin/console.php calmio:verify-subscriptions`
