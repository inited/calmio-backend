/* View statistics_users */
SELECT MD5(`u`.`email`) md5, `u`.`email`, `u`.`name`, `u`.`surname`, IF(`user__role`.`role_id` IS NULL, 0, 1) `testing_account`,
`u`.`created` AS `registered_at`, `cu`.`preferred_voice`, COALESCE(ROUND(sum(`cul`.`time`)/60),0) AS `total_minutes`,
(
    SELECT count(`course__user_lesson`.`id`)
    FROM `course__user_lesson`
    INNER JOIN `course__user` ON `course__user`.`id`=`course__user_lesson`.`courseUser_Id`
    WHERE `course__user_lesson`.`completed`=1 AND `course__user`.`user_id`=`u`.`id`
) `lessons_finished`,
(
    SELECT count(`course_id`)
    FROM `statistics_partial_courses_lessons_users`
    WHERE `user_id`=`u`.`id` AND `lessons_completed`=`lessons_count`
) `courses_finished`,
GROUP_CONCAT(DISTINCT `ut`.`tag_id`) `reasons`, `u`.`experience_id` as `experience`
FROM `user__user` `u`
LEFT JOIN `course__user` `cu` ON `cu`.`user_id`=`u`.`id`
LEFT JOIN `course__user_lesson` `cul` ON `cul`.`courseUser_id`=`cu`.`id`
LEFT JOIN `analytics__analytics` `aa` ON `aa`.`user_id`=`u`.`id`
LEFT JOIN `user__tag` `ut` ON `ut`.`userentity_id`=`u`.`id`
LEFT JOIN `user__role` ON `user__role`.`userentity_id`=`u`.`id` AND `user__role`.`role_id`='TEST'
WHERE 1
GROUP BY `u`.`id`

/* View statistics_users_content */
SELECT `u`.`email` AS `email`,DATE_FORMAT(`cul`.`finished`, '%Y-%m-%d') AS `date`,
DATE_FORMAT(`cul`.`finished`,'%H:%i:%s') AS `time`,`c`.`name` AS `course_name`,`l`.`id` AS `lesson_id`,
`l`.`order` AS `lesson_order`,`cu`.`preferred_voice`,`cul`.`lesson_duration_seconds`,
`cul`.`current_session_time` AS `completed_time_seconds`, `la`.`id` AS `language`,
`le`.`name` AS `voice_name`
FROM `course__user` `cu`
INNER JOIN `user__user` `u` ON `u`.`id`=`cu`.`user_id`
INNER JOIN `course__course` `c` ON `c`.`id`=`cu`.`course_id`
INNER JOIN `course__user_lesson` `cul` ON `cul`.`courseUser_id`=`cu`.`id`
INNER JOIN `lesson__lesson` `l` ON `l`.`id`=`cul`.`lesson_id`
INNER JOIN `system__language` `la` ON `la`.`id`=`cul`.`language_id`
INNER JOIN `lecturer__lecturer` `le` ON `le`.`id`=`cul`.`lecturer_id`
WHERE 1
